package com.sliansoft.wechat.appcenter.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.sliansoft.wechat.appcenter.model.CorpToken;
import com.sliansoft.wechat.appcenter.model.CorpTokenExample;

public interface CorpTokenMapper {
    int countByExample(CorpTokenExample example);

    int deleteByExample(CorpTokenExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CorpToken record);

    int insertSelective(CorpToken record);

    List<CorpToken> selectByExample(CorpTokenExample example);

    CorpToken selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CorpToken record, @Param("example") CorpTokenExample example);

    int updateByExample(@Param("record") CorpToken record, @Param("example") CorpTokenExample example);

    int updateByPrimaryKeySelective(CorpToken record);

    int updateByPrimaryKey(CorpToken record);
}