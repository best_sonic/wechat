package com.sliansoft.wechat.appcenter.model;

import java.io.Serializable;

public class AgentInfo implements Serializable {
    private Integer id;

    private String corpId;

    private String suiteId;

    private String agentApp;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId == null ? null : corpId.trim();
    }

    public String getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(String suiteId) {
        this.suiteId = suiteId == null ? null : suiteId.trim();
    }

    public String getAgentApp() {
        return agentApp;
    }

    public void setAgentApp(String agentApp) {
        this.agentApp = agentApp == null ? null : agentApp.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AgentInfo other = (AgentInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCorpId() == null ? other.getCorpId() == null : this.getCorpId().equals(other.getCorpId()))
            && (this.getSuiteId() == null ? other.getSuiteId() == null : this.getSuiteId().equals(other.getSuiteId()))
            && (this.getAgentApp() == null ? other.getAgentApp() == null : this.getAgentApp().equals(other.getAgentApp()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCorpId() == null) ? 0 : getCorpId().hashCode());
        result = prime * result + ((getSuiteId() == null) ? 0 : getSuiteId().hashCode());
        result = prime * result + ((getAgentApp() == null) ? 0 : getAgentApp().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", corpId=").append(corpId);
        sb.append(", suiteId=").append(suiteId);
        sb.append(", agentApp=").append(agentApp);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}