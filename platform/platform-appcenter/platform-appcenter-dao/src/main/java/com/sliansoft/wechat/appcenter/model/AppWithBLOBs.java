package com.sliansoft.wechat.appcenter.model;

import java.io.Serializable;

public class AppWithBLOBs extends App implements Serializable {
    private String appIntroduction;

    private String appScene;

    private static final long serialVersionUID = 1L;

    public String getAppIntroduction() {
        return appIntroduction;
    }

    public void setAppIntroduction(String appIntroduction) {
        this.appIntroduction = appIntroduction == null ? null : appIntroduction.trim();
    }

    public String getAppScene() {
        return appScene;
    }

    public void setAppScene(String appScene) {
        this.appScene = appScene == null ? null : appScene.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AppWithBLOBs other = (AppWithBLOBs) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
            && (this.getCallbackurl() == null ? other.getCallbackurl() == null : this.getCallbackurl().equals(other.getCallbackurl()))
            && (this.getAppName() == null ? other.getAppName() == null : this.getAppName().equals(other.getAppName()))
            && (this.getAppLogoname() == null ? other.getAppLogoname() == null : this.getAppLogoname().equals(other.getAppLogoname()))
            && (this.getAppIntroImage() == null ? other.getAppIntroImage() == null : this.getAppIntroImage().equals(other.getAppIntroImage()))
            && (this.getAppSuiteId() == null ? other.getAppSuiteId() == null : this.getAppSuiteId().equals(other.getAppSuiteId()))
            && (this.getAppIntroduction() == null ? other.getAppIntroduction() == null : this.getAppIntroduction().equals(other.getAppIntroduction()))
            && (this.getAppScene() == null ? other.getAppScene() == null : this.getAppScene().equals(other.getAppScene()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
        result = prime * result + ((getCallbackurl() == null) ? 0 : getCallbackurl().hashCode());
        result = prime * result + ((getAppName() == null) ? 0 : getAppName().hashCode());
        result = prime * result + ((getAppLogoname() == null) ? 0 : getAppLogoname().hashCode());
        result = prime * result + ((getAppIntroImage() == null) ? 0 : getAppIntroImage().hashCode());
        result = prime * result + ((getAppSuiteId() == null) ? 0 : getAppSuiteId().hashCode());
        result = prime * result + ((getAppIntroduction() == null) ? 0 : getAppIntroduction().hashCode());
        result = prime * result + ((getAppScene() == null) ? 0 : getAppScene().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", appIntroduction=").append(appIntroduction);
        sb.append(", appScene=").append(appScene);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}