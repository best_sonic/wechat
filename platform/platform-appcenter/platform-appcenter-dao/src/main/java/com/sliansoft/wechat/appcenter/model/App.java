package com.sliansoft.wechat.appcenter.model;

import java.io.Serializable;

public class App implements Serializable {
    private Integer id;

    private Integer appId;

    private String callbackurl;

    private String appName;

    private String appLogoname;

    private String appIntroImage;

    private String appSuiteId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getCallbackurl() {
        return callbackurl;
    }

    public void setCallbackurl(String callbackurl) {
        this.callbackurl = callbackurl == null ? null : callbackurl.trim();
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName == null ? null : appName.trim();
    }

    public String getAppLogoname() {
        return appLogoname;
    }

    public void setAppLogoname(String appLogoname) {
        this.appLogoname = appLogoname == null ? null : appLogoname.trim();
    }

    public String getAppIntroImage() {
        return appIntroImage;
    }

    public void setAppIntroImage(String appIntroImage) {
        this.appIntroImage = appIntroImage == null ? null : appIntroImage.trim();
    }

    public String getAppSuiteId() {
        return appSuiteId;
    }

    public void setAppSuiteId(String appSuiteId) {
        this.appSuiteId = appSuiteId == null ? null : appSuiteId.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        App other = (App) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
            && (this.getCallbackurl() == null ? other.getCallbackurl() == null : this.getCallbackurl().equals(other.getCallbackurl()))
            && (this.getAppName() == null ? other.getAppName() == null : this.getAppName().equals(other.getAppName()))
            && (this.getAppLogoname() == null ? other.getAppLogoname() == null : this.getAppLogoname().equals(other.getAppLogoname()))
            && (this.getAppIntroImage() == null ? other.getAppIntroImage() == null : this.getAppIntroImage().equals(other.getAppIntroImage()))
            && (this.getAppSuiteId() == null ? other.getAppSuiteId() == null : this.getAppSuiteId().equals(other.getAppSuiteId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
        result = prime * result + ((getCallbackurl() == null) ? 0 : getCallbackurl().hashCode());
        result = prime * result + ((getAppName() == null) ? 0 : getAppName().hashCode());
        result = prime * result + ((getAppLogoname() == null) ? 0 : getAppLogoname().hashCode());
        result = prime * result + ((getAppIntroImage() == null) ? 0 : getAppIntroImage().hashCode());
        result = prime * result + ((getAppSuiteId() == null) ? 0 : getAppSuiteId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", appId=").append(appId);
        sb.append(", callbackurl=").append(callbackurl);
        sb.append(", appName=").append(appName);
        sb.append(", appLogoname=").append(appLogoname);
        sb.append(", appIntroImage=").append(appIntroImage);
        sb.append(", appSuiteId=").append(appSuiteId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}