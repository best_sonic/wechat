package com.sliansoft.wechat.appcenter.model;

import java.io.Serializable;

public class SuiteWithBLOBs extends Suite implements Serializable {
    private String suiteIntroduction;

    private String suiteScene;

    private static final long serialVersionUID = 1L;

    public String getSuiteIntroduction() {
        return suiteIntroduction;
    }

    public void setSuiteIntroduction(String suiteIntroduction) {
        this.suiteIntroduction = suiteIntroduction == null ? null : suiteIntroduction.trim();
    }

    public String getSuiteScene() {
        return suiteScene;
    }

    public void setSuiteScene(String suiteScene) {
        this.suiteScene = suiteScene == null ? null : suiteScene.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SuiteWithBLOBs other = (SuiteWithBLOBs) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSuiteId() == null ? other.getSuiteId() == null : this.getSuiteId().equals(other.getSuiteId()))
            && (this.getSuiteSecret() == null ? other.getSuiteSecret() == null : this.getSuiteSecret().equals(other.getSuiteSecret()))
            && (this.getToken() == null ? other.getToken() == null : this.getToken().equals(other.getToken()))
            && (this.getEncodingaeskey() == null ? other.getEncodingaeskey() == null : this.getEncodingaeskey().equals(other.getEncodingaeskey()))
            && (this.getSuiteName() == null ? other.getSuiteName() == null : this.getSuiteName().equals(other.getSuiteName()))
            && (this.getSuiteLogoname() == null ? other.getSuiteLogoname() == null : this.getSuiteLogoname().equals(other.getSuiteLogoname()))
            && (this.getSuiteIntroduction() == null ? other.getSuiteIntroduction() == null : this.getSuiteIntroduction().equals(other.getSuiteIntroduction()))
            && (this.getSuiteScene() == null ? other.getSuiteScene() == null : this.getSuiteScene().equals(other.getSuiteScene()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSuiteId() == null) ? 0 : getSuiteId().hashCode());
        result = prime * result + ((getSuiteSecret() == null) ? 0 : getSuiteSecret().hashCode());
        result = prime * result + ((getToken() == null) ? 0 : getToken().hashCode());
        result = prime * result + ((getEncodingaeskey() == null) ? 0 : getEncodingaeskey().hashCode());
        result = prime * result + ((getSuiteName() == null) ? 0 : getSuiteName().hashCode());
        result = prime * result + ((getSuiteLogoname() == null) ? 0 : getSuiteLogoname().hashCode());
        result = prime * result + ((getSuiteIntroduction() == null) ? 0 : getSuiteIntroduction().hashCode());
        result = prime * result + ((getSuiteScene() == null) ? 0 : getSuiteScene().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", suiteIntroduction=").append(suiteIntroduction);
        sb.append(", suiteScene=").append(suiteScene);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}