package com.sliansoft.wechat.appcenter.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.sliansoft.wechat.appcenter.model.Menuone;
import com.sliansoft.wechat.appcenter.model.MenuoneExample;

public interface MenuoneMapper {
    int countByExample(MenuoneExample example);

    int deleteByExample(MenuoneExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Menuone record);

    int insertSelective(Menuone record);

    List<Menuone> selectByExample(MenuoneExample example);

    Menuone selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Menuone record, @Param("example") MenuoneExample example);

    int updateByExample(@Param("record") Menuone record, @Param("example") MenuoneExample example);

    int updateByPrimaryKeySelective(Menuone record);

    int updateByPrimaryKey(Menuone record);
}