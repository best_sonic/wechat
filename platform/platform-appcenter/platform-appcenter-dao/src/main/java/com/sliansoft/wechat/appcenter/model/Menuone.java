package com.sliansoft.wechat.appcenter.model;

import java.io.Serializable;

public class Menuone implements Serializable {
    private Integer id;

    private String menuoneName;

    private String menuoneType;

    private String menuoneUrl;

    private String menuoneKey;

    private Integer menuoneAppId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMenuoneName() {
        return menuoneName;
    }

    public void setMenuoneName(String menuoneName) {
        this.menuoneName = menuoneName == null ? null : menuoneName.trim();
    }

    public String getMenuoneType() {
        return menuoneType;
    }

    public void setMenuoneType(String menuoneType) {
        this.menuoneType = menuoneType == null ? null : menuoneType.trim();
    }

    public String getMenuoneUrl() {
        return menuoneUrl;
    }

    public void setMenuoneUrl(String menuoneUrl) {
        this.menuoneUrl = menuoneUrl == null ? null : menuoneUrl.trim();
    }

    public String getMenuoneKey() {
        return menuoneKey;
    }

    public void setMenuoneKey(String menuoneKey) {
        this.menuoneKey = menuoneKey == null ? null : menuoneKey.trim();
    }

    public Integer getMenuoneAppId() {
        return menuoneAppId;
    }

    public void setMenuoneAppId(Integer menuoneAppId) {
        this.menuoneAppId = menuoneAppId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Menuone other = (Menuone) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getMenuoneName() == null ? other.getMenuoneName() == null : this.getMenuoneName().equals(other.getMenuoneName()))
            && (this.getMenuoneType() == null ? other.getMenuoneType() == null : this.getMenuoneType().equals(other.getMenuoneType()))
            && (this.getMenuoneUrl() == null ? other.getMenuoneUrl() == null : this.getMenuoneUrl().equals(other.getMenuoneUrl()))
            && (this.getMenuoneKey() == null ? other.getMenuoneKey() == null : this.getMenuoneKey().equals(other.getMenuoneKey()))
            && (this.getMenuoneAppId() == null ? other.getMenuoneAppId() == null : this.getMenuoneAppId().equals(other.getMenuoneAppId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getMenuoneName() == null) ? 0 : getMenuoneName().hashCode());
        result = prime * result + ((getMenuoneType() == null) ? 0 : getMenuoneType().hashCode());
        result = prime * result + ((getMenuoneUrl() == null) ? 0 : getMenuoneUrl().hashCode());
        result = prime * result + ((getMenuoneKey() == null) ? 0 : getMenuoneKey().hashCode());
        result = prime * result + ((getMenuoneAppId() == null) ? 0 : getMenuoneAppId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", menuoneName=").append(menuoneName);
        sb.append(", menuoneType=").append(menuoneType);
        sb.append(", menuoneUrl=").append(menuoneUrl);
        sb.append(", menuoneKey=").append(menuoneKey);
        sb.append(", menuoneAppId=").append(menuoneAppId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}