package com.sliansoft.wechat.appcenter.model;

import java.util.ArrayList;
import java.util.List;

import com.sliansoft.wechat.appcenter.utils.Page;

public class SuiteExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Page page;

    public SuiteExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSuiteIdIsNull() {
            addCriterion("suite_id is null");
            return (Criteria) this;
        }

        public Criteria andSuiteIdIsNotNull() {
            addCriterion("suite_id is not null");
            return (Criteria) this;
        }

        public Criteria andSuiteIdEqualTo(String value) {
            addCriterion("suite_id =", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdNotEqualTo(String value) {
            addCriterion("suite_id <>", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdGreaterThan(String value) {
            addCriterion("suite_id >", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdGreaterThanOrEqualTo(String value) {
            addCriterion("suite_id >=", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdLessThan(String value) {
            addCriterion("suite_id <", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdLessThanOrEqualTo(String value) {
            addCriterion("suite_id <=", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdLike(String value) {
            addCriterion("suite_id like", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdNotLike(String value) {
            addCriterion("suite_id not like", value, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdIn(List<String> values) {
            addCriterion("suite_id in", values, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdNotIn(List<String> values) {
            addCriterion("suite_id not in", values, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdBetween(String value1, String value2) {
            addCriterion("suite_id between", value1, value2, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteIdNotBetween(String value1, String value2) {
            addCriterion("suite_id not between", value1, value2, "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretIsNull() {
            addCriterion("suite_secret is null");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretIsNotNull() {
            addCriterion("suite_secret is not null");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretEqualTo(String value) {
            addCriterion("suite_secret =", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretNotEqualTo(String value) {
            addCriterion("suite_secret <>", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretGreaterThan(String value) {
            addCriterion("suite_secret >", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretGreaterThanOrEqualTo(String value) {
            addCriterion("suite_secret >=", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretLessThan(String value) {
            addCriterion("suite_secret <", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretLessThanOrEqualTo(String value) {
            addCriterion("suite_secret <=", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretLike(String value) {
            addCriterion("suite_secret like", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretNotLike(String value) {
            addCriterion("suite_secret not like", value, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretIn(List<String> values) {
            addCriterion("suite_secret in", values, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretNotIn(List<String> values) {
            addCriterion("suite_secret not in", values, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretBetween(String value1, String value2) {
            addCriterion("suite_secret between", value1, value2, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretNotBetween(String value1, String value2) {
            addCriterion("suite_secret not between", value1, value2, "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andTokenIsNull() {
            addCriterion("token is null");
            return (Criteria) this;
        }

        public Criteria andTokenIsNotNull() {
            addCriterion("token is not null");
            return (Criteria) this;
        }

        public Criteria andTokenEqualTo(String value) {
            addCriterion("token =", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotEqualTo(String value) {
            addCriterion("token <>", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenGreaterThan(String value) {
            addCriterion("token >", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenGreaterThanOrEqualTo(String value) {
            addCriterion("token >=", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenLessThan(String value) {
            addCriterion("token <", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenLessThanOrEqualTo(String value) {
            addCriterion("token <=", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenLike(String value) {
            addCriterion("token like", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotLike(String value) {
            addCriterion("token not like", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenIn(List<String> values) {
            addCriterion("token in", values, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotIn(List<String> values) {
            addCriterion("token not in", values, "token");
            return (Criteria) this;
        }

        public Criteria andTokenBetween(String value1, String value2) {
            addCriterion("token between", value1, value2, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotBetween(String value1, String value2) {
            addCriterion("token not between", value1, value2, "token");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyIsNull() {
            addCriterion("encodingaeskey is null");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyIsNotNull() {
            addCriterion("encodingaeskey is not null");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyEqualTo(String value) {
            addCriterion("encodingaeskey =", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyNotEqualTo(String value) {
            addCriterion("encodingaeskey <>", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyGreaterThan(String value) {
            addCriterion("encodingaeskey >", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyGreaterThanOrEqualTo(String value) {
            addCriterion("encodingaeskey >=", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyLessThan(String value) {
            addCriterion("encodingaeskey <", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyLessThanOrEqualTo(String value) {
            addCriterion("encodingaeskey <=", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyLike(String value) {
            addCriterion("encodingaeskey like", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyNotLike(String value) {
            addCriterion("encodingaeskey not like", value, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyIn(List<String> values) {
            addCriterion("encodingaeskey in", values, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyNotIn(List<String> values) {
            addCriterion("encodingaeskey not in", values, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyBetween(String value1, String value2) {
            addCriterion("encodingaeskey between", value1, value2, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyNotBetween(String value1, String value2) {
            addCriterion("encodingaeskey not between", value1, value2, "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andSuiteNameIsNull() {
            addCriterion("suite_name is null");
            return (Criteria) this;
        }

        public Criteria andSuiteNameIsNotNull() {
            addCriterion("suite_name is not null");
            return (Criteria) this;
        }

        public Criteria andSuiteNameEqualTo(String value) {
            addCriterion("suite_name =", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameNotEqualTo(String value) {
            addCriterion("suite_name <>", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameGreaterThan(String value) {
            addCriterion("suite_name >", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameGreaterThanOrEqualTo(String value) {
            addCriterion("suite_name >=", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameLessThan(String value) {
            addCriterion("suite_name <", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameLessThanOrEqualTo(String value) {
            addCriterion("suite_name <=", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameLike(String value) {
            addCriterion("suite_name like", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameNotLike(String value) {
            addCriterion("suite_name not like", value, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameIn(List<String> values) {
            addCriterion("suite_name in", values, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameNotIn(List<String> values) {
            addCriterion("suite_name not in", values, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameBetween(String value1, String value2) {
            addCriterion("suite_name between", value1, value2, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteNameNotBetween(String value1, String value2) {
            addCriterion("suite_name not between", value1, value2, "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameIsNull() {
            addCriterion("suite_logoname is null");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameIsNotNull() {
            addCriterion("suite_logoname is not null");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameEqualTo(String value) {
            addCriterion("suite_logoname =", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameNotEqualTo(String value) {
            addCriterion("suite_logoname <>", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameGreaterThan(String value) {
            addCriterion("suite_logoname >", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameGreaterThanOrEqualTo(String value) {
            addCriterion("suite_logoname >=", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameLessThan(String value) {
            addCriterion("suite_logoname <", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameLessThanOrEqualTo(String value) {
            addCriterion("suite_logoname <=", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameLike(String value) {
            addCriterion("suite_logoname like", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameNotLike(String value) {
            addCriterion("suite_logoname not like", value, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameIn(List<String> values) {
            addCriterion("suite_logoname in", values, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameNotIn(List<String> values) {
            addCriterion("suite_logoname not in", values, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameBetween(String value1, String value2) {
            addCriterion("suite_logoname between", value1, value2, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameNotBetween(String value1, String value2) {
            addCriterion("suite_logoname not between", value1, value2, "suiteLogoname");
            return (Criteria) this;
        }

        public Criteria andSuiteIdLikeInsensitive(String value) {
            addCriterion("upper(suite_id) like", value.toUpperCase(), "suiteId");
            return (Criteria) this;
        }

        public Criteria andSuiteSecretLikeInsensitive(String value) {
            addCriterion("upper(suite_secret) like", value.toUpperCase(), "suiteSecret");
            return (Criteria) this;
        }

        public Criteria andTokenLikeInsensitive(String value) {
            addCriterion("upper(token) like", value.toUpperCase(), "token");
            return (Criteria) this;
        }

        public Criteria andEncodingaeskeyLikeInsensitive(String value) {
            addCriterion("upper(encodingaeskey) like", value.toUpperCase(), "encodingaeskey");
            return (Criteria) this;
        }

        public Criteria andSuiteNameLikeInsensitive(String value) {
            addCriterion("upper(suite_name) like", value.toUpperCase(), "suiteName");
            return (Criteria) this;
        }

        public Criteria andSuiteLogonameLikeInsensitive(String value) {
            addCriterion("upper(suite_logoname) like", value.toUpperCase(), "suiteLogoname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}