package com.sliansoft.wechat.appcenter.model;

import java.util.ArrayList;
import java.util.List;

import com.sliansoft.wechat.appcenter.utils.Page;

public class MenuoneExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Page page;

    public MenuoneExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameIsNull() {
            addCriterion("menuone_name is null");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameIsNotNull() {
            addCriterion("menuone_name is not null");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameEqualTo(String value) {
            addCriterion("menuone_name =", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameNotEqualTo(String value) {
            addCriterion("menuone_name <>", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameGreaterThan(String value) {
            addCriterion("menuone_name >", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameGreaterThanOrEqualTo(String value) {
            addCriterion("menuone_name >=", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameLessThan(String value) {
            addCriterion("menuone_name <", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameLessThanOrEqualTo(String value) {
            addCriterion("menuone_name <=", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameLike(String value) {
            addCriterion("menuone_name like", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameNotLike(String value) {
            addCriterion("menuone_name not like", value, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameIn(List<String> values) {
            addCriterion("menuone_name in", values, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameNotIn(List<String> values) {
            addCriterion("menuone_name not in", values, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameBetween(String value1, String value2) {
            addCriterion("menuone_name between", value1, value2, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameNotBetween(String value1, String value2) {
            addCriterion("menuone_name not between", value1, value2, "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeIsNull() {
            addCriterion("menuone_type is null");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeIsNotNull() {
            addCriterion("menuone_type is not null");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeEqualTo(String value) {
            addCriterion("menuone_type =", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeNotEqualTo(String value) {
            addCriterion("menuone_type <>", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeGreaterThan(String value) {
            addCriterion("menuone_type >", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeGreaterThanOrEqualTo(String value) {
            addCriterion("menuone_type >=", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeLessThan(String value) {
            addCriterion("menuone_type <", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeLessThanOrEqualTo(String value) {
            addCriterion("menuone_type <=", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeLike(String value) {
            addCriterion("menuone_type like", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeNotLike(String value) {
            addCriterion("menuone_type not like", value, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeIn(List<String> values) {
            addCriterion("menuone_type in", values, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeNotIn(List<String> values) {
            addCriterion("menuone_type not in", values, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeBetween(String value1, String value2) {
            addCriterion("menuone_type between", value1, value2, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeNotBetween(String value1, String value2) {
            addCriterion("menuone_type not between", value1, value2, "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlIsNull() {
            addCriterion("menuone_url is null");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlIsNotNull() {
            addCriterion("menuone_url is not null");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlEqualTo(String value) {
            addCriterion("menuone_url =", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlNotEqualTo(String value) {
            addCriterion("menuone_url <>", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlGreaterThan(String value) {
            addCriterion("menuone_url >", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlGreaterThanOrEqualTo(String value) {
            addCriterion("menuone_url >=", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlLessThan(String value) {
            addCriterion("menuone_url <", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlLessThanOrEqualTo(String value) {
            addCriterion("menuone_url <=", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlLike(String value) {
            addCriterion("menuone_url like", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlNotLike(String value) {
            addCriterion("menuone_url not like", value, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlIn(List<String> values) {
            addCriterion("menuone_url in", values, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlNotIn(List<String> values) {
            addCriterion("menuone_url not in", values, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlBetween(String value1, String value2) {
            addCriterion("menuone_url between", value1, value2, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlNotBetween(String value1, String value2) {
            addCriterion("menuone_url not between", value1, value2, "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyIsNull() {
            addCriterion("menuone_key is null");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyIsNotNull() {
            addCriterion("menuone_key is not null");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyEqualTo(String value) {
            addCriterion("menuone_key =", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyNotEqualTo(String value) {
            addCriterion("menuone_key <>", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyGreaterThan(String value) {
            addCriterion("menuone_key >", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyGreaterThanOrEqualTo(String value) {
            addCriterion("menuone_key >=", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyLessThan(String value) {
            addCriterion("menuone_key <", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyLessThanOrEqualTo(String value) {
            addCriterion("menuone_key <=", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyLike(String value) {
            addCriterion("menuone_key like", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyNotLike(String value) {
            addCriterion("menuone_key not like", value, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyIn(List<String> values) {
            addCriterion("menuone_key in", values, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyNotIn(List<String> values) {
            addCriterion("menuone_key not in", values, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyBetween(String value1, String value2) {
            addCriterion("menuone_key between", value1, value2, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyNotBetween(String value1, String value2) {
            addCriterion("menuone_key not between", value1, value2, "menuoneKey");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdIsNull() {
            addCriterion("menuone_app_id is null");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdIsNotNull() {
            addCriterion("menuone_app_id is not null");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdEqualTo(Integer value) {
            addCriterion("menuone_app_id =", value, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdNotEqualTo(Integer value) {
            addCriterion("menuone_app_id <>", value, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdGreaterThan(Integer value) {
            addCriterion("menuone_app_id >", value, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("menuone_app_id >=", value, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdLessThan(Integer value) {
            addCriterion("menuone_app_id <", value, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdLessThanOrEqualTo(Integer value) {
            addCriterion("menuone_app_id <=", value, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdIn(List<Integer> values) {
            addCriterion("menuone_app_id in", values, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdNotIn(List<Integer> values) {
            addCriterion("menuone_app_id not in", values, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdBetween(Integer value1, Integer value2) {
            addCriterion("menuone_app_id between", value1, value2, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneAppIdNotBetween(Integer value1, Integer value2) {
            addCriterion("menuone_app_id not between", value1, value2, "menuoneAppId");
            return (Criteria) this;
        }

        public Criteria andMenuoneNameLikeInsensitive(String value) {
            addCriterion("upper(menuone_name) like", value.toUpperCase(), "menuoneName");
            return (Criteria) this;
        }

        public Criteria andMenuoneTypeLikeInsensitive(String value) {
            addCriterion("upper(menuone_type) like", value.toUpperCase(), "menuoneType");
            return (Criteria) this;
        }

        public Criteria andMenuoneUrlLikeInsensitive(String value) {
            addCriterion("upper(menuone_url) like", value.toUpperCase(), "menuoneUrl");
            return (Criteria) this;
        }

        public Criteria andMenuoneKeyLikeInsensitive(String value) {
            addCriterion("upper(menuone_key) like", value.toUpperCase(), "menuoneKey");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}