package com.sliansoft.wechat.appcenter.model;

import java.util.ArrayList;
import java.util.List;

import com.sliansoft.wechat.appcenter.utils.Page;

public class MenutwoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Page page;

    public MenutwoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameIsNull() {
            addCriterion("menutwo_name is null");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameIsNotNull() {
            addCriterion("menutwo_name is not null");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameEqualTo(String value) {
            addCriterion("menutwo_name =", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameNotEqualTo(String value) {
            addCriterion("menutwo_name <>", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameGreaterThan(String value) {
            addCriterion("menutwo_name >", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameGreaterThanOrEqualTo(String value) {
            addCriterion("menutwo_name >=", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameLessThan(String value) {
            addCriterion("menutwo_name <", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameLessThanOrEqualTo(String value) {
            addCriterion("menutwo_name <=", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameLike(String value) {
            addCriterion("menutwo_name like", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameNotLike(String value) {
            addCriterion("menutwo_name not like", value, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameIn(List<String> values) {
            addCriterion("menutwo_name in", values, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameNotIn(List<String> values) {
            addCriterion("menutwo_name not in", values, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameBetween(String value1, String value2) {
            addCriterion("menutwo_name between", value1, value2, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameNotBetween(String value1, String value2) {
            addCriterion("menutwo_name not between", value1, value2, "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeIsNull() {
            addCriterion("menutwo_type is null");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeIsNotNull() {
            addCriterion("menutwo_type is not null");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeEqualTo(String value) {
            addCriterion("menutwo_type =", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeNotEqualTo(String value) {
            addCriterion("menutwo_type <>", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeGreaterThan(String value) {
            addCriterion("menutwo_type >", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("menutwo_type >=", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeLessThan(String value) {
            addCriterion("menutwo_type <", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeLessThanOrEqualTo(String value) {
            addCriterion("menutwo_type <=", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeLike(String value) {
            addCriterion("menutwo_type like", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeNotLike(String value) {
            addCriterion("menutwo_type not like", value, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeIn(List<String> values) {
            addCriterion("menutwo_type in", values, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeNotIn(List<String> values) {
            addCriterion("menutwo_type not in", values, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeBetween(String value1, String value2) {
            addCriterion("menutwo_type between", value1, value2, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeNotBetween(String value1, String value2) {
            addCriterion("menutwo_type not between", value1, value2, "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlIsNull() {
            addCriterion("menutwo_url is null");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlIsNotNull() {
            addCriterion("menutwo_url is not null");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlEqualTo(String value) {
            addCriterion("menutwo_url =", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlNotEqualTo(String value) {
            addCriterion("menutwo_url <>", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlGreaterThan(String value) {
            addCriterion("menutwo_url >", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlGreaterThanOrEqualTo(String value) {
            addCriterion("menutwo_url >=", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlLessThan(String value) {
            addCriterion("menutwo_url <", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlLessThanOrEqualTo(String value) {
            addCriterion("menutwo_url <=", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlLike(String value) {
            addCriterion("menutwo_url like", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlNotLike(String value) {
            addCriterion("menutwo_url not like", value, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlIn(List<String> values) {
            addCriterion("menutwo_url in", values, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlNotIn(List<String> values) {
            addCriterion("menutwo_url not in", values, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlBetween(String value1, String value2) {
            addCriterion("menutwo_url between", value1, value2, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlNotBetween(String value1, String value2) {
            addCriterion("menutwo_url not between", value1, value2, "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyIsNull() {
            addCriterion("menutwo_key is null");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyIsNotNull() {
            addCriterion("menutwo_key is not null");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyEqualTo(String value) {
            addCriterion("menutwo_key =", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyNotEqualTo(String value) {
            addCriterion("menutwo_key <>", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyGreaterThan(String value) {
            addCriterion("menutwo_key >", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyGreaterThanOrEqualTo(String value) {
            addCriterion("menutwo_key >=", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyLessThan(String value) {
            addCriterion("menutwo_key <", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyLessThanOrEqualTo(String value) {
            addCriterion("menutwo_key <=", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyLike(String value) {
            addCriterion("menutwo_key like", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyNotLike(String value) {
            addCriterion("menutwo_key not like", value, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyIn(List<String> values) {
            addCriterion("menutwo_key in", values, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyNotIn(List<String> values) {
            addCriterion("menutwo_key not in", values, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyBetween(String value1, String value2) {
            addCriterion("menutwo_key between", value1, value2, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyNotBetween(String value1, String value2) {
            addCriterion("menutwo_key not between", value1, value2, "menutwoKey");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdIsNull() {
            addCriterion("menutwo_menuone_id is null");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdIsNotNull() {
            addCriterion("menutwo_menuone_id is not null");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdEqualTo(Integer value) {
            addCriterion("menutwo_menuone_id =", value, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdNotEqualTo(Integer value) {
            addCriterion("menutwo_menuone_id <>", value, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdGreaterThan(Integer value) {
            addCriterion("menutwo_menuone_id >", value, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("menutwo_menuone_id >=", value, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdLessThan(Integer value) {
            addCriterion("menutwo_menuone_id <", value, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdLessThanOrEqualTo(Integer value) {
            addCriterion("menutwo_menuone_id <=", value, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdIn(List<Integer> values) {
            addCriterion("menutwo_menuone_id in", values, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdNotIn(List<Integer> values) {
            addCriterion("menutwo_menuone_id not in", values, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdBetween(Integer value1, Integer value2) {
            addCriterion("menutwo_menuone_id between", value1, value2, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoMenuoneIdNotBetween(Integer value1, Integer value2) {
            addCriterion("menutwo_menuone_id not between", value1, value2, "menutwoMenuoneId");
            return (Criteria) this;
        }

        public Criteria andMenutwoNameLikeInsensitive(String value) {
            addCriterion("upper(menutwo_name) like", value.toUpperCase(), "menutwoName");
            return (Criteria) this;
        }

        public Criteria andMenutwoTypeLikeInsensitive(String value) {
            addCriterion("upper(menutwo_type) like", value.toUpperCase(), "menutwoType");
            return (Criteria) this;
        }

        public Criteria andMenutwoUrlLikeInsensitive(String value) {
            addCriterion("upper(menutwo_url) like", value.toUpperCase(), "menutwoUrl");
            return (Criteria) this;
        }

        public Criteria andMenutwoKeyLikeInsensitive(String value) {
            addCriterion("upper(menutwo_key) like", value.toUpperCase(), "menutwoKey");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}