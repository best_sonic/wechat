package com.sliansoft.wechat.appcenter.model;

import java.util.ArrayList;
import java.util.List;

import com.sliansoft.wechat.appcenter.utils.Page;

public class AppExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Page page;

    public AppExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNull() {
            addCriterion("app_id is null");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNotNull() {
            addCriterion("app_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppIdEqualTo(Integer value) {
            addCriterion("app_id =", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotEqualTo(Integer value) {
            addCriterion("app_id <>", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThan(Integer value) {
            addCriterion("app_id >", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("app_id >=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThan(Integer value) {
            addCriterion("app_id <", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThanOrEqualTo(Integer value) {
            addCriterion("app_id <=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdIn(List<Integer> values) {
            addCriterion("app_id in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotIn(List<Integer> values) {
            addCriterion("app_id not in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdBetween(Integer value1, Integer value2) {
            addCriterion("app_id between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotBetween(Integer value1, Integer value2) {
            addCriterion("app_id not between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andCallbackurlIsNull() {
            addCriterion("callbackurl is null");
            return (Criteria) this;
        }

        public Criteria andCallbackurlIsNotNull() {
            addCriterion("callbackurl is not null");
            return (Criteria) this;
        }

        public Criteria andCallbackurlEqualTo(String value) {
            addCriterion("callbackurl =", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlNotEqualTo(String value) {
            addCriterion("callbackurl <>", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlGreaterThan(String value) {
            addCriterion("callbackurl >", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlGreaterThanOrEqualTo(String value) {
            addCriterion("callbackurl >=", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlLessThan(String value) {
            addCriterion("callbackurl <", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlLessThanOrEqualTo(String value) {
            addCriterion("callbackurl <=", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlLike(String value) {
            addCriterion("callbackurl like", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlNotLike(String value) {
            addCriterion("callbackurl not like", value, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlIn(List<String> values) {
            addCriterion("callbackurl in", values, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlNotIn(List<String> values) {
            addCriterion("callbackurl not in", values, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlBetween(String value1, String value2) {
            addCriterion("callbackurl between", value1, value2, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andCallbackurlNotBetween(String value1, String value2) {
            addCriterion("callbackurl not between", value1, value2, "callbackurl");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNull() {
            addCriterion("app_name is null");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNotNull() {
            addCriterion("app_name is not null");
            return (Criteria) this;
        }

        public Criteria andAppNameEqualTo(String value) {
            addCriterion("app_name =", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotEqualTo(String value) {
            addCriterion("app_name <>", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThan(String value) {
            addCriterion("app_name >", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThanOrEqualTo(String value) {
            addCriterion("app_name >=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThan(String value) {
            addCriterion("app_name <", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThanOrEqualTo(String value) {
            addCriterion("app_name <=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLike(String value) {
            addCriterion("app_name like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotLike(String value) {
            addCriterion("app_name not like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameIn(List<String> values) {
            addCriterion("app_name in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotIn(List<String> values) {
            addCriterion("app_name not in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameBetween(String value1, String value2) {
            addCriterion("app_name between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotBetween(String value1, String value2) {
            addCriterion("app_name not between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andAppLogonameIsNull() {
            addCriterion("app_logoname is null");
            return (Criteria) this;
        }

        public Criteria andAppLogonameIsNotNull() {
            addCriterion("app_logoname is not null");
            return (Criteria) this;
        }

        public Criteria andAppLogonameEqualTo(String value) {
            addCriterion("app_logoname =", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameNotEqualTo(String value) {
            addCriterion("app_logoname <>", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameGreaterThan(String value) {
            addCriterion("app_logoname >", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameGreaterThanOrEqualTo(String value) {
            addCriterion("app_logoname >=", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameLessThan(String value) {
            addCriterion("app_logoname <", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameLessThanOrEqualTo(String value) {
            addCriterion("app_logoname <=", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameLike(String value) {
            addCriterion("app_logoname like", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameNotLike(String value) {
            addCriterion("app_logoname not like", value, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameIn(List<String> values) {
            addCriterion("app_logoname in", values, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameNotIn(List<String> values) {
            addCriterion("app_logoname not in", values, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameBetween(String value1, String value2) {
            addCriterion("app_logoname between", value1, value2, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppLogonameNotBetween(String value1, String value2) {
            addCriterion("app_logoname not between", value1, value2, "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageIsNull() {
            addCriterion("app_intro_image is null");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageIsNotNull() {
            addCriterion("app_intro_image is not null");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageEqualTo(String value) {
            addCriterion("app_intro_image =", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageNotEqualTo(String value) {
            addCriterion("app_intro_image <>", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageGreaterThan(String value) {
            addCriterion("app_intro_image >", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageGreaterThanOrEqualTo(String value) {
            addCriterion("app_intro_image >=", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageLessThan(String value) {
            addCriterion("app_intro_image <", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageLessThanOrEqualTo(String value) {
            addCriterion("app_intro_image <=", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageLike(String value) {
            addCriterion("app_intro_image like", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageNotLike(String value) {
            addCriterion("app_intro_image not like", value, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageIn(List<String> values) {
            addCriterion("app_intro_image in", values, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageNotIn(List<String> values) {
            addCriterion("app_intro_image not in", values, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageBetween(String value1, String value2) {
            addCriterion("app_intro_image between", value1, value2, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageNotBetween(String value1, String value2) {
            addCriterion("app_intro_image not between", value1, value2, "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdIsNull() {
            addCriterion("app_suite_id is null");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdIsNotNull() {
            addCriterion("app_suite_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdEqualTo(String value) {
            addCriterion("app_suite_id =", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdNotEqualTo(String value) {
            addCriterion("app_suite_id <>", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdGreaterThan(String value) {
            addCriterion("app_suite_id >", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdGreaterThanOrEqualTo(String value) {
            addCriterion("app_suite_id >=", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdLessThan(String value) {
            addCriterion("app_suite_id <", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdLessThanOrEqualTo(String value) {
            addCriterion("app_suite_id <=", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdLike(String value) {
            addCriterion("app_suite_id like", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdNotLike(String value) {
            addCriterion("app_suite_id not like", value, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdIn(List<String> values) {
            addCriterion("app_suite_id in", values, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdNotIn(List<String> values) {
            addCriterion("app_suite_id not in", values, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdBetween(String value1, String value2) {
            addCriterion("app_suite_id between", value1, value2, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdNotBetween(String value1, String value2) {
            addCriterion("app_suite_id not between", value1, value2, "appSuiteId");
            return (Criteria) this;
        }

        public Criteria andCallbackurlLikeInsensitive(String value) {
            addCriterion("upper(callbackurl) like", value.toUpperCase(), "callbackurl");
            return (Criteria) this;
        }

        public Criteria andAppNameLikeInsensitive(String value) {
            addCriterion("upper(app_name) like", value.toUpperCase(), "appName");
            return (Criteria) this;
        }

        public Criteria andAppLogonameLikeInsensitive(String value) {
            addCriterion("upper(app_logoname) like", value.toUpperCase(), "appLogoname");
            return (Criteria) this;
        }

        public Criteria andAppIntroImageLikeInsensitive(String value) {
            addCriterion("upper(app_intro_image) like", value.toUpperCase(), "appIntroImage");
            return (Criteria) this;
        }

        public Criteria andAppSuiteIdLikeInsensitive(String value) {
            addCriterion("upper(app_suite_id) like", value.toUpperCase(), "appSuiteId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}