package com.sliansoft.wechat.appcenter.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.sliansoft.wechat.appcenter.model.Menutwo;
import com.sliansoft.wechat.appcenter.model.MenutwoExample;

public interface MenutwoMapper {
    int countByExample(MenutwoExample example);

    int deleteByExample(MenutwoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Menutwo record);

    int insertSelective(Menutwo record);

    List<Menutwo> selectByExample(MenutwoExample example);

    Menutwo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Menutwo record, @Param("example") MenutwoExample example);

    int updateByExample(@Param("record") Menutwo record, @Param("example") MenutwoExample example);

    int updateByPrimaryKeySelective(Menutwo record);

    int updateByPrimaryKey(Menutwo record);
}