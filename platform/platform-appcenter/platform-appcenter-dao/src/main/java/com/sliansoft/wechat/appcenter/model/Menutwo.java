package com.sliansoft.wechat.appcenter.model;

import java.io.Serializable;

public class Menutwo implements Serializable {
    private Integer id;

    private String menutwoName;

    private String menutwoType;

    private String menutwoUrl;

    private String menutwoKey;

    private Integer menutwoMenuoneId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMenutwoName() {
        return menutwoName;
    }

    public void setMenutwoName(String menutwoName) {
        this.menutwoName = menutwoName == null ? null : menutwoName.trim();
    }

    public String getMenutwoType() {
        return menutwoType;
    }

    public void setMenutwoType(String menutwoType) {
        this.menutwoType = menutwoType == null ? null : menutwoType.trim();
    }

    public String getMenutwoUrl() {
        return menutwoUrl;
    }

    public void setMenutwoUrl(String menutwoUrl) {
        this.menutwoUrl = menutwoUrl == null ? null : menutwoUrl.trim();
    }

    public String getMenutwoKey() {
        return menutwoKey;
    }

    public void setMenutwoKey(String menutwoKey) {
        this.menutwoKey = menutwoKey == null ? null : menutwoKey.trim();
    }

    public Integer getMenutwoMenuoneId() {
        return menutwoMenuoneId;
    }

    public void setMenutwoMenuoneId(Integer menutwoMenuoneId) {
        this.menutwoMenuoneId = menutwoMenuoneId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Menutwo other = (Menutwo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getMenutwoName() == null ? other.getMenutwoName() == null : this.getMenutwoName().equals(other.getMenutwoName()))
            && (this.getMenutwoType() == null ? other.getMenutwoType() == null : this.getMenutwoType().equals(other.getMenutwoType()))
            && (this.getMenutwoUrl() == null ? other.getMenutwoUrl() == null : this.getMenutwoUrl().equals(other.getMenutwoUrl()))
            && (this.getMenutwoKey() == null ? other.getMenutwoKey() == null : this.getMenutwoKey().equals(other.getMenutwoKey()))
            && (this.getMenutwoMenuoneId() == null ? other.getMenutwoMenuoneId() == null : this.getMenutwoMenuoneId().equals(other.getMenutwoMenuoneId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getMenutwoName() == null) ? 0 : getMenutwoName().hashCode());
        result = prime * result + ((getMenutwoType() == null) ? 0 : getMenutwoType().hashCode());
        result = prime * result + ((getMenutwoUrl() == null) ? 0 : getMenutwoUrl().hashCode());
        result = prime * result + ((getMenutwoKey() == null) ? 0 : getMenutwoKey().hashCode());
        result = prime * result + ((getMenutwoMenuoneId() == null) ? 0 : getMenutwoMenuoneId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", menutwoName=").append(menutwoName);
        sb.append(", menutwoType=").append(menutwoType);
        sb.append(", menutwoUrl=").append(menutwoUrl);
        sb.append(", menutwoKey=").append(menutwoKey);
        sb.append(", menutwoMenuoneId=").append(menutwoMenuoneId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}