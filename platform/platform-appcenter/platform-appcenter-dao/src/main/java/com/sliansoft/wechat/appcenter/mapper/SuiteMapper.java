package com.sliansoft.wechat.appcenter.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.sliansoft.wechat.appcenter.model.Suite;
import com.sliansoft.wechat.appcenter.model.SuiteExample;
import com.sliansoft.wechat.appcenter.model.SuiteWithBLOBs;

public interface SuiteMapper {
    int countByExample(SuiteExample example);

    int deleteByExample(SuiteExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SuiteWithBLOBs record);

    int insertSelective(SuiteWithBLOBs record);

    List<SuiteWithBLOBs> selectByExampleWithBLOBs(SuiteExample example);

    List<Suite> selectByExample(SuiteExample example);

    SuiteWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SuiteWithBLOBs record, @Param("example") SuiteExample example);

    int updateByExampleWithBLOBs(@Param("record") SuiteWithBLOBs record, @Param("example") SuiteExample example);

    int updateByExample(@Param("record") Suite record, @Param("example") SuiteExample example);

    int updateByPrimaryKeySelective(SuiteWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(SuiteWithBLOBs record);

    int updateByPrimaryKey(Suite record);
}