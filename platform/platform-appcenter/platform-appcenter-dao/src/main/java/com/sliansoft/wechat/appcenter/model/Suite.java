package com.sliansoft.wechat.appcenter.model;

import java.io.Serializable;

public class Suite implements Serializable {
    private Integer id;

    private String suiteId;

    private String suiteSecret;

    private String token;

    private String encodingaeskey;

    private String suiteName;

    private String suiteLogoname;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(String suiteId) {
        this.suiteId = suiteId == null ? null : suiteId.trim();
    }

    public String getSuiteSecret() {
        return suiteSecret;
    }

    public void setSuiteSecret(String suiteSecret) {
        this.suiteSecret = suiteSecret == null ? null : suiteSecret.trim();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public String getEncodingaeskey() {
        return encodingaeskey;
    }

    public void setEncodingaeskey(String encodingaeskey) {
        this.encodingaeskey = encodingaeskey == null ? null : encodingaeskey.trim();
    }

    public String getSuiteName() {
        return suiteName;
    }

    public void setSuiteName(String suiteName) {
        this.suiteName = suiteName == null ? null : suiteName.trim();
    }

    public String getSuiteLogoname() {
        return suiteLogoname;
    }

    public void setSuiteLogoname(String suiteLogoname) {
        this.suiteLogoname = suiteLogoname == null ? null : suiteLogoname.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Suite other = (Suite) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSuiteId() == null ? other.getSuiteId() == null : this.getSuiteId().equals(other.getSuiteId()))
            && (this.getSuiteSecret() == null ? other.getSuiteSecret() == null : this.getSuiteSecret().equals(other.getSuiteSecret()))
            && (this.getToken() == null ? other.getToken() == null : this.getToken().equals(other.getToken()))
            && (this.getEncodingaeskey() == null ? other.getEncodingaeskey() == null : this.getEncodingaeskey().equals(other.getEncodingaeskey()))
            && (this.getSuiteName() == null ? other.getSuiteName() == null : this.getSuiteName().equals(other.getSuiteName()))
            && (this.getSuiteLogoname() == null ? other.getSuiteLogoname() == null : this.getSuiteLogoname().equals(other.getSuiteLogoname()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSuiteId() == null) ? 0 : getSuiteId().hashCode());
        result = prime * result + ((getSuiteSecret() == null) ? 0 : getSuiteSecret().hashCode());
        result = prime * result + ((getToken() == null) ? 0 : getToken().hashCode());
        result = prime * result + ((getEncodingaeskey() == null) ? 0 : getEncodingaeskey().hashCode());
        result = prime * result + ((getSuiteName() == null) ? 0 : getSuiteName().hashCode());
        result = prime * result + ((getSuiteLogoname() == null) ? 0 : getSuiteLogoname().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", suiteId=").append(suiteId);
        sb.append(", suiteSecret=").append(suiteSecret);
        sb.append(", token=").append(token);
        sb.append(", encodingaeskey=").append(encodingaeskey);
        sb.append(", suiteName=").append(suiteName);
        sb.append(", suiteLogoname=").append(suiteLogoname);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}