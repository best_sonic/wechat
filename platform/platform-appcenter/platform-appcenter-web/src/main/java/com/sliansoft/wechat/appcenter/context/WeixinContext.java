package com.sliansoft.wechat.appcenter.context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 用于在内存中存放数据
 * @author david
 *
 */
public class WeixinContext {
	private static String accessToken = "UOnoMbQZrr504bSH1wt8q-cxb83f_ygyFoVJsH7UDxbq855cliBsfpNr2X9dp_LexaUirgWhet_KZnp-6a_ZRg";
	private static String suiteAccessToken = "MMm-HWyfFCDm-V5xgPigOQ_ygGVMdzx_Mps7QiuU5MgcGEaN8kPNB3T9Yshftcwj";
	private static String preAuthCode = "MMm-HWyfFCDm-V5xgPigOd4eftUqwnIb_WD28hsELC7olps_8AWXGV_G3oBGBRoU";
	private static String permanentCode = "IGCx1yc47Ld0M7kltx86Tc8_2wQCzRRS80WdKADgTSB3-GrPAwNC6sJEm1TPDFi_";
	private static Map<String,String> suiteTicketMap = new HashMap<String,String>();

	/*static{
		String baseUrl="C:\\Users\\demon\\git\\weixin-mz\\weixin-web\\src\\main\\resources";
//		String baseUrl="C:\\david_data\\git\\weixin-mz\\weixin-web\\src\\main\\resources";
		int num=baseUrl.indexOf("WEB-INF");
		//System.out.println(baseUrl.substring(1,num-1).replace('/', '\\')+"\\src\\main\\resources\\properties.properties");
		try {
			FileInputStream inputStream = new FileInputStream(baseUrl + "\\properties.properties");
			Properties p = new Properties();
			p.load(inputStream);
			Enumeration en = p.propertyNames();
			while (en.hasMoreElements()) {
				String key = (String) en.nextElement();
				String Property = p.getProperty(key);
				suiteTicketMap.put(key, Property);
				// System.out.println(key+"++++++++++"+Property);
			}
			inputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}*/
	
/*	public static Map<String, String> getSuiteTicketMap() {
		return suiteTicketMap;
	}

	public static void setSuiteTicketMap(Map<String, String> suiteTicketMap) {
		WeixinContext.suiteTicketMap = suiteTicketMap;
	}*/
	
	public static void setSuiteTicket(String suite_id, String suiteTicket) {
		suiteTicketMap.put(suite_id, suiteTicket);
		//storeStr2Properties(suite_id, suiteTicket);
	}
	
	public static String getSuiteTicket(String suiteId) {
		return suiteTicketMap.get(suiteId);
	}

	public static String getPermanentCode() {
		return WeixinContext.permanentCode;
	}

	public static void setPermanentCode(String permanentCode) {
		WeixinContext.permanentCode = permanentCode;
		//storeStr2Properties("permanentCode", permanentCode);
	}

	public static String getPreAuthCode() {
		return WeixinContext.preAuthCode;
	}

	public static void setPreAuthCode(String preAuthCode) {
		WeixinContext.preAuthCode = preAuthCode;
	}

	public static String getSuiteAccessToken() {
		return WeixinContext.suiteAccessToken;
	}

	public static void setSuiteAccessToken(String suiteAccessToken) {
		WeixinContext.suiteAccessToken = suiteAccessToken;
	}

/*	public static String getSuiteTicket() {
		return WeixinContext.suiteTicket;
	}

	public static void setSuiteTicket(String suiteTicket) {
		WeixinContext.suiteTicket = suiteTicket;
		storeStr2Properties("suiteTicket", suiteTicket);
	}*/

	public static void setAccessToken(String accessToken) {
		WeixinContext.accessToken = accessToken;
		//storeStr2Properties("accessToken", accessToken);
		//System.out.println("保存accessToken：" + accessToken);
	}

	public static String getAccessToken() {
		return WeixinContext.accessToken;
	}
	
	
	
	/**
	 * 保存str到properties文件
	 * @param key
	 * @param value
	 */
	/*public static void storeStr2Properties(String key, String value) {
		try {
			
			//String baseUrl =Thread.currentThread().getContextClassLoader().getResource("").getPath();
			String baseUrl="C:\\Users\\demon\\git\\weixin-mz\\weixin-web\\src\\main\\resources";
//			String baseUrl="C:\\david_data\\git\\weixin-mz\\weixin-web\\src\\main\\resources";
			//int num=baseUrl.indexOf("WEB-INF");
			//System.out.println(baseUrl.substring(1,num-1).replace('/', '\\')+"\\properties.properties");
			FileInputStream inputStream = new FileInputStream(baseUrl+"\\properties.properties");
			Properties p = new Properties();
			p.load(inputStream);
			p.setProperty(key, value);
			System.out.println("保存"+key+":"+value);
			inputStream.close();
			FileOutputStream outputStream = new FileOutputStream(baseUrl+"\\properties.properties");
			p.store(outputStream, null);
			outputStream.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}*/

	

}
