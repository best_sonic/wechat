package com.sliansoft.wechat.appcenter.quartz;

import java.util.List;
import javax.inject.Inject;

import com.sliansoft.wechat.appcenter.kit.WeixinKit;
import com.sliansoft.wechat.appcenter.model.CorpToken;
import com.sliansoft.wechat.appcenter.model.SuiteWithBLOBs;
import com.sliansoft.wechat.appcenter.service.ICorpTokenService;
import com.sliansoft.wechat.appcenter.service.ISuiteService;

public class WeixinQuartzJob {

	@Inject
	private ICorpTokenService corpTokenService;
	
	@Inject
	private ISuiteService suiteService;

	protected void executeInternal() {

		/**
		 * 每隔2小时从数据库取出所有的corp_id、suite_id、permanent_code
		 * 及对应的access_token，并更新access_token
		 */
		//CorpToken corpToken = corpTokenService.selectByCorpIdAndSuiteId("wxa75fcc28f7f6401a", "tjabcb7acdfa6aa80e");
		List<CorpToken> corpTokenList = corpTokenService.seletAll();
		for (CorpToken corpToken : corpTokenList) {
			String corp_id = corpToken.getCorpId();
			String suite_id = corpToken.getSuiteId();
			String permanent_code = corpToken.getPermanentCode();
			SuiteWithBLOBs suite = suiteService.selectBySuiteId(suite_id);
			String access_token = WeixinKit.getAccessToken(permanent_code, corp_id, suite);
			corpToken.setAccessToken(access_token);
			corpTokenService.saveOrUpdateCorpToken(corpToken);
			System.out.println("更新" + corpToken.getCorpId() + "+" + corpToken.getSuiteId() +":"+ corpToken.getAccessToken());
			//可能需要给每个应用发数据，说明哪些企业号在用它，并将该企业的access_token发给这个应用
		}
	}

}
