package com.sliansoft.wechat.appcenter.context;

public class WeixinFinalValue {

	/**
	 * 创建菜单
	 */
	public final static String MENU_ADD = "https://qyapi.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN&agentid=AGENTID";
	
	/**
	 * 消息类型
	 */
	public final static String MSG_TEXT_TYPE = "text";
	public final static String MSG_IMAGE_TYPE = "image";
	public final static String MSG_VOICE_TYPE = "voice";
	public final static String MSG_VIDEO_TYPE = "video";
	public final static String MSG_SHORTVIDEO_TYPE = "shortvideo";
	public final static String MSG_LOCATION_TYPE = "location";
	public final static String MSG_EVENT_TYPE = "event";
	
	//这个地方需要注意，这个字段是应用的
	public final static String REDIRECT_DOMAIN = "qy.sliansoft.com";
	
	
	
	/**
	 * 获取应用套件令牌
	 */
	public final static String GET_SUITE_ACCESS_TOKEN = "https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token";
	
	/**
	 * 获取预授权码
	 */
	public final static String GET_PRE_AUTH_CODE = "https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code?suite_access_token=SUITE_ACCESS_TOKEN";
	
	/**
	 * 设置授权配置
	 */
	public final static String SET_AUTH_CONFIG = "https://qyapi.weixin.qq.com/cgi-bin/service/set_session_info?suite_access_token=SUITE_ACCESS_TOKEN";
	
	/**
	 * 获取永久授权码
	 */
	public final static String GET_PERMANENT_CODE = "https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token=SUITE_ACCESS_TOKEN";
	
	/**
	 * 获取企业号的授权信息
	 */
	public final static String GET_CORP_AUTH_INFO = "https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token=SUITE_ACCESS_TOKEN";
	
	/**
	 * 获取企业号应用
	 */
	public final static String GET_CORP_AGENT = "https://qyapi.weixin.qq.com/cgi-bin/service/get_agent?suite_access_token=SUITE_ACCESS_TOKEN";
	
	/**
	 * 设置企业号应用
	 */
	public final static String SET_CORP_AGENT = "https://qyapi.weixin.qq.com/cgi-bin/service/set_agent?suite_access_token=SUITE_ACCESS_TOKEN";
	
	/**
	 * 获取企业号access_token
	 */
	public final static String GET_CORP_ACCESS_TOKEN = "https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token=SUITE_ACCESS_TOKEN";
	
	/**
	 * 套件授权页
	 */
	public final static String APP_AUTH_PAGE = "https://qy.weixin.qq.com/cgi-bin/loginpage?suite_id=$suite_id$&pre_auth_code=$pre_auth_code$&redirect_uri=$redirect_uri$&state=$state$";

	/**
	 * 回调地址
	 */
	public final static String REDIRECT_URI = "http://qy.sliansoft.com/platform-appcenter-web/operateApp/suite_id";
	//public final static String REDIRECT_URI = "http://boomshakalaka520.6655.la/platform-appcenter-web/operateApp/suite_id";
	/**
	 * 菜单URL，通过此URL应用可以获得Code
	 */
	public final static String Get_CODE="https://open.weixin.qq.com/connect/oauth2/authorize?appid=CORPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect";
	
	/**
	 * 设置应用
	 */
	public final static String CONF_APP="https://qyapi.weixin.qq.com/cgi-bin/service/set_agent?suite_access_token=SUITE_ACCESS_TOKEN";
	/**
	 * 
	 */
	public final static String CONFIGURING_AUTH="https://qyapi.weixin.qq.com/cgi-bin/service/set_session_info?suite_access_token=SUITE_ACCESS_TOKEN";
}
