package com.sliansoft.wechat.appcenter.controller;

import java.util.List;
import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sliansoft.wechat.appcenter.model.AppWithBLOBs;
import com.sliansoft.wechat.appcenter.model.SuiteWithBLOBs;
import com.sliansoft.wechat.appcenter.service.IAppService;
import com.sliansoft.wechat.appcenter.service.ISuiteService;

@Controller
@RequestMapping("/suiteAndApp")
public class SuiteAndAppController {
	
	@Inject
	private ISuiteService suiteService;
	
	@Inject
	private IAppService appService;
	
	/**
	 * 跳转到套件展示页
	 * @param model
	 * @return
	 */
	@RequestMapping("/showSuite/{suite_id}")
	public String showSuite(@PathVariable String suite_id,Model model){
		List<SuiteWithBLOBs> suiteList = suiteService.seletAll();//取出所有的suite
		SuiteWithBLOBs suite = suiteService.selectBySuiteId(suite_id);//取出需要展示的suite
		List<AppWithBLOBs> appList = appService.selectBySuiteId(suite_id);
		model.addAttribute("suiteList", suiteList);
		model.addAttribute("suite", suite);
		model.addAttribute("appList", appList);
		for(int i=1;i<suiteList.size()+1;i++){
			model.addAttribute("appList"+i, appService.selectBySuiteId(suiteList.get(i-1).getSuiteId()));//取出每个套件中的应用list
		}
		return "suiteIntroPage";
	}
	
	
	@RequestMapping("/showApp/{suite_id}/{app_id}")
	public String showApp(@PathVariable("suite_id") String suite_id, @PathVariable("app_id") String app_id, Model model){
		List<SuiteWithBLOBs> suiteList = suiteService.seletAll();//取出所有的suite
		SuiteWithBLOBs suite = suiteService.selectBySuiteId(suite_id);//取出app所属的套件
		AppWithBLOBs app = appService.selectBySuiteIdAndAppId(suite_id, Integer.parseInt(app_id));//取出需要展示的app
		model.addAttribute("suiteList", suiteList);
		model.addAttribute("suite", suite);
		model.addAttribute("app", app);
		for(int i=1;i<suiteList.size()+1;i++){
			model.addAttribute("appList"+i, appService.selectBySuiteId(suiteList.get(i-1).getSuiteId()));//取出每个套件中的应用list
		}
		return "appIntroPage";
	}
	
	
	/**
	 * 显示数据库中所有的套件与应用名称
	 * @param model
	 * @return
	 */
	@RequestMapping("/list")
	public String list(Model model){
		List<SuiteWithBLOBs> suiteList = suiteService.seletAll();
		model.addAttribute("suiteList", suiteList);
		for(int i=1;i<suiteList.size()+1;i++){
			model.addAttribute("appList"+i, appService.selectBySuiteId(suiteList.get(i-1).getSuiteId()));
		}
		return "listSuiteAndApp";
	}
	
	
}
