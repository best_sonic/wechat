package com.sliansoft.wechat.appcenter.exception;

public class SuiteException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4717849622224590300L;

	public SuiteException() {
		// TODO Auto-generated constructor stub
	}

	public SuiteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SuiteException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SuiteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SuiteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
