
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>套件授权安装</title>
<meta name="description" content="数联云平台销售管理套件授权安装">
<meta name="keywords" content="数联云平台销售管理套件授权安装">
</head>
<style type="text/css">
    *{ margin:0; padding: 0}
    body{ font:14px 微软雅黑; color: #666; line-height: 1.7; background:#4990e2 url("../../static/images/bg.jpg") repeat center top; }
    h1{ font-size: 20px; color: #1a1917; font-weight: 500; }
    ul{ list-style: none}
    img{border-radius: 5px;}
    a{ text-decoration: none; color: #666}
    a:hover{text-decoration: underline;}
    .center{ text-align: center}
    .ohidden{ overflow: hidden; }
    .mt20{ margin-top: 20px; }
    .pb40{ padding-bottom: 40px; }
    .fr{ float: right; }
    .wrap{ width: 680px; margin:40px auto; background: #fff; -webkit-box-shadow: 0 2px 4px rgba(0,0,0,.4); -moz-box-shadow: 0 2px 4px rgba(0,0,0,.4); box-shadow: 0 2px 4px rgba(0,0,0,.4); }
    .main{ width: 600px; margin:0 auto; }
    .logo{ padding-top: 60px; }
    .logo img{ width: 80px; height: 80px; }
    .btn { height: 42px; width: 320px; margin: 65px auto 10px; background-color: #4a90e2; display: block; text-align: center; line-height: 42px; color: white; text-decoration: none; font-size: 16px; cursor: pointer; }
    .c4a90e3{ color: #4a90e3}
    .list a { display: block; width: 60px; float: left; margin-right: 20px; }
    .list img { width: 60px; height: 60px; vertical-align: middle; }
    .info { margin-top: 30px; border-top: 1px solid #ddd; padding-top: 10px; margin-bottom: 15px; }
    .btn:hover { background: #4281CA;text-decoration:none }
</style>
<body>
	<div class="wrap">
		<div class="main">
			<div class="logo center">
				<img src="http://qy.sliansoft.com/static/upload/suitelogo/${suite.suiteLogoname }"
					alt="我是套件" width="80px" height="80px" />
			</div>
			<h1 class="center">授权${suite.suiteName }套件托管你的应用</h1>

			<div class="mt20">
				<p align="center">${suite.suiteIntroduction }</p>
			</div>
			<!-- <a onclick="submit()" class="btn">立即授权安装</a> -->
			<a class="btn"
				href="https://qy.weixin.qq.com/cgi-bin/loginpage?suite_id=${suite_id}&pre_auth_code=${pre_auth_code }&redirect_uri=${redirect_uri }&state=${state }">立即授权安装</a>
				<br><br>
			<!-- <p class="center">
				授权后表明你已同意<a target="_blank"
					href="http://qy.do1.com.cn/qwy/qiweipublicity/experience2/service.html"
					class="c4a90e3">《服务条款》</a>
			</p> -->
			<!-- <p class="info ohidden">您还可<span style="color:#f05454">免费</span>安装以下套件：<a target="_blank" href="http://wbg.do1.com.cn/suiteps/question/st_help.html" class="c4a90e3 fr">套件安装说明 > </a> </p>
<div class="list ohidden pb40">
    
    <a href="javascript:togo('tj0663c8afd643ba9e');"><img src="images/ic3.png" alt="工作协同" />工作协同</a>
    <a href="javascript:togo('tj18e98bf29b3e0a15);"><img src="images/ic4.png" alt="资源管理" />资源管理</a>
    <a href="javascript:togo('tjf7e6b5acbc7a715e');"><img src="images/ic5.png" alt="企业文化" />企业文化</a>
    <a href="javascript:togo('tj3adffe72e54c0f4c');"><img src="images/ic1.png" alt="移动办公" />移动办公</a>
    <a href="javascript:togo('tj7412c2fd27db139d');"><img src="images/ic6.png" alt="消息服务" />消息服务</a>
</div> -->
		</div>
	</div>
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery.min.js"></script>
    <script type="text/javascript">

  /*   function submit(){

        var args = document.location.toString().split('?');
         var param=args[1].replace("code","suiteId");
         $.ajax({
            type:"post",
            url:"http://qy.do1.com.cn/qwy/portal/experienceapplication/expappAction!addClickCount.action?"+param,
            dataType:'json'
         });
        window.location.href="http://qy.do1.com.cn/qwy/portal/weixin/weixinclientAction!authSuite.action?"+param;
            var code="";
            var args = document.location.toString().split('?');
             var param = args[1].split('&');
             for (var i = 0; i < param.length; i ++) {
                 if (param[i].split('=')[0] == "code")
                    code= decodeURI(args[1].split('=')[1]);
             }
             $.ajax({
                type:"post",
                url:"http://qy.do1.com.cn/qwy/portal/experienceapplication/expappAction!addClickCount.action?suiteId="+code,
                dataType:'json'
             });
            window.location.href="http://qy.do1.com.cn/qwy/portal/weixin/weixinclientAction!authSuite.action?suiteId="+code;
            
       
    }
    function togo(code){
        var args = document.location.toString().split('?');
        var param=args[1].replace("tjf7e6b5acbc7a715e",code).replace("tj3adffe72e54c0f4c",code).replace("tjd2ac85eb412f2081",code).replace("tj0663c8afd643ba9e",code).replace("tj7412c2fd27db139d",code);
        if(code=="tjf7e6b5acbc7a715e"){
            window.location.href="http://qy.do1.com.cn/qwy/qiweipublicity/experience2/step_suite_xxcj.html?"+param;
        }
        else if(code=="tj3adffe72e54c0f4c"){
            window.location.href="http://qy.do1.com.cn/qwy/qiweipublicity/experience2/step_suite_wxbg.html?"+param;
        }
        else if(code=="tjd2ac85eb412f2081"){
            window.location.href="http://qy.do1.com.cn/qwy/qiweipublicity/experience2/step_suite_xsgl.html?"+param;
        }
        else if(code=="tj0663c8afd643ba9e"){
            window.location.href="http://qy.do1.com.cn/qwy/qiweipublicity/experience2/step_suite_gzxt.html?"+param;
        }
        else if(code=="tj7412c2fd27db139d"){
            window.location.href="http://qy.do1.com.cn/qwy/qiweipublicity/experience2/step_suite_xxfw.html?"+param;
        }
    } */
</script>
</body>
</html>