<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:forEach items="appList" var="app">
	<li>
		<div class="appbox3">
			<div class="appbox4">
				<span class="icon_app_60" style="background: url("../images/${app.appLogoname }") no-repeat scroll 0px center #fff;">
				</span> <span class="appname"
					title="应用">${app.appName }</span>
			</div>
		</div>
	</li>
</c:forEach>
<!-- <li>
	<div class="appbox3">
		<div class="appbox4">
			<span class="icon_app_60"></span> <span class="appname"
				title="我是应用2">我是应用2</span>
		</div>
	</div>
</li>
<li>
	<div class="appbox3">
		<div class="appbox4">
			<span class="icon_app_60"></span> <span class="appname"
				title="我是应用3">我是应用3</span>
		</div>
	</div>
</li>
<li>
	<div class="appbox3">
		<div class="appbox4">
			<span class="icon_app_60"></span> <span class="appname"
				title="我是应用3">我是应用3</span>
		</div>
	</div>
</li>
<li>
	<div class="appbox3">
		<div class="appbox4">
			<span class="icon_app_60"></span> <span class="appname"
				title="我是应用3">我是应用3</span>
		</div>
	</div>
</li> -->
<li>
	<div class="appbox3">
		<div class="appbox4">
			<span class="icon_add_60"></span> <span class="appname" title="添加应用">添加应用</span>
		</div>
	</div>
</li>
