<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数联软件</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/static/manager/css/style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/static/manager/css/suiteandapp.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/static/manager/css/style-suite_store.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/static/manager/uploadify/uploadify.css">
<script
	src='${pageContext.request.contextPath }/static/manager/js/jquery-1.10.2.min.js'></script>
<script
	src='${pageContext.request.contextPath }/static/manager/js/jquery.validate.js'></script>
<script
	src='${pageContext.request.contextPath }/static/manager/js/jquery.app.validate.js'></script>
<script
	src='${pageContext.request.contextPath }/static/manager/uploadify/jquery.uploadify.min.js'></script>
<script 
	src='${pageContext.request.contextPath }/static/manager/js/jquery.suiteandapp.core.js'></script>
<script type="text/javascript">
$(function() {
	var menutwonum = 0;
	var menuonenum = 0;
	var ctstatus = 0;
	var ntstatus = 0;
	
	if("" != $("#menuonetype").val()){
		$("#menuoneurl").removeAttr("disabled");
		$("#menuonekey").removeAttr("disabled");
		$("#btn_next_two").unbind("click");
		$("#btn_next_two").css("color","#fff").css("background-color", "gray")
						.css("border", "1px solid #4a90e2");
	//	$("#btn_new_one_without_two").bind("click", next_two);
		$("#btn_new_one_without_two").css("color","#fff").css("background-color", "#4a90e2")
						.css("border", "1px solid #4a90e2");
		ntstatus = 1;
	}else{
		$("#menuonekey").attr("disabled","disabled");
		$("#menuoneurl").attr("disabled","disabled");
		$("#menuonekey").val("");
		$("#menuonekey").val("");
		if(ntstatus == 0){
		}else{
			$("#btn_next_two").bind("click", next_two);
				$("#btn_next_two").css("color","#fff").css("background-color", "#4a90e2")
								.css("border", "1px solid #4a90e2");
			//	$("#btn_new_one_without_two").unbind("click");
				$("#btn_new_one_without_two").css("color","#fff").css("background-color", "gray")
								.css("border", "1px solid #4a90e2");
				ntstatus = 0;
		}
	}
	
	
	/* 表单验证函数 */
	appValidate();
	menuoneValidate();
	menutwoValidate();
	/* 表单验证函数 */
	
	$("#menuonetype").change(function(){
		
		if("" != $("#menuonetype").val()){
			$("#menuoneurl").removeAttr("disabled");
			$("#menuonekey").removeAttr("disabled");
			$("#btn_next_two").unbind("click");
			$("#btn_next_two").css("color","#fff").css("background-color", "gray")
							.css("border", "1px solid #4a90e2");
			
		//	$("#btn_new_one_without_two").bind("click", next_two);
			$("#btn_new_one_without_two").css("color","#fff").css("background-color", "#4a90e2")
							.css("border", "1px solid #4a90e2");
			
			ntstatus = 1;
		}else{
			$("#menuonekey").attr("disabled","disabled");
			$("#menuoneurl").attr("disabled","disabled");
			$("#menuonekey").val("");
			$("#menuonekey").val("");
			if(ntstatus == 0){
			}else{
				$("#btn_next_two").bind("click", next_two);
				$("#btn_next_two").css("color","#fff").css("background-color", "#4a90e2")
								.css("border", "1px solid #4a90e2");
		//		$("#btn_new_one_without_two").unbind("click");
				$("#btn_new_one_without_two").css("color","#fff").css("background-color", "gray")
								.css("border", "1px solid #4a90e2");
				ntstatus = 0;
			}
		}
	});
	
	$("#menutwotype").change(function(){
		
		if("view" == $("#menutwotype").val()){
			$("#menutwourl").removeAttr("disabled");
			$("#menutwokey").val("");
			$("#menutwokey").attr("disabled","disabled");
		}else{
			$("#menutwokey").removeAttr("disabled");
			$("#menutwourl").val("");
			$("#menutwourl").attr("disabled","disabled");
		}
	});
	
	
	$("#upload_org_code")
			.uploadify(
					{
						'height' : 27,
						'width' : 64,
						'buttonText' : '选择图片',
						'swf' : '${pageContext.request.contextPath}/static/manager/uploadify/uploadify.swf',
						'uploader' : "${pageContext.request.contextPath}/manager/uploadify?type=app",
						'auto' : true,
						'multi' : false,
						'removeCompleted' : false,
						'cancelImg' : '${pageContext.request.contextPath}/static/manager/uploadify/uploadify-cancel.png',
						'fileTypeExts' : '*.jpg;*.jpge;*.png',
						'fileSizeLimit' : '5MB',
						'onUploadSuccess' : function(file, data, response) {
							$('#' + file.id).find('.data').html('');
							$("#upload_org_code_name").val(data);
							$("#upload_org_code_img").attr(
									"src",
									"${pageContext.request.contextPath}/manager/getimage?file="
											+ data + "&type=app");
							$("#upload_org_code_img").show();
						},
						//加上此句会重写onSelectError方法【需要重写的事件】
						'overrideEvents' : [ 'onSelectError',
								'onDialogClose' ],
						//返回一个错误，选择文件的时候触发
						'onSelectError' : function(file, errorCode,
								errorMsg) {
							switch (errorCode) {
							case -110:
								alert("文件 ["
										+ file.name
										+ "] 大小超出系统限制的"
										+ jQuery('#upload_org_code')
												.uploadify('settings',
														'fileSizeLimit')
										+ "大小！");
								break;
							case -120:
								alert("文件 [" + file.name + "] 大小异常！");
								break;
							case -130:
								alert("文件 [" + file.name + "] 类型不正确！");
								break;
							}
						},
					});
	
	$("#btn_next_one").click(function() {
		if($("#app_add_form").valid() == false){
		}else{
			$("#appinfo").css("display", "none");
			$("#menuoneinfo").css("display", "block");
			
			var app = {
				appid : $("#appid").val(),
				appname : $("#appname").val(),
				callbackurl : $("#callbackurl").val(),
				appdescription : $("#appdescription").val().trim(),
				appscene : $("#appdescription").val().trim(),
				applogoname : $("#upload_org_code_name").val(),
				suiteId : $("#suiteId").val(),
			};
			var jsonStr = JSON.stringify(app);
			$.ajax({
				type : "POST",
				url : "${pageContext.request.contextPath}/manager/addApp",
				data : jsonStr,
				success : function(data) {
					$("#return_app_id").val(data);
				}
			});
		}
	});
	
	function next_two() {
		if($("#menuone_add_form").valid() == false){
		}else{
			$("#menuoneinfo").css("display", "none");
			$("#menutwoinfo").css("display", "block");
			
			
			var menuone = {
					menuonename:$("#menuonename").val(),
					menuonetype:$("#menuonetype").val(),
					menuoneurl:$("#menuoneurl").val(),
					menuonekey:$("#menuonekey").val(),
					pre_app_id:$("#return_app_id").val()
			};
			var jsonStr = JSON.stringify(menuone);
			$.ajax({
				type : "POST",
				url : "${pageContext.request.contextPath}/manager/addMenuone",
				data : jsonStr,
				success : function(data) {
					$("#btn_continue_two").css("color","#fff").css("background-color", "#4a90e2")
										.css("border", "1px solid #4a90e2");
					$("#menuone_id").val(data);
					$("#menutwoname").val("");
					$("#menutwotype").val("");
					$("#menutwourl").val("");
					$("#menutwokey").val("");
				}
			});
		}
	} 
	
	$("#btn_new_one_without_two").click(function() {
		if($("#menuone_add_form").valid() == false){
		}else{
			menuonenum++;
			if(menuonenum == 3){
				
				$("#btn_new_one_without_two").css("color","#fff").css("background-color", "gray")
											.css("border", "1px solid #4a90e2");
				$("#btn_new_one_without_two").unbind("click");
				$("#btn_next_two").css("color","#fff").css("background-color", "gray")
								.css("border", "1px solid #4a90e2");
				$("#btn_next_two").unbind("click");
			}
			var menuone = {
					menuonename:$("#menuonename").val(),
					menuonetype:$("#menuonetype").val(),
					menuoneurl:$("#menuoneurl").val(),
					menuonekey:$("#menuonekey").val(),
					pre_app_id:$("#return_app_id").val()
			};
			var jsonStr = JSON.stringify(menuone);
			
			if(menuonenum<4){
				$("#menu_num_toAdd").html("该应用已经安装"+menuonenum+"个一级菜单");
				$("#menu_num_toAdd").css("color","green");
				if(menuonenum == 3){
					$("#menu_num_toAdd").html("一个应用只能安装3个一级菜单");
					$("#menu_num_toAdd").css("color","red");
				}
				$.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/manager/addMenuone",
					data : jsonStr,
					success : function(data) {
						$("#menuone_id").val(data);
						$("#menuonename").val("");
						$("#menuonetype").val("");
						$("#menuoneurl").val("");
						$("#menuonekey").val("");
					}
				});
			}
		}
	});
	
	
	function continue_two(){
		if($("#menutwo_add_form").valid() == false){
		}else{
			$("#menuoneinfo").css("display", "none");
			$("#menutwoinfo").css("display", "block");
			menutwonum++;
			if(menutwonum==5){
				$("#btn_continue_two").css("color","#fff").css("background-color", "gray")
									.css("border", "1px solid #4a90e2");
				$("#btn_continue_two").unbind("click");
				ctstatus = 1;
			}
			var menutwo = {
					menutwoname:$("#menutwoname").val(),
					menutwotype:$("#menutwotype").val(),
					menutwourl:$("#menutwourl").val(),
					menutwokey:$("#menutwokey").val(),
					menuoneid:$("#menuone_id").val()
			};
			var jsonStr = JSON.stringify(menutwo);
			
			if(menutwonum<6){
				$("#menu_num_toAdd").html("该一级菜单已经安装"+menutwonum+"个二级菜单");
				$("#menu_num_toAdd").css("color","green");
				$.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/manager/addMenutwo",
					data : jsonStr,
					success : function(data) {
					//	$("#menuone_id").val(data);
						$("#menutwoname").val("");
						$("#menutwotype").val("");
						$("#menutwourl").val("");
						$("#menutwokey").val("");
					}
				});
				if(menutwonum==5){
					$("#menu_num_toAdd").html("该一级菜单已经安装只能安装5个二级菜单");
					$("#menu_num_toAdd").css("color","red");
				}
			}
		}						
		}
	
	$("#btn_continue_two").bind("click", continue_two);
	
	$("#btn_new_one").click(function() {
		
			$("#menuoneinfo").css("display", "block");
			$("#menutwoinfo").css("display", "none");
			menuonenum++;
			menutwonum=0;
			if(ctstatus == 0 ){ 
				
			}else{
				$("#btn_continue_two").bind("click", continue_two); 
				ctstatus = 0;
			}
			if(menuonenum == 3){
				$("#menu_num_toAdd").html("一个应用只能安装3个一级菜单");
				$("#menu_num_toAdd").css("color","red");
				$("#btn_new_one").css("color","#fff").css("background-color", "gray")
								.css("border", "1px solid #4a90e2");
				$("#btn_next_two").css("color","#fff").css("background-color", "gray")
								.css("border", "1px solid #4a90e2");
				$("#btn_next_two").unbind("click"); 
				$("#btn_new_one_without_two").unbind("click"); 
				$("#btn_new_one_without_two").css("color","#fff").css("background-color", "gray")
											.css("border", "1px solid #4a90e2");
			}
			if(menuonenum<3){
				$("#menu_num_toAdd").html("该应用已经安装"+menuonenum+"个一级菜单");
				$("#menu_num_toAdd").css("color","green");
				
				$("#menuonename").val("");
				$("#menuonetype").val("");
				$("#menuoneurl").val("");
				$("#menuonekey").val("");
			}
	});
	
		String.prototype.trim = function() {
			return this.replace(/^\s*/, "").replace(/\s*S/, "");
		};
});
	
</script>
</head>
<body>
	<div class="wrap-header">
		<div id="header">
			<div class="inner-header">
				<div class="logoNav clearfix">
					<a href="http://wbg.do1.com.cn" id="logo" target="_blank"> <img
						id="headLogo" style="margin-top: 20px; width:250px;" alt="logo"
						src="${pageContext.request.contextPath }/static/images/logoManager.png">
					</a>
					<div id="login_text">
						<span id="userOrgName">欢迎你使用数联云平台应用</span> <a class="ml10" href="#">前台</a>
					</div>
				</div>
				<div id="nav">
					<ul class="inline-float">
						<li style="margin-left: 530px" permission="" id="addSuite"><a
							href="#">申请注册应用</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap-container">
		<div id="container" class="clearfix">
			<div class="bindingAPP">
				<div class="apptop" style="text-align: center;">
					<div class="apptop-tit"
						style="font-size: 20px; font-weight: 700; margin: 20px 20px 10px">应用中心</div>
					<div class="apptop-lead" >
						<p id="menu_num_toAdd">应用需在企业号进行托管才能正常使用</p>
					</div>
				</div>
			</div>
			<div id="suite">
				<input type="hidden" id="ctx" value="<%=request.getContextPath()%>" />
				<input type="hidden" id="suiteId" value="${suiteId }"> 
				<!-- 应用表单 -->
				<div style="width: 320px; margin: 0 auto" id="appinfo"
					class="appinfo">
					<div class="mod-process mod-process_two"
						style="width: 640px; margin: 0 auto">
						<div
							class="mod-process__item mod-process__item_first mod-process__item_current"
							style="margin-left: -80px">1.填写应用信息</div>
						<div
							class="mod-process__item mod-process__item_last mod-process__item_current-next">
							1.填写一级菜单信息</div>
						<div
							class="mod-process__item mod-process__item_last mod-process__item_current-next">
							2.填写二级菜单信息</div>
					</div>
					
					<form action="#" id="app_add_form" method="post">
						<div class="mod-set-pwd__form"
							style="margin-top: 40px; margin-bottom: 30px">
							<div class="mod-set-pwd__form-item js_upload_parent">
								<tr>
									<td align="right"><font style="color: red;">*</font>应用logo：</td>
									<td>
										<table>
											<tr>
												<td width="45%"><input type="file"
													name="upload_org_code" id="upload_org_code" /></td>
												<td><img style="display: none" id="upload_org_code_img"
													src="" width="80" height="80"></td>
											</tr>
										</table> <input type="hidden" name="upload_org_code_name"
										id="upload_org_code_name" />
										<hr>
									</td>
								</tr>
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">应用app_id </label> 
									<input type="text" id="appid" class="input-text mod-set-pwd__form-input js_rules" name="id">
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">callbackurl </label> 
								<input type="text" id="callbackurl" class="input-text mod-set-pwd__form-input js_rules" name="url">
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small"> 应用名称</label> 
							<input type="text" id="appname" class="input-text mod-set-pwd__form-input js_rules"
									name="appname">
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small"> 应用介绍</label>
								<textarea name="description" id="appdescription" class="input-text mod-set-pwd__form-input js_rules">
	                              </textarea>
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small"> 应用场景</label>
								<textarea name="scene" id="appscene" class="input-text mod-set-pwd__form-input js_rules">
	                                    </textarea>
								<span> </span>
							</div>
						</div>
					<a href="javascript:;" id="btn_next_one" 
						class="ui-mb-small button button_block button-primary js_step_next">
						下一步 
					</a>
				</form>
				</div>

				<!-- 一级菜单表单 -->
				<div style="width: 320px; margin: 0 auto; display: none;"
					id="menuoneinfo" class="appinfo">
					<form action="#" id="menuone_add_form" method="post">
						<div class="mod-process mod-process_two"
							style="width: 640px; margin: 0 auto">
							<div
								class="mod-process__item mod-process__item_first mod-process__item_current-next"
								style="margin-left: -80px">1.填写应用信息</div>
							<div
								class="mod-process__item mod-process__item_last mod-process__item_current">
								1.填写一级菜单信息</div>
							<div
								class="mod-process__item mod-process__item_last mod-process__item_current-next">
								2.填写二级菜单信息</div>
						</div>
						<div class="mod-set-pwd__form"
							style="margin-top: 40px; margin-bottom: 30px">
							<input type="hidden" id="return_app_id">
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									一级菜单名称 </label> <input type="text" id="menuonename" name="menuonename"
									class="input-text mod-set-pwd__form-input js_rules"> 
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									一级菜单类型 </label> 
									<select id="menuonetype" name="menuonetype" class="input-text mod-set-pwd__form-input js_rules">
										<option value="">需要二级菜单选这个</option>
										<option value="click">click</option>
										<option value="view" selected="selected">view</option>
										<option value="scancode_push">scancode_push</option>
										<option value="scancode_waitmsg">scancode_waitmsg</option>
										<option value="pic_sysphoto">pic_sysphoto</option>
										<option value="pic_photo_or_album">pic_photo_or_album</option>
										<option value="pic_sysphoto">pic_sysphoto</option>
										<option value="pic_photo_or_album">pic_photo_or_album</option>
										<option value="pic_weixin">pic_weixin</option>
										<option value="location_select">location_select</option>
									 </select>
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									一级菜单url </label> <input type="text" id="menuoneurl" name="menuoneurl"
									class="input-text mod-set-pwd__form-input js_rules"> 
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									一级菜单key </label> <input type="text" id="menuonekey" name="menuonekey"
									class="input-text mod-set-pwd__form-input js_rules"> 
							</div>
						</div>
						<a href="javascript:;" id="btn_next_two" style="width:130px; float: right; font-size: 10px"
							class="ui-mb-small button button_block button-primary js_step_next">
							保存并创建二级菜单 
						</a>
						<a href="javascript:;" id="btn_new_one_without_two"
							class="ui-mb-small button button_block button-primary js_step_next" style="width:130px;font-size: 10px">
								保存并新建下个一级菜单
						</a>
						
						<a href="${pageContext.request.contextPath }/manager/list"
							class="ui-mb-small button button_block button-primary js_step_next">
								完成应用创建，回主页
						</a>
					</form>
				</div>

				<!-- 二级菜单表单 -->
				<div style="width: 320px; margin: 0 auto; display: none;"
					id="menutwoinfo" class="appinfo">
					<form action="#" id="menutwo_add_form" method="post">
						<div class="mod-process mod-process_two"
							style="width: 640px; margin: 0 auto">
							<div
								class="mod-process__item mod-process__item_first mod-process__item_current-next"
								style="margin-left: -80px">1.填写应用信息</div>
							<div
								class="mod-process__item mod-process__item_last mod-process__item_current-next">
								1.填写一级菜单信息</div>
							<div
								class="mod-process__item mod-process__item_last mod-process__item_current">
								2.填写二级菜单信息</div>
						</div>
						<div class="mod-set-pwd__form"
							style="margin-top: 40px; margin-bottom: 30px">
							
							<input type="hidden" id="menuone_id"/>
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									二级菜单名称 </label> <input type="text" id="menutwoname" name="menutwoname"
									class="input-text mod-set-pwd__form-input js_rules">
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									二级菜单类型 </label> <select id="menutwotype" name="menutwotype" class="input-text mod-set-pwd__form-input js_rules">
										<option value="click">click</option>
										<option value="view" selected="selected">view</option>
										<option value="scancode_push">scancode_push</option>
										<option value="scancode_waitmsg">scancode_waitmsg</option>
										<option value="pic_sysphoto">pic_sysphoto</option>
										<option value="pic_photo_or_album">pic_photo_or_album</option>
										<option value="pic_sysphoto">pic_sysphoto</option>
										<option value="pic_photo_or_album">pic_photo_or_album</option>
										<option value="pic_weixin">pic_weixin</option>
										<option value="location_select">location_select</option>
									 </select>
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									二级菜单url </label> <input type="text" id="menutwourl" name="menutwourl"
									class="input-text mod-set-pwd__form-input js_rules">
							</div>
	
							<div class="mod-set-pwd__form-item">
								<label class="mod-set-pwd__form-label ui-mb-small">
									二级菜单key </label> <input type="text" id="menutwokey" name="menutwokey"
									class="input-text mod-set-pwd__form-input js_rules">
							</div>
						</div>
						<a href="javascript:;" id="btn_continue_two"
							class="ui-mb-small button button_block button-primary js_step_next" style="width:130px;float:right; font-size: 10px">
								保存并继续创建二级菜单
						</a>
						<a href="javascript:;" id="btn_new_one"
							class="ui-mb-small button button_block button-primary js_step_next" style="width:130px;font-size: 10px ">
								新建下个一级菜单(不保存)
						</a>
						
						<a href="${pageContext.request.contextPath }/manager/list"
							class="ui-mb-small button button_block button-primary js_step_next">
								完成应用创建，回主页
						</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>