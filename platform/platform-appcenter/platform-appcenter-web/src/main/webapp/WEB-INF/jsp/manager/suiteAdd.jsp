<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数联软件</title>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/manager/css/style.css">
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/manager/css/suiteandapp.css">
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/manager/css/style-suite_store.css">
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/manager/uploadify/uploadify.css">
    <script src='${pageContext.request.contextPath }/static/manager/js/jquery-1.10.2.min.js'></script>
     <script src='${pageContext.request.contextPath }/static/manager/js/jquery.validate.js'></script>
    <script src='${pageContext.request.contextPath }/static/manager/js/jquery.suite.validate.js'></script>
    <script src='${pageContext.request.contextPath }/static/manager/uploadify/jquery.uploadify.min.js'></script>
    <script type="text/javascript">
    $(function() {
        $("#upload_org_code").uploadify({
            'height'        : 27,
            'width'         : 64, 
            'buttonText'    : '选择图片',
            'swf'           : '${pageContext.request.contextPath}/static/manager/uploadify/uploadify.swf',
            'uploader'      : "${pageContext.request.contextPath}/manager/uploadify?type=suite",
            'auto'          : true,
            'multi'         : false,
            'removeCompleted':false,
            'cancelImg'     : '${pageContext.request.contextPath}/static/manager/uploadify/uploadify-cancel.png',
            'fileTypeExts'  : '*.jpg;*.jpge;*.png',
            'fileSizeLimit' : '5MB',
            'onUploadSuccess':function(file,data,response){
                $('#' + file.id).find('.data').html('');
                $("#upload_org_code_name").val(data);
                $("#upload_org_code_img").attr("src","${pageContext.request.contextPath}/manager/getimage?file="+data+"&type=suite");  
                $("#upload_org_code_img").show();
            },
            //加上此句会重写onSelectError方法【需要重写的事件】
            'overrideEvents': ['onSelectError', 'onDialogClose'],
            //返回一个错误，选择文件的时候触发
            'onSelectError':function(file, errorCode, errorMsg){
                switch(errorCode) {
                    case -110:
                        alert("文件 ["+file.name+"] 大小超出系统限制的" + jQuery('#upload_org_code').uploadify('settings', 'fileSizeLimit') + "大小！");
                        break;
                    case -120:
                        alert("文件 ["+file.name+"] 大小异常！");
                        break;
                    case -130:
                        alert("文件 ["+file.name+"] 类型不正确！");
                        break;
                }
            },
        });
        
        $("#btn_submit").click(function(){
        	if($("#suite_add_form").valid() == false){
			}else{
	        	var suite = 
	        		{
	        			suiteid: $("#suiteid").val(),
	        			suitename:$("#suitename").val(),
	        			suitesecret:$("#suitesecret").val(),
						suitetoken:$("#suitetoken").val(),
						suiteencodingaeskey:$("#suiteencodingaeskey").val(),
						suitedescription:$("#suitedescription").val().trim(),
						suitescene:$("#suitescene").val().trim(),
						suitelogoname:$("#upload_org_code_name").val()
	        		};
	        	var jsonStr=JSON.stringify(suite);
	        	$.ajax({
	        		type:"POST",
	        		url:"${pageContext.request.contextPath}/manager/addSuite",
	        		data:jsonStr,
	        		success: function(data){
		        		alert("套件创建成功");
		        	}
	        	});
			}
        });
        
        String.prototype.trim = function(){
        	return this.replace(/^\s*/,"").replace(/\s*S/,"");
        };
        
        suiteValidate();
    })
    </script>
</head>
<body>
	<div class="wrap-header">
		<div id="header">
			<div class="inner-header">
				<div class="logoNav clearfix">
					<a href="http://wbg.do1.com.cn" id="logo" target="_blank">
                        <img id="headLogo" style="margin-top: 20px; width:250px;" alt="logo" src="${pageContext.request.contextPath }/static/images/logoManager.png">
                    </a>
					<div id="login_text">
						<span id="userOrgName" >欢迎你使用敏智微服应用</span>
						<a class="ml10" href="#">前台</a>
					</div>
				</div>
	
				<div id="nav" >
					<ul class="inline-float" >
						<li style="margin-left: 530px" permission="" 
							 id="addSuite"><a
							href="#">申请注册套件</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap-container">
        <div id="container" class="clearfix">
            <div class="bindingAPP">
              <div class="apptop" style="text-align: center;">
                <div class="apptop-tit" style="font-size: 20px; font-weight: 700; margin: 20px 20px 10px">套件中心</div>
                <div class="apptop-lead">应用需在企业号进行托管才能正常使用</div>               
              </div>
            </div>
			<div id="suite">
			<input type="hidden" id="ctx" value="<%=request.getContextPath() %>"/>
                        <div style="width:320px;margin:0 auto">
                           <form action="#" id="suite_add_form" method="post">
                            <div class="mod-set-pwd__form" style="margin-top:40px;margin-bottom:30px">
                                <div class="mod-set-pwd__form-item js_upload_parent">
                                    <!-- <div class="mod-appbundle__upload-avatar ui-ta-c">
                                        <div>
                                            <div class="mod-appbundle__upload-avatar-img">
                                                <img src="" alt="" width="80" height="80" class="js_img">
                                                <div class="mod-appbundle__upload-avatar-loading ui-dp-n js_upload_loading">
                                                </div>
                                            </div>
                                            <a href="javascript:" id="upload_img" style="vertical-align:bottom">
                                           	     上传
                                            </a>
                                        </div>
                                        <div class="mod-set-pwd__form-desc">
                                  		          小于5M，尺寸为640X640px。
                                        </div>
                                    </div>
                                    <input type="hidden" name="logo" class="js_logo input-text" >
                                    <span class="mod-set-pwd__form-error js_error_box ui-dp-n">
                               		         上传图片超过5M。
                                    </span> -->
									<tr>
							            <td align="right"><font style="color: red;">*</font>套件logo：</td>
							            <td>
							                <table>
							                    <tr>
							                        <td width="45%"><input type="file" name="upload_org_suite" id="upload_org_code"/></td>
							                        <td><img style="display: none" id="upload_org_code_img" src="" width="80" height="80"></td>
							                    </tr>
							                </table>
							                <input type="hidden" name="upload_org_suite_name" id="upload_org_code_name" />
							                <hr>
							            </td>
							        </tr>
						</div>
                                
                                <div class="mod-set-pwd__form-item">
                                    <label class="mod-set-pwd__form-label ui-mb-small">
                               		         应用套件id
                                    </label>
                                    <input type="text" id="suiteid" class="input-text mod-set-pwd__form-input js_rules"
                                    name="suiteid" >
                                </div>
                                
                                <div class="mod-set-pwd__form-item">
                                    <label class="mod-set-pwd__form-label ui-mb-small">
                                	        应用套件名称
                                    </label>
                                    <input type="text" id="suitename" class="input-text mod-set-pwd__form-input js_rules"
                                    name="suitename" >
                                </div>
                                
                                <div class="mod-set-pwd__form-item">
                                    <label class="mod-set-pwd__form-label ui-mb-small">
                                    	    应用套件secret
                                    </label>
                                    <input type="text" id="suitesecret"  class="input-text mod-set-pwd__form-input js_rules"
                                    name="secret" >
                                </div>
                                
                                <div class="mod-set-pwd__form-item">
                                    <label class="mod-set-pwd__form-label ui-mb-small">
             						  	Token
                                    </label>
                                    <input type="text" id="suitetoken" class="input-text mod-set-pwd__form-input js_rules"
                                    name="token" >
                                </div>
                                
                                <div class="mod-set-pwd__form-item">
                                    <label class="mod-set-pwd__form-label ui-mb-small">
              						 	EncodingAesKey
                                    </label>
                                    <input type="text" id="suiteencodingaeskey" class="input-text mod-set-pwd__form-input js_rules"
                                    name="encodingseskey" >
                                </div>
                                
                                <div class="mod-set-pwd__form-item">
                                    <label class="mod-set-pwd__form-label ui-mb-small">
                                    	    应用套件介绍
                                    </label>
                                    <textarea name="suitedescription" id="suitedescription" class="input-text mod-set-pwd__form-input js_rules">
                                    </textarea>
                                </div>
                                
                                <div class="mod-set-pwd__form-item">
                                    <label class="mod-set-pwd__form-label ui-mb-small">
                                    	    套件应用场景
                                    </label>
                                    <textarea name="suitescene" id="suitescene" class="input-text mod-set-pwd__form-input js_rules">
                                    </textarea>
                                </div>
                            </div>
                            <a href="javascript:;" id="btn_submit" class="ui-mb-small button button_block button-primary js_step_next">
                            	    提交
                            </a>
                           </form> 
                            <a href="${pageContext.request.contextPath}/manager/list" id="btn_return" class="ui-mb-small button button_block button-primary js_step_next">
                            	  返回主界面
                            </a>
                        </div>
                    </div>
			</div>
		</div>
</body>
</html>