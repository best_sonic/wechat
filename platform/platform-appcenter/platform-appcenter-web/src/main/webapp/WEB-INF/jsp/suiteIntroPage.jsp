<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="">
		<meta name="baidu-site-verification" content="Vup9CGlqbA" />
		<!-- 百度检测-->
		<!-- <meta name="msvalidate.01" content="6C48EF31F67FA6E09A742953ACAB5A39" /> -->
		<!-- 必应搜索-->
		<!-- <meta name="description"
			content="移动办公套件深度融合微信企业号，创造性的无缝对接移动办公系统，解决企业多组织间沟通协作、移动办公的需要，提升企业内部移动办公效率和管理水平。该套件集成了审批请示、会议助手、请假出差、微信考勤等应用。"> -->
		<meta name="keywords"
			content="微信企业号应用软件，微信企业号第三方应用，企业号应用中心,微信会议，微信请假应用，微信审批，移动审批应用，微信考勤，微信">
		<title>${suite.suiteName }-数联云平台</title>
		<link href="${pageContext.request.contextPath }/static/css/do1.css?v1.43" rel="stylesheet"
			media="screen" type="text/css" />
		<script language="javascript" type="text/javascript"
			src="${pageContext.request.contextPath }/static/js/jquery-1.11.1.min.js"></script>
		<script language="javascript" type="text/javascript"
			src="${pageContext.request.contextPath }/static/js/common.js?v1.43"></script>
		<script src=" http://hm.baidu.com/h.js?0d8c8e0c89f9c63e43a87823376c485e"
			type="text/javascript"></script>
	</head>
	<body class="applyBody applyBody2">
		<jsp:include page="header.jsp" />
		<div class="applyWrapBg">
			<div class="title">四大应用，轻松使用</div>
			<div class="container">
				<div class="more-news hidden">
					<jsp:include page="leftList.jsp" />
					<!-- 遮罩层 -->
					<div class="mask-box"></div>
					<div class="closed-container-box">
						<span class="close-btn"></span>
						<div class="anzhuan-contain add-azheight">
							<div class="anzhuan-box">
								<h1 class="anzhuan-tit aztpt">数联应用套件安装指引</h1>
								<div class="anzhuan-shixiang">
									<h1 class="azsx-tit">安装注意事项：</h1>
									<p>
										1、安装应用套件前，需要拥有 ”微信企业号”（<a href="https://qy.weixin.qq.com/"
											target="_blank">注册</a>）；<br>
										2、避免重复安装应用（每个套件包含多个应用，重复安装会导致错误）；<br> 3、使用 <a
											href="http://www.firefox.com.cn/" target="_blank">Firefox</a>、<a
											href="http://www.google.cn/chrome/browser/desktop/index.html"
											target="_blank">Chrome</a> 或相关内核的浏览器，体验更好，速度更快，同时避免安装过程失败；<br>
										4、安装应用后，需要在微信企业号后台<span> “设置应用的可见范围”</span>，手机端才能正常显示。
									</p>
								</div>
								<div class="anzhuan-step-box">
									<h1 class="anzhuan-step-tit">安装步骤：</h1>
									<p class="step-content">
										访问数联官网：<a href="${pageContext.request.contextPath }/suiteAndApp/showSuite/tj7a34e960e9abae07" target="_blank">数联云平台</a>
										，进入【应用与套件】页面，选择需要安装的应用或套件；
									</p>
									<div class="step-details">
										<img src="${pageContext.request.contextPath }/static/images/st_img01.png" alt="" />
									</div>
									<p class="step-content sc-bg2">在授权安装页面，点击“立即授权安装”；</p>
									<div class="step-details">
										<img src="${pageContext.request.contextPath }/static/images/st_img02.png" alt="" />
									</div>
									<p class="step-content sc-bg3">进入微信企业号授权页面，用微信扫描二维码，并输入二维码登录；</p>
									<div class="step-details">
										<img src="${pageContext.request.contextPath }/static/images/st_img03.png" alt="" />
										<img src="${pageContext.request.contextPath }/static/images/st_img04.png" alt="" />
									</div>
									<p class="step-content sc-bg4">登录成功后在以下页面，点击图中“套件应用”下的“添加”按钮；</p>
									<div class="step-details">
										<img src="${pageContext.request.contextPath }/static/images/st_img05.png" alt="" />
										<p>
											①<span>在弹窗里勾选上所有应用；</span>
										</p>
										<img src="${pageContext.request.contextPath }/static/images/st_img06.png" alt="" />
										<p>
											②<span>点击“可见范围”下方的“设置”按钮，在弹出的窗口勾选可用该套件的部门；</span>
										</p>
										<img src="${pageContext.request.contextPath }/static/images/st_img07.png" alt="" />
										<p class="beizhu-col">备注1：如出现“企业号应用总数已达到上限”的提示，需要在企业号后台删除不用的应用，重新托管即可；</p>
										<p class="beizhu-col">备注2：如出现以下提示，可能受到网络以及浏览器兼容性影响，换Chrome等浏览器重新托管即可；</p>
										<img src="${pageContext.request.contextPath }/static/images/st_img10.png" alt="" />
									</div>
									
									<p class="step-content sc-bg7">其他注意事项</p>
									<div class="step-details step-details-nobor pb10">
										<p>
											① 确保微信企业号后台中安装的应用都在 “托管的应用” 里，而不是在 “我的应用” 中；<br> ②
											在微信企业号后台设置应用可见范围后，手机端仍看不见，可以通过重新托管应用解决；<br> ③
											重新托管不会影响用户的数据。
										</p>
									</div>
								</div>
							</div>
							<div class="bottom-box"></div>
						</div>
					</div>
					<script type="text/javascript">
						$(document).ready(function() {
							$('.click-out').click(function(e) {
								e.preventDefault();
								$('.mask-box').show();
								$('.closed-container-box').show();
								$('body').css('overflow','hidden');
							})
							$('.close-btn').click(function() {
								$('.mask-box').hide();
								$('.closed-container-box').hide();
								$('body').css('overflow', 'auto');
							})
							var pingmuh = document.documentElement.clientHeight;
							$('.closed-container-box').css('height', pingmuh * 0.85 + 20);
							$('.closed-container-box').css('top','50%');
							$('.closed-container-box').css('margin-top',-pingmuh * 0.85 / 2);
							$('.add-azheight').css('height',pingmuh * 0.85 - 22);
						});
					</script>
					<div class="apply_r apply_bgBox">
						<div class="apply_item p0">
							<div class="apply_bg">
								<div class="bg_text" style="color: #2d9a93">
									<img src="http://qy.sliansoft.com/static/upload/suitelogo/${suite.suiteLogoname}" height="125px" width="125px">
									<h1 style="color: #2d9a93">${suite.suiteName }</h1>
									<p>应用场景：${suite.suiteScene }</p>
									<!--  suiteId后的-1代表着可以安装授权该套件的所有应用-->
									<a class="btn" target="_blank"
										href="${pageContext.request.contextPath }/installApp/${suite.suiteId }/${-1}">免费安装</a>
									<!-- <a  class="click-out" href="/suiteps/question/st_help.html"  target="_blank">套件安装指引</a>-->
									<a class="click-out" target="_blank">套件安装指引</a>
								</div>
							</div>
							<p class="c333 hidden mt40 mb5">
								<span class="fz18">包含应用</span> 
								<span class="to-enroll">安装套件需要微信企业号（<a
									class="to-enroll-con" href="https://qy.weixin.qq.com"
									rel="nofollow" target="_blank">注册</a>），建议使用 chrome 浏览器
								</span>
							</p>
						</div>
						<div class="apply_item pb0">
							<c:forEach items="${appList}" var="app">
								<a class="applyImg"
									href="${pageContext.request.contextPath }/suiteAndApp/showApp/${suite.suiteId }/${app.appId }"><img
									src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname }"
									alt="${app.appName  }" height="70px" width="70px">
									<c:if test="${fn:length(app.appName)>4 }">${ fn:substring( app.appName ,0,4)} ..</c:if>
									<c:if test="${fn:length(app.appName)<=4 }">${ fn:substring( app.appName ,0,4)}</c:if>
									</a>
							</c:forEach>
							<p class="c333 hidden mt30 mb5">
								<span class="fz18">套件介绍</span>
							</p>
						</div>
						<div class="apply_item hidden borderNone">
							<div>${suite.suiteIntroduction }</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		﻿
		<script type="text/javascript">
			var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
			document.write(unescape("%3Cspan style='display:none' id='cnzz_stat_icon_1254638826'%3E%3C/span%3E%3Cscript src='" 
					+ cnzz_protocol 
					+ "s4.cnzz.com/z_stat.php%3Fid%3D1254638826%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<jsp:include page="footer.jsp" />
	</body>
</html>