<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数联软件</title>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/manager/css/style.css">
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/static/manager/css/suiteandapp.css">
    <script src='${pageContext.request.contextPath }/static/manager/js/jquery-1.10.2.min.js'></script>
   
</head>
<body>
<div class="loading_bg" id="msgshow_bg" style="display:none"></div>
<div class="con_loading" id="con_loading" style="display:none">正在加载中……</div>
	<div class="wrap-header">
		<div id="header">
			<div class="inner-header">
				<div class="logoNav clearfix">
					<a href="http://wbg.do1.com.cn" id="logo" target="_blank">
                        <img id="headLogo" style="margin-top: 20px; width:250px;" alt="logo" src="${pageContext.request.contextPath }/static/images/logoManager.png">
                    </a>
					<div id="login_text">
						<span id="userOrgName" >欢迎你使用数联软件</span>
						<a class="ml10" href="${pageContext.request.contextPath }">前台</a>
					</div>
				</div>
	
				<div id="nav" >
					<ul class="inline-float" >
						<li permission="" 
							 id="content"><a
							href="#">内容管理</a></li>
						<li permission="contactMenu" 
							 id="addressbook"><a
							href="#">通讯录管理</a></li>
						<li id="yyzx_liid" permission="configMenu" 
							class="active" id="addressbook">
							<SPAN id="yyzx_spanid" >
								<a id="yyzx_aid" href="${pageContext.request.contextPath }/manager/list">应用中心</a>
							</SPAN>
							<div class="yyzx_liid_pop" id="yyzx_liid_pop" style="display:none">点击“应用中心”还可以
							安装更多免费应用！
							</div>
						</li>
						<li permission="sysmgrMenu" 
							><a
							href="#">系统管理</a></li>
						<li permission="settingmenu" 
							><a
							href="#">设置中心</a></li>
						<li >
							<SPAN id="bz_spanid" >
								<a href="#" >帮助</a>
							</SPAN>
						</li>
						<li permission="cooperationMenu" 
							><a
							href="#">企业信息</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap-container">
        <div id="container" class="clearfix">
            <div class="bindingAPP">
              <div class="apptop" style="text-align: center;">
                <div class="apptop-tit" style="font-size: 20px; font-weight: 700; margin: 20px 20px 10px">应用中心</div>
                <div class="apptop-lead">只能在平台上添加5个套件<a href="${pageContext.request.contextPath }/manager/addSuite">添加套件</a></div>               
              </div>
            </div>
           
			<div id="suite">
			<c:if test="${empty appList }">
				<div>
					<p>目前没有套件和应用</p>
				</div>
			</c:if>
				<c:if test="${! empty appList }">
			 	<c:forEach items="${appList }" var="suite" varStatus="index">
					<div class="meberbox">
						<span class="meberimg"> <img alt="suitelogo"
							style="width: 40px; height: 40px"
							src="http://qy.sliansoft.com/static/upload/suitelogo/${suite.key.suiteLogoname}">
						</span>
						<div class="mebername">${suite.key.suiteName }</div>
						<div class="meberinfo">
							<span class="icon_deposit"> 共三个应用,点击图标修改应用 </span> <a style href="${pageContext.request.contextPath }/manager/deleteSuite/${suite.key.id}"
								target class="meberbtn">删除套件</a>
						</div>
					</div>
					<div class="appbox">
						<span class="appboxhere">
							<span class="appboxhere2"></span>
						</span>
						<div class="appbox2">
							<ul id="${suite.key.id }">
								<c:forEach items="${suite.value}" var="app">
									<li>
										<div class="appbox3">
											<div class="appbox4">
												<span class="icon_app_60" id="${app.id }" >
												<img alt="" src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname}" style="width: 80px; height: 80px">
												</span> <span class="appname" title="应用">${app.appName }</span>
												<a class="appdelete" href="${pageContext.request.contextPath }/manager/deleteApp/${app.id}">删除</a>
											</div> 
										</div>
									</li>
								</c:forEach>
								<li>
									<div class="appbox3">
										<div class="appbox4">
										<a href="${pageContext.request.contextPath }/manager/addApp/${suite.key.id}">
											<span class="icon_add_60" >
											</span> <span class="appname" title="添加应用">添加应用</span>
										</a>
										</div> 
									</div>
								</li>
							</ul>
						</div>
					</div>
				 </c:forEach>
				 </c:if>
			</div>
		</div>
     </div>
</body>
</html>