<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="">
	<meta name="baidu-site-verification" content="Vup9CGlqbA" />
	<!-- 百度检测
	<meta name="msvalidate.01" content="6C48EF31F67FA6E09A742953ACAB5A39" />
	必应搜索
	<meta name="description"
		content="移动报销应用，通过微信即可快速上报报销明细，领导随时随地审批，提高财务处理效率。优化企业繁琐复杂的报销环节，让公司费用管理有据可查">
	<meta name="keywords"
		content="微信企业号功能,免费微信企业号应用,企业号应用,微信企业级应用,微信企业号第三方应用,微信企业号应用中心"> -->
	<title>${app.appName }-数联云平台</title>
	<link href="${pageContext.request.contextPath }/static/css/do1.css?v1.44" rel="stylesheet"
		media="screen" type="text/css" />
	<script language="javascript" type="text/javascript"
		src="${pageContext.request.contextPath }/static/js/jquery-1.11.1.min.js"></script>
	<script language="javascript" type="text/javascript"
		src="${pageContext.request.contextPath }/static/js/common.js?v1.44"></script>
	<script src=" http://hm.baidu.com/h.js?0d8c8e0c89f9c63e43a87823376c485e"
		type="text/javascript"></script>
</head>
<body class="applyBody">
	<jsp:include page="header.jsp" />
	<div class="applyWrapBg">
		<div class="title">HelloWorld</div>
		<div class="container">
			<div class="more-news hidden">
				<jsp:include page="leftList.jsp" />

				<script type="text/javascript">
					$(document).ready(function() {
						$('.click-out').click(function(e) {
							e.preventDefault();
							$('.mask-box').show();
							$('.closed-container-box').show();
							$('body').css('overflow', 'hidden');
						})
						$('.close-btn').click(function() {
							$('.mask-box').hide();
							$('.closed-container-box').hide();
							$('body').css('overflow', 'auto');
						})
						var pingmuh = document.documentElement.clientHeight;
						$('.closed-container-box').css('height', pingmuh * 0.85 + 20);
						$('.closed-container-box').css('top','50%');
						$('.closed-container-box').css('margin-top', -pingmuh * 0.85 / 2);
						$('.add-azheight').css('height', pingmuh * 0.85 - 22);
					});
				</script>
				<div class="apply_r">
					<div class="apply_item pt0">
						<div class="apply_icon fl">
							<img src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname }" width="100px" height="100px">
						</div>
						<div class="apply_item_wrap">
							<div class="iblock">
								<h3>${app.appName }</h3>
								<span class=''><a href="" target="_blank">场景：${app.appScene }</a></span>
								<span class='parent_apply'><a href="${pageContext.request.contextPath }/suiteAndApp/showSuite/${suite.suiteId }">所属套件：${suite.suiteName }</a></span>
							</div>
							<div class="apply_item_btn">
								<a id="reimbursement" 
									onclick="_hmt.push(['_trackEvent', 'WX_suite', 'sign', 'reimbursement'])"
									href="${pageContext.request.contextPath }/installApp/${suite.suiteId }/${app.appId }"
									class="bgff9b0c" target="_blank" rel="nofollow">安装应用</a> 
							</div>
						</div>
					</div>
					<div class="apply_item">
						<h4>应用介绍</h4>
						<p>${app.appIntroduction }</p>
					</div>
					<div class="apply_item_flex">
						<div class="imgbox" id="imgbox">
							<!-- <img src=""> 
							<img src=""> 
							<img src=""> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	﻿
	<script type="text/javascript">
		var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
		document.write(unescape("%3Cspan style='display:none' id='cnzz_stat_icon_1254638826'%3E%3C/span%3E%3Cscript src='"
				+ cnzz_protocol
				+ "s4.cnzz.com/z_stat.php%3Fid%3D1254638826%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<jsp:include page="footer.jsp" />
</body>
</html>