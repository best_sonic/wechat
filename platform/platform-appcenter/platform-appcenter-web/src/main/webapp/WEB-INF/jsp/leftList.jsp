<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="apply_l">
	<c:forEach var="suite" items="${suiteList}" varStatus="status">
		<dl>
			<c:if test="${status.count==1 }">
				<dt style="margin-top: -4px;">
					<a href="${pageContext.request.contextPath }/suiteAndApp/showSuite/${suite.suiteId }">${suite.suiteName }</a>
				</dt>
			</c:if>
			<c:if test="${status.count!=1 }">
				<dt>
					<a href="${pageContext.request.contextPath }/suiteAndApp/showSuite/${suite.suiteId }">${suite.suiteName }</a>
				</dt>
			</c:if>
			<c:choose>
				<c:when test="${status.count==1 }">
					<c:forEach items="${appList1}" var="app">
						<dd>
							<a href="${pageContext.request.contextPath }/suiteAndApp/showApp/${suite.suiteId }/${app.appId }"><img
								src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname }" height="20px" width="20px">${app.appName }</a>
						</dd>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==2 }">
					<c:forEach items="${appList2}" var="app">
						<dd>
							<a href="${pageContext.request.contextPath }/suiteAndApp/showApp/${suite.suiteId }/${app.appId }"><img
								src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname }" height="20px" width="20px">${app.appName }</a>
						</dd>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==3 }">
					<c:forEach items="${appList3}" var="app">
						<dd>
							<a href="${pageContext.request.contextPath }/suiteAndApp/showApp/${suite.suiteId }/${app.appId }"><img
								src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname }" height="20px" width="20px">${app.appName }</a>
						</dd>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==4 }">
					<c:forEach items="${appList4}" var="app">
						<dd>
							<a href="${pageContext.request.contextPath }/suiteAndApp/showApp/${suite.suiteId }/${app.appId }"><img
								src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname }" height="20px" width="20px">${app.appName }</a>
						</dd>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==5 }">
					<c:forEach items="${appList5}" var="app">
						<dd>
							<a href="${pageContext.request.contextPath }/suiteAndApp/showApp/${suite.suiteId }/${app.appId }"><img
								src="http://qy.sliansoft.com/static/upload/applogo/${app.appLogoname }" height="20px" width="20px">${app.appName }</a>
						</dd>
					</c:forEach>
				</c:when>
			</c:choose>
		</dl>
	</c:forEach>
</div>