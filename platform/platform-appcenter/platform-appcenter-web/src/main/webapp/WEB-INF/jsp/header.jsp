<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id='header'>
	<div class="navbar w960">
		<a title="微信企业号应用，选择数联云平台" href="${pageContext.request.contextPath }/" class="logo" style="left:10px;">
			<img src="${pageContext.request.contextPath }/static/images/logo.png" alt="logo" height="50px" style="margin-top:8px; margin-left:40px"></a> 
 		<a href="https://qy.weixin.qq.com/app" target="_self" style="position: absolute; left: 190px; top: 15px;">
			<img src="https://res.wx.qq.com/mmocbiz/zh_CN/tmt/home/dist/img/cert_logo_white_m_2cb55b0b.png" alt="企业号登录"></a>
		<ul id="nav">
			<li><a href="${pageContext.request.contextPath }/index.jsp">首页</a></li>
			<!-- <li><a href="/scenario/scenario1/">场景</a></li> -->
			<li><a href="${pageContext.request.contextPath }/suiteAndApp/showSuite/tj7a34e960e9abae07">应用与套件</a></li>
			<li><a href="http://qy.sliansoft.com/platform-settings-web/login.html">应用管理</a></li>
		</ul>
	</div>
</div>