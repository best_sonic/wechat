<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:forEach var="suite" items="${suiteList}" varStatus="status">
		<div>
			<h2>${suite.suiteName }</h2>
			<c:choose>
				<c:when test="${status.count==1 }">
					<c:forEach items="${appList1}" var="app">
						<span>${app.appName }</span>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==2 }">
					<c:forEach items="${appList2}" var="app">
						<span>${app.appName }</span>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==3 }">
					<c:forEach items="${appList3}" var="app">
						<span>${app.appName }</span>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==4 }">
					<c:forEach items="${appList4}" var="app">
						<span>${app.appName }</span>
					</c:forEach>
				</c:when>
				<c:when test="${status.count==5 }">
					<c:forEach items="${appList5}" var="app">
						<span>${app.appName }</span>
					</c:forEach>
				</c:when>
			</c:choose>
		</div>
	</c:forEach>
</body>
</html>