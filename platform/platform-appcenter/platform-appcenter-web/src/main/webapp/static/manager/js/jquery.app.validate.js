function appValidate(){
	 $("#app_add_form").validate({
		 focusCleanup:true,//clear the error message when the error element get focus again. 
		 onkeyup:false, 
		
		rules:{
			upload_org_code:{
				required:true,
				accept: "gif|png|jpg"
			},
			id:{
				required:true,
				digits:true
			},
			url:{
				required:true,
				url:true
			},
			appname:{
				required:true
			}
		},
		messages:{
			upload_org_code:{
				required:"请上传logo",
				accept: "格式不正确"
			},
			id:{
				required:"appid不能为空",
				digits:"请填写正整数"
			},
			url:{
				required:"url不能为空",
				url:"url格式不正确"
			},
			appname:{
				required:"应用名称不能为空"
			}
		}
	});
}

function menuoneValidate(){
	
	/*$("#menuone_add_form").validate({
		
		focusCleanup:true,
		onkeyup:false, 
		rules:{
			menuonename:{
				required:true
			},
			menuoneurl:{
				url:true
			}
		},
		messages:{
			menuonename:{
				required:"一级菜单名称不能为空"
			},
			menuoneurl:{
					url:"url格式不正确"
			}
		}
	});*/
}

function menutwoValidate(){
	if("view" == $("#menutwotype").val()){
		$("#menutwourl").removeAttr("disabled");
		$("#menutwokey").attr("disabled","disabled");
	}else{
		$("#menutwokey").removeAttr("disabled");
		$("#menutwourl").attr("disabled","disabled");
	}
	$("#menutwo_add_form").validate({
		
		focusCleanup:true,
		onkeyup:false, 
		rules:{
			menutwoname:{
				required:true
			},
			menutwotype:{
				required:true
			},
			menutwourl:{
				required:true,
				url:true
			},
			menutwokey:{
				required:true
			}
		},
		messages:{
			menutwoname:{
				required:"一级菜单名称不能为空"
			},
			menutwotype:{
				required:"必须填写菜单类型"
			},
			menutwourl:{
				required:"url不能为空",
				url:"url格式不正确"
			},
			menutwokey:{
				required:"key不能为空"
			}
		}
	});
}