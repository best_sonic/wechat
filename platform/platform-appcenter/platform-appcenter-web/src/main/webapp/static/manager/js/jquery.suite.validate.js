function suiteValidate(){
	 $("#suite_add_form").validate({
		 focusCleanup:true,//clear the error message when the error element get focus again. 
		 onkeyup:false, 
		
		rules:{
			upload_org_suite:{
				required:true,
				accept: "gif|png|jpg"
			},
			suiteid:{
				required:true,
			},
			suitename:{
				required:true,
			},
			secret:{
				required:true
			},
			token:{
				required:true
			},
			encodingseskey:{
				required:true
			}
		},
		messages:{
			upload_org_suite:{
				required:"请上传logo",
				accept: "格式不正确"
			},
			suiteid:{
				required:"appid不能为空",
			},
			suitename:{
				required:"套件名称不能为空",
			},
			secret:{
				required:"secret不能为空"
			},
			token:{
				required:"Token不能为空"
			},
			encodingseskey:{
				required:"encodingaeskey不能为空"
			}
		}
	});
}