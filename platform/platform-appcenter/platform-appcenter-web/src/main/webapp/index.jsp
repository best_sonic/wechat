<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- <html>
<body>
<h2>首页</h2>

<a href="${pageContext.request.contextPath }/suiteAndApp/list">列出应用与套件</a>
<br><br><br>
<a href="${pageContext.request.contextPath }/suiteAndApp/showSuite/tj7a34e960e9abae07">套件展示页面</a>
<br><br><br>
<a href="${pageContext.request.contextPath }/suiteAndApp/showApp/tj7a34e960e9abae07/1">应用展示页面</a>

</body>
</html> --%>
<html lang="zh-cn">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="">
	<meta name="description"
		content="">
	<meta name="keywords"
		content="微信企业号,微信办公平台,企业号第三方应用,微信企业号第三方,企业号服务商">
	<!-- <meta name="baidu-site-verification" content="Vup9CGlqbA" /> -->
	<!-- 百度检测-->
	<!-- <meta name="msvalidate.01" content="6C48EF31F67FA6E09A742953ACAB5A39" /> -->
	<!-- 必应搜索-->
	<title>数联云平台</title>
	<link href="${pageContext.request.contextPath }/static/css/do1.css?v1.45" rel="stylesheet"
		media="screen" type="text/css" />
	<script language="javascript" type="text/javascript"
		src="${pageContext.request.contextPath }/static/js/jquery-1.11.1.min.js"></script>
	<script language="javascript" type="text/javascript"
		src="${pageContext.request.contextPath }/static/js/bootstrap.min.js"></script>
	<script language="javascript" type="text/javascript"
		src="${pageContext.request.contextPath }/static/js/common.js?v1.45"></script>
	<!-- <script src=" http://hm.baidu.com/h.js?0d8c8e0c89f9c63e43a87823376c485e"
		type="text/javascript"></script> -->
	</head>
<body>
	<jsp:include page="/WEB-INF/jsp/header.jsp" />
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<!-- <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li> -->
			<!-- <li data-target="#carousel-example-generic" data-slide-to="1"></li> -->
			<!-- <li data-target="#carousel-example-generic" data-slide-to="2"></li> -->
			<!--  <li data-target="#carousel-example-generic" data-slide-to="3"></li> -->
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active item-bg1">
				<a class="banner-btn banner-btn-1" href="${pageContext.request.contextPath }/suiteAndApp/showSuite/tj7a34e960e9abae07"
					target="_self">马上查看套件</a>
				<!-- <a class="banner-btn banner-btn-2" href="http://wbg.do1.com.cn/help/newhand/2015/1012/469.html?1444643542" target="_blank">&nbsp;&nbsp;新手入门&nbsp;></a> -->
				<!-- <a class="banner-btn banner-btn-2"
					href="http://wbg.do1.com.cn/QWinformation/activity/2015/1109/529.html"
					target="_blank">&nbsp;&nbsp;入门教学&nbsp;></a> -->
			</div>
			<%-- <div class="item item-bg2">
				<a href="${pageContext.request.contextPath }/suiteAndApp/list" target="_blank"
					class="banner-btn-3"></a>
			</div> --%>
			<%-- <div class="item item-bg4">
				<a class="banner-btn banner-btn-1 banner-btn-5" href="${pageContext.request.contextPath }/suiteAndApp/list"
					target="_blank">显示所有应用</a>
				<!-- <a href="./workspace" target="_blank" class="banner-btn-3"></a> -->
			</div> --%>
		</div>
		
	</div>

	<div class="content">
		<div class="tit">
			<h3>无需安装APP，在微信上就能移动办公的平台</h3>
			<div class="lead">
				重新定义企业内部工作流程和运营管理方式，创造性解决企业内部消息通知不及时、消息通知（短彩信）成本高、<br>
				工作审批严重滞后、客户管理不规范、运营管理方式落后等传统难题。
			</div>
		</div>
		<div class="hidden center">
			<img data-src="${pageContext.request.contextPath }/static/images/c11.png" alt="无需安装APP"
				style="width: 919px; height: 147px">
		</div>
<!-- 		<div class="tit">
			<h3>“为场景而生”——专为移动办公量身定制</h3>
			<div class="lead">
				四大移动办公场景，完美解决80%的日常工作和管理问题；<br>
				包括信息沟通、员工建设、沟通协作、客户管理（CRM）、运营管理（采购管理、销售汇报、店面巡检、问卷调查……）等。
			</div>
		</div>
		<div class="hidden">
			<a href="./scenario/scenario1" class="index_pic">
				<div class="">
					<img data-src="/templets/default/images/c4.jpg" alt="信息沟通场景"
						style="width: 238px; height: 250px">
				</div>
				<div class="index_bgcolor1">
					<img data-src="/templets/default/images/c4.jpg" alt="信息沟通场景"
						style="width: 238px; height: 250px"> <span>信息沟通场景</span>
				</div>
			</a> <a href="./scenario/scenario2" class="index_pic sp">
				<div class="">
					<img data-src="/templets/default/images/c1.jpg" alt="工作协同场景"
						style="width: 238px; height: 250px">
				</div>
				<div class="index_bgcolor2">
					<img data-src="/templets/default/images/c1.jpg" alt="工作协同场景"
						style="width: 238px; height: 250px"> <span>工作协同场景</span>
				</div>
			</a> <a href="./scenario/scenario3" class="index_pic">
				<div class="">
					<img data-src="/templets/default/images/c5.jpg" alt="销售管理场景"
						style="width: 238px; height: 250px">
				</div>
				<div class="index_bgcolor3">
					<img data-src="/templets/default/images/c5.jpg" alt="销售管理场景"
						style="width: 238px; height: 250px"> <span>销售管理场景</span>
				</div>
			</a> <a href="./scenario/scenario4" class="index_pic sp">
				<div class="">
					<img data-src="/templets/default/images/c7.jpg" alt="运营管理场景"
						style="width: 238px; height: 250px">
				</div>
				<div class="index_bgcolor4">
					<img data-src="/templets/default/images/c7.jpg" alt="运营管理场景"
						style="width: 238px; height: 250px"> <span>运营管理场景</span>
				</div>
			</a>
		</div> -->


		<div class="tit">
			<h3 class="mt50">4个基础应用，实用、易用</h3>
			<div class="lead">
				围绕四大场景，打造专业、实用、易用的基础应用，<br>
				让企业轻松管理员工、设备巡检、客户管理以及日程管理等，帮助提升企业工作效率和管理效率。
			</div>
		</div>
		<ul class="index_icon hidden">
			<a class="center" href="${pageContext.request.contextPath }/suiteAndApp/showApp/tj7a34e960e9abae07/3"><p>
					<img data-src="./static/images/icon_6.png" alt="日程管理"
						style="width: 80px; height: 80px">
				</p> <span>日程管理</span>
				<p>安排时间，管理日程</p></a>
			<a class="center" href="${pageContext.request.contextPath }/suiteAndApp/showApp/tj7a34e960e9abae07/4"><p>
					<img data-src="./static/images/icon_3.png"
						alt="设备巡检" style="width: 80px; height: 80px">
				</p> <span>设备巡检</span>
				<p>便捷快速，巡检设备</p></a>
			<a class="center" href="${pageContext.request.contextPath }/suiteAndApp/showApp/tj7a34e960e9abae07/2"><p>
					<img data-src="./static/images/icon_20.png"
						alt="微信考勤" style="width: 80px; height: 80px">
				</p> <span>考勤打卡</span>
				<p>轻松签到，快乐签退</p></a>
			<a class="center" href="${pageContext.request.contextPath }/suiteAndApp/showApp/tj37d849d1f6c65194/5"><p>
					<img data-src="./static/images/icon_12.png"
						alt="移动CRM" style="width: 80px; height: 80px">
				</p> <span>移动CRM</span>
				<p>客户管理，商机管理</p></a>

			<a href="" class="index_icon_more">更多...</a>

		</ul>
<!-- 		<div class="tit" style="margin-top: 70px;">
			<h3 class="mt50">4大产品应用“一键授权安装”</h3>
			<div class="lead">已拥有微信企业号的企业用户，可点击以下套件安装，快速接入数联云平台</div>
		</div>

		<div class="hidden">
			<a href="" class="index_suite_icon sp"><img
				data-src="/templets/default/images/suite1.png" alt="客户关系（CRM）"
				style="width: 232px"></a> <a href=""
				class="index_suite_icon"><img
				data-src="/templets/default/images/suite2.png" alt="日程管理"
				style="width: 232px"></a> <a href=""
				class="index_suite_icon"><img
				data-src="/templets/default/images/suite3.png" alt="设备巡检"
				style="width: 232px"></a> <a href=""
				class="index_suite_icon"><img
				data-src="/templets/default/images/suite4.png" alt="考勤打卡"
				style="width: 232px"></a>
			  <a href="/suite/resource_suite" class="index_suite_icon"><img data-src="/templets/default/images/suite5.png" alt="资源管理" style="width:184px;height:156px;"></a>
		</div> -->
		<!-- <div class="tit">
			<h3 class="mt50">没有微信企业号？也可免费体验</h3>
			<a class="lead" href="https://qy.weixin.qq.com/" target="_blank" rel="nofollow">什么是微信企业号？</a>
		</div>
		<div class="hidden">
			<a href="./Experience" class="index_Btn">免费体验通道</a>
		</div> -->
	</div>
	
	<%-- <a href=" https://qy.weixin.qq.com/cgi-bin/loginpage?corp_id=wxa75fcc28f7f6401a&redirect_uri=${pageContext.request.contextPath }/suiteAndApp/showSuite/tj7a34e960e9abae07&state=xxxx">
		<img src="https://res.wx.qq.com/mmocbiz/zh_CN/tmt/home/dist/img/logo_blue_b_c97f8734.png" alt="企业号登录"/>
	</a> --%>
	﻿
	<script type="text/javascript">
		var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
		document.write(unescape("%3Cspan style='display:none' id='cnzz_stat_icon_1254638826'%3E%3C/span%3E%3Cscript src='"
				+ cnzz_protocol
				+ "s4.cnzz.com/z_stat.php%3Fid%3D1254638826%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));
	</script>
	
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>
