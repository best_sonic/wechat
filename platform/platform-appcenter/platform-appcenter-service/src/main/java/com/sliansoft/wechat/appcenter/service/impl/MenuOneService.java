package com.sliansoft.wechat.appcenter.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.appcenter.mapper.MenuoneMapper;
import com.sliansoft.wechat.appcenter.model.Menuone;
import com.sliansoft.wechat.appcenter.model.MenuoneExample;
import com.sliansoft.wechat.appcenter.service.IMenuOneService;

@Service
public class MenuOneService implements IMenuOneService {
	
	@Inject
	private MenuoneMapper menuoneMapper;

	@Override
	public List<Menuone> selectByAppId(int appId) {
		MenuoneExample menuoneExample = new MenuoneExample();
		menuoneExample.createCriteria().andMenuoneAppIdEqualTo(appId);
		return menuoneMapper.selectByExample(menuoneExample);
	}

	@Override
	public void addMenuOne(Menuone menuone) {
		menuoneMapper.insertSelective(menuone);
	}

	@Override
	public void deleteMenuOne(int id) {
		menuoneMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateMenuone(Menuone menuone) {
		menuoneMapper.updateByPrimaryKeySelective(menuone);
		
	}

	@Override
	public Menuone getMenuoenById(int id) {
		return menuoneMapper.selectByPrimaryKey(id);
	}

	@Override
	public void deleteMenuonesByAppId(int appId) {
		List<Menuone> list = selectByAppId(appId);
		for(Menuone menuone :list){
			deleteMenuOne(menuone.getId());
		}
	}
}
