package com.sliansoft.wechat.appcenter.service;

import java.util.List;

import com.sliansoft.wechat.appcenter.model.CorpToken;

public interface ICorpTokenService {
	
	public void saveCorpToken(CorpToken corpToken);
	
	public void updateCorpToken(CorpToken corpToken);
	
	public CorpToken selectByCorpIdAndSuiteId(String corpId, String suiteId);
	
	public void saveOrUpdateCorpToken(CorpToken corpToken);
	
	public List<CorpToken> seletAll();
	
	public void deleteCorpTokenByCorpIdSuiteId(String corpId, String suiteId);
}
