package com.sliansoft.wechat.appcenter.service;

import com.sliansoft.wechat.appcenter.model.AgentInfo;

public interface IAgentInfoService {
	
	public void saveAgentInfo(AgentInfo agentInfo);
	
	public void updateAgentInfo(AgentInfo agentInfo);
	
	public AgentInfo selectByCorpIdAndSuiteId(String corpId, String suiteId);
	
	public void deleteAgentInfoByCorpIdSuiteId(String corpId, String suiteId);
	
}
