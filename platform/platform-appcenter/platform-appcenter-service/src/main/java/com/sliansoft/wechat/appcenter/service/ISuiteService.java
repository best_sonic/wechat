package com.sliansoft.wechat.appcenter.service;

import java.util.List;

import com.sliansoft.wechat.appcenter.model.Suite;
import com.sliansoft.wechat.appcenter.model.SuiteWithBLOBs;

public interface ISuiteService {
	
	public void add(SuiteWithBLOBs suite);
	
	public void delete(int id);
	
	public void update(SuiteWithBLOBs suite);
	
	public SuiteWithBLOBs selectById(int id);
	
	public SuiteWithBLOBs selectBySuiteId(String suiteId);
	
	public SuiteWithBLOBs seletBySuiteName(String suiteName);
	
	public List<SuiteWithBLOBs> seletAll();
	
}
