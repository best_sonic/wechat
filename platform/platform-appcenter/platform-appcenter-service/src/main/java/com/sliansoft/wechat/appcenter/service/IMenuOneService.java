package com.sliansoft.wechat.appcenter.service;

import java.util.List;

import com.sliansoft.wechat.appcenter.model.Menuone;

public interface IMenuOneService {
	
	public void addMenuOne(Menuone menuone);
	
	public void deleteMenuOne(int id);
	
	public void updateMenuone(Menuone menuone);
	
	public Menuone getMenuoenById(int id);
	
	public List<Menuone> selectByAppId(int appId);
	
	public void deleteMenuonesByAppId(int appId);
	
}
