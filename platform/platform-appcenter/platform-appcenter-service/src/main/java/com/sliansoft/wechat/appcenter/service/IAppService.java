package com.sliansoft.wechat.appcenter.service;

import java.util.List;

import com.sliansoft.wechat.appcenter.model.AppWithBLOBs;
import com.sliansoft.wechat.appcenter.model.Menuone;
import com.sliansoft.wechat.appcenter.model.Menutwo;

public interface IAppService {
	
	public void add(AppWithBLOBs app);
	
	public void delete(int id);
	
	public void update(AppWithBLOBs app);
	
	public AppWithBLOBs selectById(int id);
	
	public AppWithBLOBs selectByAppName(String appName);
	
	public AppWithBLOBs selectBySuiteIdAndAppId(String suiteId, int appId);
	
	public List<AppWithBLOBs> selectBySuiteId(String suiteId);
	
	public void addAppAndMenu(AppWithBLOBs app, Menuone menuone, Menutwo menutwo);
}
