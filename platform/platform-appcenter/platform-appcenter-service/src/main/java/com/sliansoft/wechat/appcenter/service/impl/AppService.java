package com.sliansoft.wechat.appcenter.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.appcenter.mapper.AppMapper;
import com.sliansoft.wechat.appcenter.mapper.MenuoneMapper;
import com.sliansoft.wechat.appcenter.mapper.MenutwoMapper;
import com.sliansoft.wechat.appcenter.model.AppExample;
import com.sliansoft.wechat.appcenter.model.AppWithBLOBs;
import com.sliansoft.wechat.appcenter.model.Menuone;
import com.sliansoft.wechat.appcenter.model.Menutwo;
import com.sliansoft.wechat.appcenter.service.IAppService;

@Service
public class AppService implements IAppService {
	
	@Inject
	private AppMapper appMapper;
	
	@Inject
	private MenuoneMapper menuoneMapper;
	
	@Inject
	private MenutwoMapper menutwoMapper;

	@Override
	public void add(AppWithBLOBs app) {
		appMapper.insertSelective(app);
	}
	
	
	@Override
	public void delete(int id) {
		appMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void update(AppWithBLOBs app) {
		appMapper.updateByPrimaryKeyWithBLOBs(app);
	}

	@Override
	public AppWithBLOBs selectById(int id) {
		return appMapper.selectByPrimaryKey(id);
	}

	@Override
	public AppWithBLOBs selectByAppName(String appName) {
		AppExample appExample = new AppExample();
		appExample.createCriteria().andAppNameEqualTo(appName);
		return appMapper.selectByExampleWithBLOBs(appExample).get(0);
	}

	@Override
	public List<AppWithBLOBs> selectBySuiteId(String suiteId) {
		AppExample appExample = new AppExample();
		appExample.createCriteria().andAppSuiteIdEqualTo(suiteId);
		return appMapper.selectByExampleWithBLOBs(appExample);
	}

	@Override
	public AppWithBLOBs selectBySuiteIdAndAppId(String suiteId, int appId) {
		AppExample appExample = new AppExample();
		appExample.createCriteria().andAppSuiteIdEqualTo(suiteId).andAppIdEqualTo(appId);
		if(appMapper.selectByExampleWithBLOBs(appExample).size()>0){
			return appMapper.selectByExampleWithBLOBs(appExample).get(0);
		}
		return null;
	}


	@Override
	public void addAppAndMenu(AppWithBLOBs app, Menuone menuone, Menutwo menutwo) {
		appMapper.insertSelective(app);
		menuoneMapper.insertSelective(menuone);
		menutwoMapper.insertSelective(menutwo);
	}

}
