package com.sliansoft.wechat.appcenter.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.appcenter.mapper.SuiteMapper;
import com.sliansoft.wechat.appcenter.model.SuiteExample;
import com.sliansoft.wechat.appcenter.model.SuiteWithBLOBs;
import com.sliansoft.wechat.appcenter.service.ISuiteService;

@Service
public class SuiteService implements ISuiteService {
	
	@Inject
	private SuiteMapper suiteMapper;

	@Override
	public void add(SuiteWithBLOBs suite) {
		suiteMapper.insert(suite);
	}

	@Override
	public void delete(int id) {
		suiteMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void update(SuiteWithBLOBs suite) {
		suiteMapper.updateByPrimaryKeySelective(suite);
	}

	@Override
	public SuiteWithBLOBs selectById(int id) {
		return suiteMapper.selectByPrimaryKey(id);
	}

	@Override
	public SuiteWithBLOBs selectBySuiteId(String suiteId) {
		SuiteExample suiteExample = new SuiteExample();
		suiteExample.createCriteria().andSuiteIdEqualTo(suiteId);
		return suiteMapper.selectByExampleWithBLOBs(suiteExample).get(0);
	}

	@Override
	public SuiteWithBLOBs seletBySuiteName(String suiteName) {
		SuiteExample suiteExample = new SuiteExample();
		suiteExample.createCriteria().andSuiteNameEqualTo(suiteName);
		return suiteMapper.selectByExampleWithBLOBs(suiteExample).get(0);
	}

	@Override
	public List<SuiteWithBLOBs> seletAll() {
		SuiteExample suiteExample = new SuiteExample();
		suiteExample.createCriteria();
		return suiteMapper.selectByExampleWithBLOBs(suiteExample);
	}

}
