package com.sliansoft.wechat.appcenter.service;

import java.util.List;

import com.sliansoft.wechat.appcenter.model.Menutwo;

public interface IMenuTwoService {
	
	public List<Menutwo> selectByMenuoneId(int menuoneId);
	
	public void addMenutwo(Menutwo menutwo);
	
	public void deleteMenutwo(int id);
	
	public void updateMenutwo(Menutwo menutwo);
	
	public Menutwo getMenutwoById(int id);
	
	public void deleteMenutwosByMenuone(int menuoneId);
	
}
