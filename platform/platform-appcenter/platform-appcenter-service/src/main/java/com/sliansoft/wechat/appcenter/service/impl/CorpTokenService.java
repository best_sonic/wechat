package com.sliansoft.wechat.appcenter.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.appcenter.mapper.CorpTokenMapper;
import com.sliansoft.wechat.appcenter.model.CorpToken;
import com.sliansoft.wechat.appcenter.model.CorpTokenExample;
import com.sliansoft.wechat.appcenter.model.SuiteExample;
import com.sliansoft.wechat.appcenter.service.ICorpTokenService;
@Service
public class CorpTokenService implements ICorpTokenService{
	@Inject
	private CorpTokenMapper corpTokenMapper;
	/**
	 * 存储
	 */
	@Override
	public void saveCorpToken(CorpToken corpToken) {
		corpTokenMapper.insert(corpToken);
	}
	/**
	 * 更新
	 */
	@Override
	public void updateCorpToken(CorpToken corpToken) {
		CorpTokenExample example=new CorpTokenExample();
		example.createCriteria().andCorpIdEqualTo(corpToken.getCorpId()).andSuiteIdEqualTo(corpToken.getSuiteId());
		corpTokenMapper.updateByExampleSelective(corpToken, example);
	}
	/**
	 * 根据CorpId和SuiteId来选取CorpToken
	 */
	@Override
	public CorpToken selectByCorpIdAndSuiteId(String corpId, String suiteId) {
		CorpTokenExample example=new CorpTokenExample();
		example.createCriteria().andCorpIdEqualTo(corpId).andSuiteIdEqualTo(suiteId);
		List<CorpToken> ctList=corpTokenMapper.selectByExample(example);
		if(ctList.isEmpty())
			return null;
		return ctList.get(0);
	}
	/**
	 * 根据CorpId和SuiteId先查询，然后查看数据库中是否有相应的数据，如果有则更新，否则存储
	 */
	@Override
	public void saveOrUpdateCorpToken(CorpToken corpToken) {
		CorpTokenExample example=new CorpTokenExample();
		example.createCriteria().andCorpIdEqualTo(corpToken.getCorpId()).andSuiteIdEqualTo(corpToken.getSuiteId());
		List<CorpToken> ctList=corpTokenMapper.selectByExample(example);
		if(ctList.isEmpty()){
			saveCorpToken(corpToken);
		}else{
			updateCorpToken(corpToken);
		}
	}
	/**
	 * 取得所有的CorpToken数据
	 */
	@Override
	public List<CorpToken> seletAll() {
		CorpTokenExample corpTokenExample = new CorpTokenExample();
		corpTokenExample.createCriteria().andAccessTokenIsNotNull();
		return corpTokenMapper.selectByExample(corpTokenExample);
	}
	/**
	 * 根据CorpId和SuiteId来删除CorpToken
	 */
	@Override
	public void deleteCorpTokenByCorpIdSuiteId(String corpId, String suiteId) {
		// TODO Auto-generated method stub
		CorpTokenExample example=new CorpTokenExample();
		example.createCriteria().andCorpIdEqualTo(corpId).andSuiteIdEqualTo(suiteId);
		corpTokenMapper.deleteByExample(example);
	}
	
}
