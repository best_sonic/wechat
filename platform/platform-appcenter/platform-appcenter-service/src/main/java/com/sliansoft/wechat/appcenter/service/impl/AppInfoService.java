package com.sliansoft.wechat.appcenter.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.appcenter.mapper.AgentInfoMapper;
import com.sliansoft.wechat.appcenter.model.AgentInfo;
import com.sliansoft.wechat.appcenter.model.AgentInfoExample;
import com.sliansoft.wechat.appcenter.service.IAgentInfoService;

@Service
public class AppInfoService implements IAgentInfoService{
	
	@Inject
	private AgentInfoMapper agentInfoMapper;
	/**
	 * 存储AgentInfo
	 */
	@Override
	public void saveAgentInfo(AgentInfo agentInfo) {
		agentInfoMapper.insert(agentInfo);
	}
	/**
	 * 更新
	 */
	@Override
	public void updateAgentInfo(AgentInfo agentInfo) {
		AgentInfoExample example=new AgentInfoExample();
		example.createCriteria().andCorpIdEqualTo(agentInfo.getCorpId()).andSuiteIdEqualTo(agentInfo.getSuiteId());
		agentInfoMapper.updateByExampleSelective(agentInfo, example);
	}
	/**
	 * 通过CorpId和SuiteId来取得AgentInfo
	 */
	@Override
	public AgentInfo selectByCorpIdAndSuiteId(String corpId,String suiteId) {
		AgentInfoExample example=new AgentInfoExample();
		example.createCriteria().andCorpIdEqualTo(corpId).andSuiteIdEqualTo(suiteId);
		List<AgentInfo> aInfo=agentInfoMapper.selectByExample(example);
		if(aInfo.isEmpty())
			return null;
		return aInfo.get(0);
	}
	/**
	 * 通过CorpId和SuiteId来删除AgentInfo
	 */
	@Override
	public void deleteAgentInfoByCorpIdSuiteId(String corpId, String suiteId) {
		// TODO Auto-generated method stub
		AgentInfoExample example=new AgentInfoExample();
		example.createCriteria().andCorpIdEqualTo(corpId).andSuiteIdEqualTo(suiteId);
		agentInfoMapper.deleteByExample(example);
	}
}
