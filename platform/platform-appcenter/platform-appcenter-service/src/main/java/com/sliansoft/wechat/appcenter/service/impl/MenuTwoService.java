package com.sliansoft.wechat.appcenter.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.appcenter.mapper.MenutwoMapper;
import com.sliansoft.wechat.appcenter.model.Menutwo;
import com.sliansoft.wechat.appcenter.model.MenutwoExample;
import com.sliansoft.wechat.appcenter.service.IMenuTwoService;

@Service
public class MenuTwoService implements IMenuTwoService {

	@Inject
	private MenutwoMapper menutwoMapper;
	
	@Override
	public List<Menutwo> selectByMenuoneId(int menuoneId) {
		MenutwoExample menutwoExample = new MenutwoExample();
		menutwoExample.createCriteria().andMenutwoMenuoneIdEqualTo(menuoneId);
		return menutwoMapper.selectByExample(menutwoExample);
	}

	@Override
	public void addMenutwo(Menutwo menutwo) {
		menutwoMapper.insertSelective(menutwo);
	}

	@Override
	public void deleteMenutwo(int id) {
		menutwoMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void updateMenutwo(Menutwo menutwo) {
		menutwoMapper.updateByPrimaryKeySelective(menutwo);
	}

	@Override
	public Menutwo getMenutwoById(int id) {
		return menutwoMapper.selectByPrimaryKey(id);
	}

	@Override
	public void deleteMenutwosByMenuone(int menuoneId) {
		List<Menutwo> list = selectByMenuoneId(menuoneId);
		for(Menutwo menutwo : list){
			deleteMenutwo(menutwo.getId());
		}
	}

}
