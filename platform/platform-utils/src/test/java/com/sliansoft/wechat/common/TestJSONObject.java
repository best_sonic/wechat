package com.sliansoft.wechat.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sliansoft.wechat.contacts.EmployeeUtil;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.Employee;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestJSONObject{

	@Before
	public void setUp(){
	}
	@After
	public void tearDown(){
	}
	@Test
	public void test(){
		String str = "{" +
   "\"errcode\": 0," + 
   "\"errmsg\": \"ok\"," +
   "\"department\": [" +
       "{" + 
           "\"id\": 2," +
           "\"name\": \"广州研发中心\"," +
           "\"parentid\": 1," +
           "\"order\": 10" +
       "}," +
       "{" +
           "\"id\": 3," +
           "\"name\": \"邮箱产品部\"," +
           "\"parentid\": 2," +
           "\"order\": 40" +
       "}" +
   "]" +
"}";
		JSONObject jsonObject = JSONObject.fromObject(str);
		System.out.println(jsonObject.get("errcode"));
		System.out.println(jsonObject.get("errmsg"));
		System.out.println(jsonObject.get("department"));
		JSONArray departments = jsonObject.getJSONArray("department");
		List<Department> list = new ArrayList<Department>();
		for(int i = 0; i < departments.size(); i++){
			JSONObject obj = departments.getJSONObject(i);
			Department department = new Department();
			department.setDepartmentid(obj.getInt("id"));
			department.setDepOrder(obj.getInt("order"));
			department.setName(obj.getString("name"));
			department.setParentId(obj.getString("parentid"));
			list.add(department);
		}
		System.out.println(list);
		System.out.println(EmployeeUtil.getDepartmentIds(list));
	}
}
