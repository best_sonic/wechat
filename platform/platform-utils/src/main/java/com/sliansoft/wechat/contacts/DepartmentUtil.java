package com.sliansoft.wechat.contacts;

import java.util.ArrayList;
import java.util.List;

import com.sliansoft.wechat.common.HTTPUtil;
import com.sliansoft.wechat.platform.contact.model.Department;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class DepartmentUtil {

	public static final String CREATE_DEPARTMENT = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=ACCESS_TOKEN";
	
	public static final String UPDATE_DEPARTMENT = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=ACCESS_TOKEN";
	
	public static final String DELETE_DEPARTMENT = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token=ACCESS_TOKEN&id=ID";
	
	public static final String GET_DEPARTMENT_LIST = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN&id=ID";

	public static Integer createDepartment(String accessToken, Department department){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", department.getName());
		jsonObject.put("parentid", department.getParentId());
		jsonObject.put("order", department.getDepOrder());
		String result = HTTPUtil.postReq(CREATE_DEPARTMENT.replace("ACCESS_TOKEN", accessToken), jsonObject.toString(), "application/json;charset=utf-8");
		JSONObject responseJSON = JSONObject.fromObject(result);
		System.out.println("------------"+responseJSON.toString());
		if(0 == (Integer)responseJSON.get("errcode") && "created".equals(responseJSON.get("errmsg"))){
			return Integer.valueOf((String) responseJSON.get("id"));
		}
		return -1;
	}
	
	public static boolean updateDepartment(String accessToken, Department department){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", department.getName());
		jsonObject.put("parentid", department.getParentId());
		jsonObject.put("order", department.getDepOrder());
		jsonObject.put("id", department.getDepartmentid());
		String result = HTTPUtil.postReq(UPDATE_DEPARTMENT.replace("ACCESS_TOKEN", accessToken), jsonObject.toString(), "application/json;charset=utf-8");
		JSONObject responseJSON = JSONObject.fromObject(result);
		if(0 == (Integer)responseJSON.get("errcode") && "updated".equals(responseJSON.get("errmsg"))){
			return true;
		}
		return false;
	}
	
	public static boolean deleteDepartment(String accessToken, Integer departmentId){
		String result = HTTPUtil.sendGet(DELETE_DEPARTMENT.replace("ACCESS_TOKEN", accessToken).replace("ID", departmentId.toString()));
		JSONObject responseJSON = JSONObject.fromObject(result);
		if(0 == (Integer)responseJSON.get("errcode") && "deleted".equals(responseJSON.get("errmsg"))){
			return true;
		}
		return false;
	}
	
	public static List<Department> getDepartmentList(String accessToken, Integer departmentId){
		String result = HTTPUtil.sendGet(GET_DEPARTMENT_LIST.replace("ACCESS_TOKEN", accessToken).replace("ID", departmentId.toString()));
		JSONObject responseJSON = JSONObject.fromObject(result);
		System.out.println(responseJSON.toString());
		if(0 == (Integer)responseJSON.get("errcode") && "ok".equals(responseJSON.get("errmsg"))){
			JSONArray departments = responseJSON.getJSONArray("department");
			List<Department> list = new ArrayList<Department>();
			for(int i = 0; i < departments.size(); i++){
				JSONObject obj = departments.getJSONObject(i);
				Department department = new Department();
				department.setDepartmentid(obj.getInt("id"));
				department.setDepOrder(obj.getInt("order"));
				department.setName(obj.getString("name"));
				department.setParentId(obj.getString("parentid"));
				list.add(department);
			}
			return list;
		}
		return null;
	}
}
