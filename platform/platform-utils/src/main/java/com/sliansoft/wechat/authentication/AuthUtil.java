package com.sliansoft.wechat.authentication;

public class AuthUtil {
	/**
	 * 获取企业号管理员登录信息
	 */
	public final static String GET_MANAGER_INFO = "https://qyapi.weixin.qq.com/cgi-bin/service/get_login_info?provider_access_token=PROVIDER_ACCESS_TOKEN";
	
	/**
	 * 获取应用提供商凭证
	 */
	public final static String GET_PROV_TOKEN = "https://qyapi.weixin.qq.com/cgi-bin/service/get_provider_token";
	
}
