package com.sliansoft.wechat.platform.contact.mapper;

import com.sliansoft.wechat.platform.contact.model.CorpDepEmp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmpExample;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.Employee;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CorpDepEmpMapper {
    int countByExample(CorpDepEmpExample example);

    int deleteByExample(CorpDepEmpExample example);

    int deleteByPrimaryKey(String id);

    int insert(CorpDepEmp record);

    int insertSelective(CorpDepEmp record);

    List<CorpDepEmp> selectByExample(CorpDepEmpExample example);

    List<String> selectByCorpIdReturnDep(String corpId);
    
    List<String> selectByCorpIdReturnEmp(String corpId);
    
    List<String> selectByCorpAndDepReturnEmp(String corpId,String depId);
    
    CorpDepEmp selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CorpDepEmp record, @Param("example") CorpDepEmpExample example);

    int updateByExample(@Param("record") CorpDepEmp record, @Param("example") CorpDepEmpExample example);

    int updateByPrimaryKeySelective(CorpDepEmp record);

    int updateByPrimaryKey(CorpDepEmp record);
}