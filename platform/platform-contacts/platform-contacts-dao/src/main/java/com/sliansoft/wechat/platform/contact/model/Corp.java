package com.sliansoft.wechat.platform.contact.model;

import java.io.Serializable;

public class Corp implements Serializable {
    private String id;

    private String corpid;

    private String corpName;

    private String corpRoundLogoUrl;

    private String corpSquareLogoUrl;

    private String corpWxqrcode;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCorpid() {
        return corpid;
    }

    public void setCorpid(String corpid) {
        this.corpid = corpid == null ? null : corpid.trim();
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName == null ? null : corpName.trim();
    }

    public String getCorpRoundLogoUrl() {
        return corpRoundLogoUrl;
    }

    public void setCorpRoundLogoUrl(String corpRoundLogoUrl) {
        this.corpRoundLogoUrl = corpRoundLogoUrl == null ? null : corpRoundLogoUrl.trim();
    }

    public String getCorpSquareLogoUrl() {
        return corpSquareLogoUrl;
    }

    public void setCorpSquareLogoUrl(String corpSquareLogoUrl) {
        this.corpSquareLogoUrl = corpSquareLogoUrl == null ? null : corpSquareLogoUrl.trim();
    }

    public String getCorpWxqrcode() {
        return corpWxqrcode;
    }

    public void setCorpWxqrcode(String corpWxqrcode) {
        this.corpWxqrcode = corpWxqrcode == null ? null : corpWxqrcode.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Corp other = (Corp) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCorpid() == null ? other.getCorpid() == null : this.getCorpid().equals(other.getCorpid()))
            && (this.getCorpName() == null ? other.getCorpName() == null : this.getCorpName().equals(other.getCorpName()))
            && (this.getCorpRoundLogoUrl() == null ? other.getCorpRoundLogoUrl() == null : this.getCorpRoundLogoUrl().equals(other.getCorpRoundLogoUrl()))
            && (this.getCorpSquareLogoUrl() == null ? other.getCorpSquareLogoUrl() == null : this.getCorpSquareLogoUrl().equals(other.getCorpSquareLogoUrl()))
            && (this.getCorpWxqrcode() == null ? other.getCorpWxqrcode() == null : this.getCorpWxqrcode().equals(other.getCorpWxqrcode()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCorpid() == null) ? 0 : getCorpid().hashCode());
        result = prime * result + ((getCorpName() == null) ? 0 : getCorpName().hashCode());
        result = prime * result + ((getCorpRoundLogoUrl() == null) ? 0 : getCorpRoundLogoUrl().hashCode());
        result = prime * result + ((getCorpSquareLogoUrl() == null) ? 0 : getCorpSquareLogoUrl().hashCode());
        result = prime * result + ((getCorpWxqrcode() == null) ? 0 : getCorpWxqrcode().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", corpid=").append(corpid);
        sb.append(", corpName=").append(corpName);
        sb.append(", corpRoundLogoUrl=").append(corpRoundLogoUrl);
        sb.append(", corpSquareLogoUrl=").append(corpSquareLogoUrl);
        sb.append(", corpWxqrcode=").append(corpWxqrcode);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}