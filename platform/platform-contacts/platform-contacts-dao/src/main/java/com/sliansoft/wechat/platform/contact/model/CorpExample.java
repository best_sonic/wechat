package com.sliansoft.wechat.platform.contact.model;

import com.sliansoft.wechat.platform.contact.utils.Page;
import java.util.ArrayList;
import java.util.List;

public class CorpExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Page page;

    public CorpExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCorpidIsNull() {
            addCriterion("CORPID is null");
            return (Criteria) this;
        }

        public Criteria andCorpidIsNotNull() {
            addCriterion("CORPID is not null");
            return (Criteria) this;
        }

        public Criteria andCorpidEqualTo(String value) {
            addCriterion("CORPID =", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidNotEqualTo(String value) {
            addCriterion("CORPID <>", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidGreaterThan(String value) {
            addCriterion("CORPID >", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidGreaterThanOrEqualTo(String value) {
            addCriterion("CORPID >=", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidLessThan(String value) {
            addCriterion("CORPID <", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidLessThanOrEqualTo(String value) {
            addCriterion("CORPID <=", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidLike(String value) {
            addCriterion("CORPID like", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidNotLike(String value) {
            addCriterion("CORPID not like", value, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidIn(List<String> values) {
            addCriterion("CORPID in", values, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidNotIn(List<String> values) {
            addCriterion("CORPID not in", values, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidBetween(String value1, String value2) {
            addCriterion("CORPID between", value1, value2, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpidNotBetween(String value1, String value2) {
            addCriterion("CORPID not between", value1, value2, "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpNameIsNull() {
            addCriterion("CORP_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCorpNameIsNotNull() {
            addCriterion("CORP_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCorpNameEqualTo(String value) {
            addCriterion("CORP_NAME =", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameNotEqualTo(String value) {
            addCriterion("CORP_NAME <>", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameGreaterThan(String value) {
            addCriterion("CORP_NAME >", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameGreaterThanOrEqualTo(String value) {
            addCriterion("CORP_NAME >=", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameLessThan(String value) {
            addCriterion("CORP_NAME <", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameLessThanOrEqualTo(String value) {
            addCriterion("CORP_NAME <=", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameLike(String value) {
            addCriterion("CORP_NAME like", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameNotLike(String value) {
            addCriterion("CORP_NAME not like", value, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameIn(List<String> values) {
            addCriterion("CORP_NAME in", values, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameNotIn(List<String> values) {
            addCriterion("CORP_NAME not in", values, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameBetween(String value1, String value2) {
            addCriterion("CORP_NAME between", value1, value2, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpNameNotBetween(String value1, String value2) {
            addCriterion("CORP_NAME not between", value1, value2, "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlIsNull() {
            addCriterion("CORP_ROUND_LOGO_URL is null");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlIsNotNull() {
            addCriterion("CORP_ROUND_LOGO_URL is not null");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlEqualTo(String value) {
            addCriterion("CORP_ROUND_LOGO_URL =", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlNotEqualTo(String value) {
            addCriterion("CORP_ROUND_LOGO_URL <>", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlGreaterThan(String value) {
            addCriterion("CORP_ROUND_LOGO_URL >", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlGreaterThanOrEqualTo(String value) {
            addCriterion("CORP_ROUND_LOGO_URL >=", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlLessThan(String value) {
            addCriterion("CORP_ROUND_LOGO_URL <", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlLessThanOrEqualTo(String value) {
            addCriterion("CORP_ROUND_LOGO_URL <=", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlLike(String value) {
            addCriterion("CORP_ROUND_LOGO_URL like", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlNotLike(String value) {
            addCriterion("CORP_ROUND_LOGO_URL not like", value, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlIn(List<String> values) {
            addCriterion("CORP_ROUND_LOGO_URL in", values, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlNotIn(List<String> values) {
            addCriterion("CORP_ROUND_LOGO_URL not in", values, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlBetween(String value1, String value2) {
            addCriterion("CORP_ROUND_LOGO_URL between", value1, value2, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlNotBetween(String value1, String value2) {
            addCriterion("CORP_ROUND_LOGO_URL not between", value1, value2, "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlIsNull() {
            addCriterion("CORP_SQUARE_LOGO_URL is null");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlIsNotNull() {
            addCriterion("CORP_SQUARE_LOGO_URL is not null");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlEqualTo(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL =", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlNotEqualTo(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL <>", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlGreaterThan(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL >", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlGreaterThanOrEqualTo(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL >=", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlLessThan(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL <", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlLessThanOrEqualTo(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL <=", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlLike(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL like", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlNotLike(String value) {
            addCriterion("CORP_SQUARE_LOGO_URL not like", value, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlIn(List<String> values) {
            addCriterion("CORP_SQUARE_LOGO_URL in", values, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlNotIn(List<String> values) {
            addCriterion("CORP_SQUARE_LOGO_URL not in", values, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlBetween(String value1, String value2) {
            addCriterion("CORP_SQUARE_LOGO_URL between", value1, value2, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlNotBetween(String value1, String value2) {
            addCriterion("CORP_SQUARE_LOGO_URL not between", value1, value2, "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeIsNull() {
            addCriterion("CORP_WXQRCODE is null");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeIsNotNull() {
            addCriterion("CORP_WXQRCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeEqualTo(String value) {
            addCriterion("CORP_WXQRCODE =", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeNotEqualTo(String value) {
            addCriterion("CORP_WXQRCODE <>", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeGreaterThan(String value) {
            addCriterion("CORP_WXQRCODE >", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeGreaterThanOrEqualTo(String value) {
            addCriterion("CORP_WXQRCODE >=", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeLessThan(String value) {
            addCriterion("CORP_WXQRCODE <", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeLessThanOrEqualTo(String value) {
            addCriterion("CORP_WXQRCODE <=", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeLike(String value) {
            addCriterion("CORP_WXQRCODE like", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeNotLike(String value) {
            addCriterion("CORP_WXQRCODE not like", value, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeIn(List<String> values) {
            addCriterion("CORP_WXQRCODE in", values, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeNotIn(List<String> values) {
            addCriterion("CORP_WXQRCODE not in", values, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeBetween(String value1, String value2) {
            addCriterion("CORP_WXQRCODE between", value1, value2, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeNotBetween(String value1, String value2) {
            addCriterion("CORP_WXQRCODE not between", value1, value2, "corpWxqrcode");
            return (Criteria) this;
        }

        public Criteria andIdLikeInsensitive(String value) {
            addCriterion("upper(ID) like", value.toUpperCase(), "id");
            return (Criteria) this;
        }

        public Criteria andCorpidLikeInsensitive(String value) {
            addCriterion("upper(CORPID) like", value.toUpperCase(), "corpid");
            return (Criteria) this;
        }

        public Criteria andCorpNameLikeInsensitive(String value) {
            addCriterion("upper(CORP_NAME) like", value.toUpperCase(), "corpName");
            return (Criteria) this;
        }

        public Criteria andCorpRoundLogoUrlLikeInsensitive(String value) {
            addCriterion("upper(CORP_ROUND_LOGO_URL) like", value.toUpperCase(), "corpRoundLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpSquareLogoUrlLikeInsensitive(String value) {
            addCriterion("upper(CORP_SQUARE_LOGO_URL) like", value.toUpperCase(), "corpSquareLogoUrl");
            return (Criteria) this;
        }

        public Criteria andCorpWxqrcodeLikeInsensitive(String value) {
            addCriterion("upper(CORP_WXQRCODE) like", value.toUpperCase(), "corpWxqrcode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}