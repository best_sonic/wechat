package com.sliansoft.wechat.platform.contact.service;

import java.util.List;

import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.Department;

public interface DepService extends BaseService<Department>{
	public List<Department> selectAllByCorp(Corp corp);
	
	public Department selectByPrimaryKey(String depId);
	
	public Department selsctParentDep(String parentId);
	
	public void saveDepartments(List<Department> departments);

	public List<Department> getSubDepartment(Corp corp, int depId);

	void saveEntity(Department entity);

	boolean saveEntity(Department department, String accessToken);

	boolean updateEntity(Department entity, String accessToken);

	boolean deleteEntity(Department entity, String accessToken);
	
	
}
