package com.sliansoft.wechat.platform.contact.service;

public interface BaseService<T> {
	public void saveEntity(T entity);
	public void updateEntity(T entity);
	public void saveOrUpdateEntity(T entity);
	public void deleteEntity(T entity);
}
