package com.sliansoft.wechat.platform.contact.service;

import java.util.List;

import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.Employee;
import com.sliansoft.wechat.platform.contact.utils.Page;

public interface EmpService extends BaseService<Employee> {
	public List<Employee> selectAllByCorp(Corp corp);

	public void saveEmployees(List<Employee> employees);

	public List<Employee> selectAllByCorpAndDep(Corp corp, Department department);

	public List<Employee> getAllEmployeesByCorp(Corp corp);

	public Page<Employee> getAllEmployeesByCorpAndDep(Corp corp, String depId, int pageNum, int pageCapacity);

	public boolean deleteEntity(String accessToken, Employee entity);

	boolean updateEntity(Employee entity, List<String> depId, String accessToken);

	public Employee selectEmpByCorpIdAndUserId(String wxUserId, String corpId);

	public Employee selectByPrimaryKey(String id);
	
	public boolean saveEntity(Employee entity,List<String> depId,String accessToken);

}
