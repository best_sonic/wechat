package com.sliansoft.wechat.platform.contact.service;

import java.util.List;
import java.util.Map;

import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmp;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.Employee;

public interface CorpDepEmpService extends BaseService<CorpDepEmp>{

	void saveContact(String id, Map<Department, List<Employee>> contacts);

	void deleteContactByCorp(Corp corp);
	
	CorpDepEmp selectByEmpId(String empId);
	
	int deleteByEmpId(String empId);

	Map<Department, List<Employee>> importContact(Corp corp, String accessToken);

	void saveEntity(CorpDepEmp entity);
	
}
