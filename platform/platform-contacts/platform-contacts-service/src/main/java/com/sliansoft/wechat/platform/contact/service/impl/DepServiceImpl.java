package com.sliansoft.wechat.platform.contact.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.contacts.DepartmentUtil;
import com.sliansoft.wechat.platform.contact.mapper.CorpDepEmpMapper;
import com.sliansoft.wechat.platform.contact.mapper.DepartmentMapper;
import com.sliansoft.wechat.platform.contact.mapper.EmployeeMapper;
import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmpExample;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.DepartmentExample;
import com.sliansoft.wechat.platform.contact.service.DepService;
@Service("depService")
public class DepServiceImpl implements DepService {
	@Inject
	private DepartmentMapper departmentMapper;
	@Inject
	private CorpDepEmpMapper corpDepEmpMapper;
	@Inject
	private EmployeeMapper employeeMapper;
	
	@Override
	public void saveEntity(Department entity) {
		departmentMapper.insertSelective(entity);
	}
	
	/**
	 * 添加新的部门
	 */
	@Override
	public boolean saveEntity(Department department,String accessToken) {
		
		int condition = DepartmentUtil.createDepartment(accessToken, department);
		if (condition != -1) {
			departmentMapper.insertSelective(department);
			return true;
		}
		return false;
	}

	
	@Override
	public void updateEntity(Department entity) {
		departmentMapper.updateByPrimaryKey(entity);
	}

	/**
	 * 更新部门
	 */
	@Override
	public boolean updateEntity(Department entity,String accessToken) {
		
		boolean condition = DepartmentUtil.updateDepartment(accessToken, entity);
		if (condition) {
			departmentMapper.updateByPrimaryKey(entity);
			return true;
		}
		return false;
	}

	
	@Override
	public void saveOrUpdateEntity(Department entity) {
		if(departmentMapper.selectByPrimaryKey(entity.getId()) != null){
			departmentMapper.updateByPrimaryKey(entity);
		}else{
			departmentMapper.insert(entity);
		}
	}

	@Override
	public void deleteEntity(Department entity) {
		CorpDepEmpExample example=new CorpDepEmpExample();
		example.createCriteria().andDepIdEqualTo(entity.getId());
		List<CorpDepEmp> corpDepEmps=corpDepEmpMapper.selectByExample(example);
		corpDepEmpMapper.deleteByExample(example);
		for (CorpDepEmp corpDepEmp : corpDepEmps) {
			employeeMapper.deleteByPrimaryKey(corpDepEmp.getEmpId());
		}
		departmentMapper.deleteByPrimaryKey(entity.getId());
	}
	/**
	 * 删除部门
	 */
	@Override
	public boolean deleteEntity(Department entity,String accessToken) {
		boolean flag = DepartmentUtil.deleteDepartment(accessToken, entity.getDepartmentid());
		if (flag) {
			CorpDepEmpExample example = new CorpDepEmpExample();
			example.createCriteria().andDepIdEqualTo(entity.getId());
			List<CorpDepEmp> corpDepEmps = corpDepEmpMapper.selectByExample(example);
			corpDepEmpMapper.deleteByExample(example);
			for (CorpDepEmp corpDepEmp : corpDepEmps) {
				employeeMapper.deleteByPrimaryKey(corpDepEmp.getEmpId());
			}
			departmentMapper.deleteByPrimaryKey(entity.getId());
			return true;
		}
		return false;
		
	}
	
	@Override
	public List<Department> selectAllByCorp(Corp corp) {
		return departmentMapper.selectAllByCorp(corp.getCorpid());
	}

	/**
	 * 该Id是表的ID
	 */
	@Override
	public Department selectByPrimaryKey(String depId) {
		return departmentMapper.selectByPrimaryKey(depId);
	}

	@Override
	public Department selsctParentDep(String parentId) {
		DepartmentExample example=new DepartmentExample();
		example.createCriteria().andParentIdEqualTo(parentId);
		return departmentMapper.selectByExample(example).get(0);
	}

	@Override
	public void saveDepartments(List<Department> departments) {
		for (Department department : departments) {
			saveEntity(department);
		}
	}

	@Override
	public List<Department> getSubDepartment(Corp corp, int depId) {
		List<Department> departments = new ArrayList<Department>();
		List<String> depIds = corpDepEmpMapper.selectByCorpIdReturnDep(corp.getId());
		DepartmentExample example = new DepartmentExample();
		example.createCriteria().andIdIn(depIds).andParentIdEqualTo(String.valueOf(depId));
		List<Department> list = departmentMapper.selectByExample(example);
		for(Department department : list){
			departments.addAll(getSubDepartment(corp, department.getDepartmentid()));
		}
		departments.addAll(list);
		System.out.println(departments);
		return departments;
	}
}
