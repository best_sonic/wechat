package com.sliansoft.wechat.platform.contact.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.contacts.EmployeeUtil;
import com.sliansoft.wechat.platform.contact.mapper.CorpDepEmpMapper;
import com.sliansoft.wechat.platform.contact.mapper.CorpMapper;
import com.sliansoft.wechat.platform.contact.mapper.DepartmentMapper;
import com.sliansoft.wechat.platform.contact.mapper.EmployeeMapper;
import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmpExample;
import com.sliansoft.wechat.platform.contact.model.CorpExample;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.DepartmentExample;
import com.sliansoft.wechat.platform.contact.model.Employee;
import com.sliansoft.wechat.platform.contact.model.EmployeeExample;
import com.sliansoft.wechat.platform.contact.service.CorpService;
import com.sliansoft.wechat.platform.contact.service.EmpService;
import com.sliansoft.wechat.platform.contact.utils.Page;

@Service("empService")
public class EmpServiceImpl implements EmpService{

	@Inject
	private EmployeeMapper employeeMapper;
	@Inject
	private CorpDepEmpMapper corpDepEmpMapper;
	@Inject
	private DepartmentMapper departmentMapper;
	@Inject
	private CorpMapper corpMapper;
	
	@Override
	public void saveEntity(Employee entity) {
		
		employeeMapper.insertSelective(entity);
	}
	
	/**
	 * 新增成员
	 * @param entity
	 * @param departments
	 * @param accessToken
	 */
	@Override
	public boolean saveEntity(Employee entity,List<String> depId,String accessToken) {
		DepartmentExample example = new DepartmentExample();
		example.createCriteria().andIdIn(depId);
		List<Department> departments = departmentMapper.selectByExample(example );
		boolean flag = EmployeeUtil.createEmployee(accessToken, entity, departments);
		if (flag) {
			employeeMapper.insertSelective(entity);
			return true;
		}
		return false;
	}

	
	@Override
	public void updateEntity(Employee entity) {
		employeeMapper.updateByPrimaryKey(entity);
	}

	/**
	 * 更新员工
	 * @return 
	 */
	@Override
	public boolean updateEntity(Employee entity,List<String> depId,String accessToken) {
		DepartmentExample example = new DepartmentExample();
		example.createCriteria().andIdIn(depId);
		List<Department> departments = departmentMapper.selectByExample(example );
		boolean flag = EmployeeUtil.updateEmployee(accessToken, entity, departments);
		if (flag) {
			employeeMapper.updateByPrimaryKey(entity);
			return true;
		}
		return false;
	}
	
	@Override
	public void saveOrUpdateEntity(Employee entity) {
		if(employeeMapper.selectByPrimaryKey(entity.getId()) != null){
			employeeMapper.updateByPrimaryKey(entity);
		}else{
			employeeMapper.insert(entity);
		}
	}

	@Override
	public void deleteEntity(Employee entity) {
		employeeMapper.deleteByPrimaryKey(entity.getId());
	}

	@Override
	public boolean deleteEntity(String accessToken,Employee entity) {
		boolean flag = EmployeeUtil.deleteEmployee(accessToken, entity.getUserid());
		if (flag) {
			employeeMapper.deleteByPrimaryKey(entity.getId());
			return true;
		}
		return false;
	}
	
	@Override
	public List<Employee> selectAllByCorp(Corp corp) {
		return employeeMapper.selectAllByCorp(corp.getId());
	}

	@Override
	public List<Employee> selectAllByCorpAndDep(Corp corp, Department department) {
		return employeeMapper.selectAllByCorpAndDep(corp.getId(), department.getId());
	}

	@Override
	public void saveEmployees(List<Employee> employees) {
		if(employees != null){
			for (Employee employee : employees) {
				saveEntity(employee);
			}
		}
	}

	@Override
	public List<Employee> getAllEmployeesByCorp(Corp corp) {
		List<String> employeesId = corpDepEmpMapper.selectByCorpIdReturnEmp(corp.getId());
		EmployeeExample employeeExample=new EmployeeExample();
		employeeExample.createCriteria().andIdIn(employeesId);
		return employeeMapper.selectByExample(employeeExample);
	}

	@Override
	public Page<Employee> getAllEmployeesByCorpAndDep(Corp corp, String depId, int pageNum, int pageCapicity) {
		List<String> employeesId = null;
		if(depId != null)
			employeesId = corpDepEmpMapper.selectByCorpAndDepReturnEmp(corp.getId(), depId);
		else 
			employeesId = corpDepEmpMapper.selectByCorpIdReturnEmp(corp.getId());
		EmployeeExample employeeExample=new EmployeeExample();
		employeeExample.createCriteria().andIdIn(employeesId);
		Page<Employee> page = new Page<Employee>();
		page.setLength(pageCapicity);
		page.setPageNo(pageNum);
		page.setTotalRecords(employeeMapper.countByExample(employeeExample));
		employeeExample.setPage(page);
		employeeExample.setOrderByClause("CONVERT(NAME USING gbk) COLLATE gbk_chinese_ci ASC");
		employeeExample.setDistinct(true);
		page.setPageDatas(employeeMapper.selectByExample(employeeExample));
		return page;
	}

	@Override
	public Employee selectEmpByCorpIdAndUserId(String wxUserId, String corpId) {
		CorpExample example = new CorpExample();
		example.createCriteria().andCorpidEqualTo(corpId);
		Corp corp = corpMapper.selectByExample(example ).get(0);
		CorpDepEmpExample example1 = new CorpDepEmpExample();
		example1.createCriteria().andCorpIdEqualTo(corp.getId());
		List<CorpDepEmp> corpDepEmps = corpDepEmpMapper.selectByExample(example1);
		EmployeeExample example2 = new EmployeeExample();
		List<String> ids=new ArrayList<String>();
		for (CorpDepEmp s:corpDepEmps) {
			ids.add(s.getId());
		}
		example2.createCriteria().andUseridEqualTo(wxUserId).andIdIn(ids);
		return	employeeMapper.selectByExample(example2 ).get(0);
	}

	@Override
	public Employee selectByPrimaryKey(String id) {
		
		return employeeMapper.selectByPrimaryKey(id);
	}

}
