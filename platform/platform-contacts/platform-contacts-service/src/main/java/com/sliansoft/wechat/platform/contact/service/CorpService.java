package com.sliansoft.wechat.platform.contact.service;

import com.sliansoft.wechat.platform.contact.model.Corp;

public interface CorpService extends BaseService<Corp>{
	
	public Corp selectByCorpId(String corpId);

	void deleteEntity(String corpId);

	void saveEntity(Corp entity);
	
}
