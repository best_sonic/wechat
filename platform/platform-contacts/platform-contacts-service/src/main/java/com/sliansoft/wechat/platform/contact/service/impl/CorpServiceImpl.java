package com.sliansoft.wechat.platform.contact.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.platform.contact.mapper.CorpDepEmpMapper;
import com.sliansoft.wechat.platform.contact.mapper.CorpMapper;
import com.sliansoft.wechat.platform.contact.mapper.DepartmentMapper;
import com.sliansoft.wechat.platform.contact.mapper.EmployeeMapper;
import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmpExample;
import com.sliansoft.wechat.platform.contact.model.CorpExample;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.DepartmentExample;
import com.sliansoft.wechat.platform.contact.model.Employee;
import com.sliansoft.wechat.platform.contact.model.EmployeeExample;
import com.sliansoft.wechat.platform.contact.service.CorpService;

@Service("corpService")
public class CorpServiceImpl implements CorpService{
	@Inject
	private CorpMapper corpMapper;

	@Inject
	private CorpDepEmpMapper corpDepEmpMapper;
	
	@Inject
	private DepartmentMapper departmentMapper;
	
	@Inject
	private EmployeeMapper employeeMapper;
	
	@Override
	public void saveEntity(Corp entity) {
		// TODO Auto-generated method stub
		corpMapper.insert(entity);
	}
	
	
	
	@Override
	public void updateEntity(Corp entity) {
		// TODO Auto-generated method stub
		corpMapper.updateByPrimaryKey(entity);
	}

	@Override
	public void saveOrUpdateEntity(Corp entity) {
		
		CorpExample example = new CorpExample();
		example.createCriteria().andCorpidEqualTo(entity.getCorpid());
		
		if( corpMapper.selectByExample(example) != null){
			corpMapper.updateByExampleSelective(entity, example);
		}else{
			corpMapper.insert(entity);
		}
	}

	@Override
	public void deleteEntity(Corp entity) {
		CorpDepEmpExample corpDepEmpExample=new CorpDepEmpExample();
		corpDepEmpExample.createCriteria().andCorpIdEqualTo(entity.getId());
		corpDepEmpMapper.deleteByExample(corpDepEmpExample);
		List<Employee> employeesId=employeeMapper.selectAllByCorp(entity.getId());
		for (Employee employee : employeesId) {
			employeeMapper.deleteByPrimaryKey(employee.getId());
		}
		List<Department> departmentsId=departmentMapper.selectAllByCorp(entity.getCorpid());
		for (Department department : departmentsId) {
			departmentMapper.deleteByPrimaryKey(department.getId());
		}
		corpMapper.deleteByPrimaryKey(entity.getId());
	}
	@Override
	public void deleteEntity(String corpId) {
		CorpExample example = new CorpExample();
		example.createCriteria().andCorpidEqualTo(corpId);
		corpMapper.deleteByExample(example);
	}
	/**
	 * 根據CorpId查詢公司信息，注意，這時可能會返回NULL(若CorpId不存在)
	 */
	@Override
	public Corp selectByCorpId(String corpId) {
		// TODO Auto-generated method stub
		CorpExample corpExample=new CorpExample();
		corpExample.createCriteria().andCorpidEqualTo(corpId);
		return corpMapper.selectByExample(corpExample).get(0);
	}
	
	
}
