package com.sliansoft.wechat.platform.contact.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sliansoft.wechat.contacts.DepartmentUtil;
import com.sliansoft.wechat.contacts.EmployeeUtil;
import com.sliansoft.wechat.platform.contact.mapper.CorpDepEmpMapper;
import com.sliansoft.wechat.platform.contact.mapper.DepartmentMapper;
import com.sliansoft.wechat.platform.contact.mapper.EmployeeMapper;
import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmp;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmpExample;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.DepartmentExample;
import com.sliansoft.wechat.platform.contact.model.Employee;
import com.sliansoft.wechat.platform.contact.model.EmployeeExample;
import com.sliansoft.wechat.platform.contact.service.CorpDepEmpService;
import com.sliansoft.wechat.platform.contact.service.DepService;
import com.sliansoft.wechat.platform.contact.service.EmpService;

@Service("corpDepEmpService")
public class CorpDepEmpServiceImpl implements CorpDepEmpService {

	@Inject
	CorpDepEmpMapper corpDempEmpMapper;
	
	@Inject
	private DepartmentMapper departmentMapper;
	
	@Inject
	private EmployeeMapper employeeMapper;
	
	@Resource(name="depService")
	private DepService depService;
	
	@Resource(name = "empService")
	private EmpService empService;
	
	@Resource(name = "corpDepEmpService")
	private CorpDepEmpService corpDepEmpService;
	
	@Override
	public void saveEntity(CorpDepEmp entity) {
		corpDempEmpMapper.insertSelective(entity);
	}

	@Override
	public void updateEntity(CorpDepEmp entity) {
		corpDempEmpMapper.updateByPrimaryKey(entity);
	}

	@Override
	public void saveOrUpdateEntity(CorpDepEmp entity) {
		if(corpDempEmpMapper.selectByPrimaryKey(entity.getId()) != null){
			corpDempEmpMapper.updateByPrimaryKeySelective(entity);
		}else{
			corpDempEmpMapper.insertSelective(entity);
		}
	}

	@Override
	public void deleteEntity(CorpDepEmp entity) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Map<Department, List<Employee>> importContact(Corp corp, String accessToken) {
		Map<String, List<String>> map = new HashMap<>();
        Map<Department, List<Employee>> contacts = new HashMap<>();
        corpDepEmpService.deleteContactByCorp(corp);
		List<Department> departments=DepartmentUtil.getDepartmentList(accessToken, 1);
		departments.remove(0);
		depService.saveDepartments(departments);
		List<Employee> depEmployees;
		for (int i = 0; i < departments.size(); i++) {
			depEmployees=EmployeeUtil.getEmployeesByDepDetials(accessToken, i+2, 1, 0, map);
			contacts.put(departments.get(i), depEmployees);
			empService.saveEmployees(depEmployees);
		}
		
		corpDepEmpService.saveContact(corp.getId(), contacts);
		System.out.println("END++++++");
		return contacts;
	}

	@Override
	public void saveContact(String id, Map<Department, List<Employee>> contacts) {
		Set<Department> departments=contacts.keySet();
		for (Department department : departments) {
			List<Employee> employees = contacts.get(department);
			if(employees == null || employees.isEmpty()){
				CorpDepEmp corpDepEmp=new CorpDepEmp();
				corpDepEmp.setCorpId(id);
				corpDepEmp.setDepId(department.getId());
				saveEntity(corpDepEmp);
			}else{
				for (Employee employee : employees) {
					CorpDepEmp corpDepEmp=new CorpDepEmp();
					corpDepEmp.setCorpId(id);
					corpDepEmp.setDepId(department.getId());
					corpDepEmp.setEmpId(employee.getId());
					saveEntity(corpDepEmp);
				}
			}
		}
	}

	@Override
	public void deleteContactByCorp(Corp corp) {
		CorpDepEmpExample example = new CorpDepEmpExample();
		example.createCriteria().andCorpIdEqualTo(corp.getId());
		List<CorpDepEmp> corpDepEmps = corpDempEmpMapper.selectByExample(example);
//		System.out.println(corp.toString()+"--------------------------"+corpDepEmps.size());
		corpDempEmpMapper.deleteByExample(example);
		DepartmentExample depExample=new DepartmentExample();
		EmployeeExample empExample=new EmployeeExample();
		Set<String> depId=new HashSet<>();
		Set<String> empId=new HashSet<>();
		for (CorpDepEmp corpDepEmp : corpDepEmps) {
			depId.add(corpDepEmp.getDepId());
			empId.add(corpDepEmp.getEmpId());
		}
//		System.out.println(depId.size()+"----------------"+empId.size());
		for (String string : depId) {
			departmentMapper.deleteByPrimaryKey(string);
		}
		for (String string : empId) {
			employeeMapper.deleteByPrimaryKey(string);
		}
	}

	@Override
	public CorpDepEmp selectByEmpId(String empId) {
		CorpDepEmpExample example = new CorpDepEmpExample();
		example.createCriteria().andEmpIdEqualTo(empId);
		return corpDempEmpMapper.selectByExample(example ).get(0);
	}

	@Override
	public int deleteByEmpId(String empId) {
		CorpDepEmpExample example = new CorpDepEmpExample();
		example.createCriteria().andEmpIdEqualTo(empId);
		return corpDempEmpMapper.deleteByExample(example);
	}

}
