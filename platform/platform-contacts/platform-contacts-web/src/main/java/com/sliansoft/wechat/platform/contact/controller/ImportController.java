package com.sliansoft.wechat.platform.contact.controller;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.Employee;
import com.sliansoft.wechat.platform.contact.service.CorpDepEmpService;
import com.sliansoft.wechat.platform.contact.service.CorpService;
import com.sliansoft.wechat.platform.contact.service.DepService;
import com.sliansoft.wechat.platform.contact.service.EmpService;
import com.sliansoft.wechat.platform.contact.utils.Page;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


@Controller
public class ImportController {

	private final static String PROVIDERCORPID = "wxa75fcc28f7f6401a";
		
	private final static String PROVIDERSECRET = "woqKiDIwa8ywPZSKYmwa0ULGvZcfwaqCSBUR4-qCOBhFL9HFUg_XsopTY_VEiv8I";
	
	@Resource(name = "corpService")
	private CorpService corpService;
	
	@Resource(name = "empService")
	private EmpService empService;

	@Resource(name="depService")
	private DepService depService;
	
	@Resource(name = "corpDepEmpService")
	private CorpDepEmpService corpDepEmpService;
	
	@RequestMapping("/importContact/{corpId}")
	public @ResponseBody JSONObject importContact(@PathVariable String corpId){
		Corp corp= corpService.selectByCorpId(corpId);
		String accessToken="GwUayJs1LyYBefRPcJ3eL4JU27E6n1loqkBdC6T9gkU2ZNxrTkWU2ljAirypDmDMQv-j8eWi0KspwUAQO8QruQ";
		Map<Department, List<Employee>> map = corpDepEmpService.importContact(corp,accessToken);
		TreeSet<Department> departments = new TreeSet<Department>((Department d1, Department d2) -> {
			return d2.getDepOrder() - d1.getDepOrder();
		});
		departments.addAll(map.keySet());
		Department corpDepartment = new Department();
		corpDepartment.setDepartmentid(1);
		corpDepartment.setDepOrder(9999);
		corpDepartment.setId("");
		corpDepartment.setName(corp.getCorpName());
		corpDepartment.setParentId("0");
		departments.add(corpDepartment);
		System.out.println(departments);
		JSONArray depArray = JSONArray.fromObject(departments);
		JSONObject depJSONObject = new JSONObject();
		depJSONObject.put("errcode", 0);
		depJSONObject.put("errmsg", "ok");
		depJSONObject.put("departments", depArray);
		return depJSONObject;
	}
	
//	private String getProviderAccessToken(){
//		String URL=AuthUtil.GET_PROV_TOKEN;
//		Map<String,String> args= new HashMap<>();
//		args.put("corpid", PROVIDERCORPID);
//		args.put("provider_secret", PROVIDERSECRET);
//		String json=JSONObject.fromObject(args).toString();
//		String jsonResult=HTTPUtil.postReq(URL, json,"application/json");
//		System.out.println(jsonResult);
//		Map<String,String> resMap=JSONObject.fromObject(jsonResult);
//		return resMap.get("provider_access_token");
//	}
	@RequestMapping("/getEmployees")
	public @ResponseBody Page<Employee> getEmployees(@RequestParam("pageNum") int pageNum,@RequestParam("pageCapacity") int pageCapacity,@RequestParam("corpId") String corpId, @RequestParam(name="depId", required=false) String depId){
		Long start = System.currentTimeMillis();
		Corp corp= corpService.selectByCorpId(corpId);
		Page<Employee> employees= empService.getAllEmployeesByCorpAndDep(corp,depId, pageNum, pageCapacity);
		System.out.println(employees.toString());
		Long end = System.currentTimeMillis();
		System.out.println("获取员工:" + (end - start));
		return employees;
	} 
	
	@RequestMapping("/getSubDep")
	public @ResponseBody JSONArray getSubDep(@RequestParam("corpId") String corpId,@RequestParam("depId") int depId){
		Long start = System.currentTimeMillis();
		Corp corp= corpService.selectByCorpId(corpId);
		List<Department> subDep= depService.getSubDepartment(corp,depId);
		
		Department corpDepartment = new Department();
		corpDepartment.setDepartmentid(1);
		corpDepartment.setDepOrder(9999);
		corpDepartment.setId("");
		corpDepartment.setName(corp.getCorpName());
		corpDepartment.setParentId("0");
		subDep.add(corpDepartment);
		Long end = System.currentTimeMillis();
		System.out.println("获取部门:" + (end - start));
		return JSONArray.fromObject(subDep);
	}
	
	
}
