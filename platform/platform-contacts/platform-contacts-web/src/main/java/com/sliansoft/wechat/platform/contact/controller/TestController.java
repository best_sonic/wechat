package com.sliansoft.wechat.platform.contact.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sliansoft.wechat.platform.contact.mapper.EmployeeMapper;
import com.sliansoft.wechat.platform.contact.model.Corp;
import com.sliansoft.wechat.platform.contact.model.Employee;
import com.sliansoft.wechat.platform.contact.service.CorpService;
import com.sliansoft.wechat.platform.contact.service.EmpService;
import com.sliansoft.wechat.platform.contact.utils.Page;

@RestController
@RequestMapping("/employee")
public class TestController {
	@Autowired
	CorpService corpService;
	@Autowired
	EmpService empService;
	@Autowired
	EmployeeMapper employeeMapper;

	@RequestMapping(method=RequestMethod.GET)
	public Page<Employee> listAll(@RequestParam("pageNum") int pageNum,
			@RequestParam("pageCapacity") int pageCapacity,
			@RequestParam("corpId") String corpId,
			@RequestParam(name = "depId", required = false) String depId) {
		Corp corp = corpService.selectByCorpId(corpId);
		Page<Employee> employees = empService.getAllEmployeesByCorpAndDep(corp,
				depId, pageNum, pageCapacity);
		return employees;
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public  void update(@RequestBody Employee employee){
		empService.updateEntity(employee);
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public void detele(@RequestParam String id){
		System.out.println(id);
	}
}
