package com.sliansoft.wechat.platform.contact.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sliansoft.wechat.appcenter.service.impl.CorpTokenService;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.service.CorpDepEmpService;
import com.sliansoft.wechat.platform.contact.service.CorpService;
import com.sliansoft.wechat.platform.contact.service.DepService;
import com.sliansoft.wechat.platform.contact.service.EmpService;

@Controller
public class DepOperation {
	
	private final static String SUITEID = "tjabcb7acdfa6aa80e";
	
	@Resource(name = "corpService")
	private CorpService corpService;
	
	@Resource(name = "empService")
	private EmpService empService;

	@Resource(name="depService")
	private DepService depService;
	
	@Resource(name = "corpDepEmpService")
	private CorpDepEmpService corpDepEmpService;
	
	@Resource(name = "corpTokenService")
	private CorpTokenService corpTokenService;
	
	@ResponseBody
	@RequestMapping("/addDep")
	public String addDep(@ModelAttribute Department department,@RequestParam("corpId")String corpId){
		System.out.println("成功进入："+department.toString()+"--------------------------"+corpId);
		String accessToken = corpTokenService.selectByCorpIdAndSuiteId(corpId, SUITEID).getAccessToken();
		boolean flag = depService.saveEntity(department, accessToken);
		if(flag){
			return "success";
		}else{
			return "error";
		}
	}
	
	@RequestMapping("/editDep")
	public String editDep(@ModelAttribute Department department,@RequestParam(required=false)String corpId){
		System.out.println("成功进入："+department.toString()+"--------------------------"+corpId);
		
		String accessToken = corpTokenService.selectByCorpIdAndSuiteId(corpId, SUITEID).getAccessToken();
		boolean flag = depService.updateEntity(department, accessToken);
		if(flag){
			return "success";
		}else{
			return "error";
		}
	}
	
	@ResponseBody
	@RequestMapping("/getDep")
	public Department getDep(String id){
		return depService.selectByPrimaryKey(id);
	}
	
	@RequestMapping("/deleteDep")
	public String deleteDep(@ModelAttribute Department department,@RequestParam(required=false)String corpId){
		System.out.println("成功进入："+department.toString()+"--------------------------"+corpId);
		
		String accessToken = corpTokenService.selectByCorpIdAndSuiteId(corpId, SUITEID).getAccessToken();
		boolean flag = depService.deleteEntity(department, accessToken);
		
		if(flag){
			return "success";
		}else{
			return "error";
		}
	}
}
