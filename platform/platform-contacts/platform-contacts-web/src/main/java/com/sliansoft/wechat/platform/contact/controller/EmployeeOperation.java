package com.sliansoft.wechat.platform.contact.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sliansoft.wechat.appcenter.service.impl.CorpTokenService;
import com.sliansoft.wechat.contacts.EmployeeUtil;
import com.sliansoft.wechat.platform.contact.model.CorpDepEmp;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.Employee;
import com.sliansoft.wechat.platform.contact.service.CorpDepEmpService;
import com.sliansoft.wechat.platform.contact.service.CorpService;
import com.sliansoft.wechat.platform.contact.service.DepService;
import com.sliansoft.wechat.platform.contact.service.EmpService;

@Controller
public class EmployeeOperation {
	
	private final static String SUITEID = "tjabcb7acdfa6aa80e";
	
	
	@Resource(name = "corpService")
	private CorpService corpService;
	
	@Resource(name = "empService")
	private EmpService empService;

	@Resource(name="depService")
	private DepService depService;
	
	@Resource(name = "corpDepEmpService")
	private CorpDepEmpService corpDepEmpService;
	
	@Resource(name = "corpTokenService")
	private CorpTokenService corpTokenService;
	
	@RequestMapping("/addEmp")
	public String addEmp(Employee employee,String corpId,String depIds){
		String accessToken = corpTokenService.selectByCorpIdAndSuiteId(corpId, SUITEID).getAccessToken();
		List<String> depId = getDepIds(depIds);
		boolean flag = empService.saveEntity(employee, depId , accessToken);
		if (flag) {
			return "success";
		}
		return "error";
		
	}
	

	private List<String> getDepIds(String depIds) {
		List<String> deIdList = java.util.Arrays.asList(depIds.trim().split("_"));
		
		return deIdList;
	}
	
	
	@RequestMapping("/editEmp")
	public String editEmp(Employee employee,String corpId,String depIds){
		String accessToken = corpTokenService.selectByCorpIdAndSuiteId(corpId, SUITEID).getAccessToken();
		List<String> depId = getDepIds(depIds);
		boolean flag = empService.updateEntity(employee, depId , accessToken);
		if (flag) {
			return "success";
		}
		return "error";
		
	}
	
	@RequestMapping("/deleteEmp")
	public String deleteEmp(Employee entity,String corpId){
		String accessToken = corpTokenService.selectByCorpIdAndSuiteId(corpId, SUITEID).getAccessToken();
		boolean flag = empService.deleteEntity(accessToken, entity);
		if (flag) {
			return "success";
		}
		return "error";
	}
	
	@RequestMapping("/moveEmp")
	public String moveEmp(String dep_id,String emp_id,String corpId){
		String accessToken = corpTokenService.selectByCorpIdAndSuiteId(corpId, SUITEID).getAccessToken();
		Employee employee = empService.selectByPrimaryKey(emp_id);
		Department department = depService.selectByPrimaryKey(dep_id);
		List<Department> departments = new ArrayList<>();
		departments.add(department);
		boolean flag = EmployeeUtil.updateEmployee(accessToken, employee, departments );
		if (flag) {
			CorpDepEmp corpDepEmp = corpDepEmpService.selectByEmpId(emp_id);
			corpDepEmpService.deleteByEmpId(emp_id);
			corpDepEmp.setDepId(dep_id);
			corpDepEmp.setEmpId(emp_id);
			corpDepEmpService.updateEntity(corpDepEmp);
			return "success";
		}
		return "error";
	}
	
	@ResponseBody
	@RequestMapping("/getEmp")
	public Employee getEmp(String wxUserId,String corpId){
		return empService.selectEmpByCorpIdAndUserId(wxUserId,corpId);
	}
}
