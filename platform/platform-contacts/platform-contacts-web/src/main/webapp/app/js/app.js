	var adminApp=angular.module('adminApp',['ngResource','ngSanitize']);
	adminApp.factory('ListFactory', ['$resource', function($resource){
		return {
			userInfo:$resource('../../employee'),
		};
	}]);

	adminApp.controller('LoadCtrl', ['$scope','ListFactory','$rootScope', function($scope,ListFactory,$rootScope){
		$rootScope.data=ListFactory.userInfo.get({pageNum:1,pageCapacity:10,corpId:"wxa75fcc28f7f6401a"},function success(){
			console.log($rootScope.data);
			ListFactory.userInfo.save($rootScope.data.pageDatas[0]);
			//ListFactory.userInfo.delete($rootScope.data.pageDatas[0]);
		});

		$scope.change=function(){
			$rootScope.data={pageDatas:[{id:1},{id:2}]};
		}

	}]);
