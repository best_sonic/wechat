var setting = {
	/*async: {
		enable: false,
		autoParam: ["id"],
		type: "get",
		url: "/platform-settings-web/getOrgTree"
	},*/
	data: {
		simpleData: {
			enable: true,
			idKey: "id",
			pIdKey: "pId",
			rootPId: 0
		},
		key: {
			children: "nodes",
			name: "name",
			title: "",
		}
	},
	edit: {
		drag: {
			autoExpandTrigger: true,
			isCopy: false,
			isMove: true,
			prev: true,
			next: true,
			inner: true,
			borderMax: 10,
			borderMin: -5,
			minMoveSize: 5,
			maxShowNodeNum: 5,
			autoOpenTime: 0
		},
		editNameSelectAll: false,
		enable: true,
		showRemoveBtn: false,
		showRenameBtn: false
	},
	view: {
		dblClickExpand: true,
		expandSpeed: "fast",
		fontCss: {
			font: "14px 'Microsoft YaHei', '微软雅黑', Arial, Lucida Grande, Tahoma, sans-serif"
		},
		nameIsHTML: true,
		selectedMulti: true,
		showIcon: true,
		showLine: true,
		showTitle: false,
		txtSelectedEnable: false,
		addDiyDom: addDiyDom
	},
	callback: {
		onClick: viewOrgNode,
		beforeDrag: viewOrgBeforeDrag,
		onDrag: viewOrgDrag,
		beforeDrop: viewOrgBeforeDrop,
		onDrop: viewOrgDrop,
		onRightClick: OnRightClick
	}
};

function Department(id, pId, name, depId, corpId){
	this.id = id;
	this.pId = pId;
	this.name = name;
	if(id == 1)
		this.icon = "img/company.png";
	else
		this.icon = "img/department.png";
	this.depId = depId;
	this.corpId = corpId;
}
function addDiyDom(treeId, treeNode) {
	var spaceWidth = 5;
	var switchObj = $("#" + treeNode.tId + "_switch"),
		icoObj = $("#" + treeNode.tId + "_ico");
	switchObj.remove();
	icoObj.before(switchObj);
	if (treeNode.level > 1) {
		var spaceStr = "<span style='display: inline-block;width:" + (spaceWidth * treeNode.level) + "px'></span>";
		switchObj.before(spaceStr);
	}
}
/**
 * 查看组织机构具体信息
 * @param event
 * @param treeId
 * @param treeNode
 */
function viewOrgNode(event, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj(treeId);
	zTree.expandNode(treeNode, null, false, false, false);
}

function viewOrgBeforeDrag(treeId, treeNodes) {
	if (treeNodes[0].pId == "0")
		return false;
}

function viewOrgDrag(event, treeId, treeNodes) {}

function viewOrgBeforeDrop(treeId, treeNodes, targetNode, moveType) {
	//return !(targetNode == null && moveType == "prev" || (moveType != "inner" && !targetNode.id == ''));
}

function viewOrgDrop(event, treeId, treeNodes, targetNode, moveType) {
	// TODO 需要提交请求，保存更改。
}
var zTree, rMenu = $("#rMenu");

function OnRightClick(event, treeId, treeNode) {
	zTree = $.fn.zTree.getZTreeObj(treeId);
	var depId = treeNode.id;
	$("#parentId").val(depId);
	console.log(treeNode.depId);
	if (treeNode.pId == 0) {
		zTree.cancelSelectedNode();
		showRMenu("root", event.clientX, event.clientY);
	} else if (treeNode && !treeNode.noR) {
		zTree.selectNode(treeNode, false);
		showRMenu("node", event.clientX, event.clientY);
	}
}

function showRMenu(type, x, y) {
	$("#rMenu ul").show();
	if (type == "root") {
		$("#m_del").hide();
		$("#m_edit").hide();
		$("#m_addMember").hide();
	} else {
		$("#m_del").show();
		$("#m_edit").show();
		$("#m_addMember").show();
	}
	rMenu.css({
		"top": y + "px",
		"left": x + "px",
		"visibility": "visible"
	});
	$("body").bind("mousedown", onBodyMouseDown);
}

function hideRMenu() {
	if (rMenu) rMenu.css({
		"visibility": "hidden"
	});
	$("body").unbind("mousedown", onBodyMouseDown);
}

function onBodyMouseDown(event) {
	if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length > 0)) {
		rMenu.css({
			"visibility": "hidden"
		});
	}
}
var addCount = 1;

function addTreeNode() {
//	hideRMenu();
//	var newNode = {
//		name: "增加" + (addCount++)
//	};
//	if (zTree.getSelectedNodes()[0]) {
//		newNode.checked = zTree.getSelectedNodes()[0].checked;
//		zTree.addNodes(zTree.getSelectedNodes()[0], newNode);
//	} else {
//		zTree.addNodes(null, newNode);
//	}
	alert($("#parentId").val());
	$.ajax({
		cache: false,
		type: "POST",
		url:baseUrl+"/addDep",	
		data:$('#addDepForm').serialize(),	
		async: false,
		error: function(request) {
			 alert("error:"+data.responseText);
		},
		success: function(data) {
				alert(data);
				if(data == "success"){}
				else{}
		}
	});
	$("#addDep").modal('hide');
	alert($("#depName").val());
}

function removeTreeNode() {
	hideRMenu();
	var nodes = zTree.getSelectedNodes();
	if (nodes && nodes.length > 0) {
		if (nodes[0].children && nodes[0].children.length > 0) {
			var msg = "要删除的节点是父节点，如果删除将连同子节点一起删掉。\n\n请确认！";
			if (confirm(msg) == true) {
				removeDep();
				zTree.removeNode(nodes[0]);
			}
		} else {
			removeDep();
			zTree.removeNode(nodes[0]);
		}
	}
}
function removeDep(){
//	$.ajax({
//		cache: false,
//		type: "POST",
//		url:baseUrl+"/deleteDep",	
//		data:,	
//		async: false,
//		error: function(request) {
//			 alert("error:"+data.responseText);
//		},
//		success: function(data) {
//				if(data == "success"){
//					
//				}
//				else{
//					
//				}
//		}
//	});
	
}
function checkTreeNode(checked) {
	var zTree = $.fn.zTree.getZTreeObj(treeId);
	var nodes = zTree.getSelectedNodes();
	if (nodes && nodes.length > 0) {
		zTree.checkNode(nodes[0], checked, true);
	}
	hideRMenu();
}

function resetTree() {
	hideRMenu();
	$.fn.zTree.init($("#treeDemo"), setting, zNodes);
}

function initOrgTree(treeObj){
	//异步加载数据
	$.ajax({
		type:"get",
		url:baseUrl + "/getSubDep",
		async:true,
		data:"corpId=wxa75fcc28f7f6401a&depId=1",
		success:function(data){
			var nodeArray = [];
			for(var i= 0; i < data.length; i++){
				var node = new Department(data[i].departmentid, data[i].parentId, data[i].name, data[i].id, 'wxa75fcc28f7f6401a');
				nodeArray.push(node);
			}
			console.log(JSON.stringify(nodeArray));
			zTreeObj = $.fn.zTree.init(treeObj, setting, nodeArray);
			zTree_Menu = $.fn.zTree.getZTreeObj("orgTree");
			var treeNodes = zTree_Menu.getSelectedNodes();
			for(var treeNode in treeNodes){
				zTree_Menu.expandNode(treeNode, true, false, true, true);
			}
		}
	});
}
