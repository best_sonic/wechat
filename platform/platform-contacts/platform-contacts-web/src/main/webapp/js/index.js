var baseUrl = "/platform-contacts-web";

function minOne(pageNum){
	if(pageNum - 1 < 1) return 1;
	return pageNum - 1;
}
function maxNum(pageNum, pageCount){
	if(pageNum + 1 > pageCount) return pageCount;
	return pageNum + 1;
}

function eliminateNull(value){
	if(value == null) return "";
	return value;
}

function menuIn(e){
	$("div.lasttdpop").css({'display':'-webkit-flex'});
}

function menuOut(e){
	$("div.lasttdpop").hide();
}
function operationIn(e){
	var hei=e.offsetParent.offsetTop;
	var top1=e.offsetParent.offsetParent.offsetTop;
	$("div.lasttdpop").css({'display':'-webkit-flex','top':hei+top1+9+'px'});
}

function operationOut(e){
	$("div.lasttdpop").hide();
}

function synchronize(){
	var treeObj = $("#orgTree");
	$.ajax({
		type:"get",
		url:baseUrl + "/importContact/wxa75fcc28f7f6401a",
		async:true,
		success:function(result){
			var data = result.departments;
			var nodeArray = [];
			for(var i= 0; i < data.length; i++){
				var node = new Department(data[i].departmentid, data[i].parentId, data[i].name, data[i].id, 'wxa75fcc28f7f6401a');
				nodeArray.push(node);
			}
			zTreeObj = $.fn.zTree.init(treeObj, setting, nodeArray);
			zTree_Menu = $.fn.zTree.getZTreeObj("orgTree");
			var treeNodes = zTree_Menu.getSelectedNodes();
			for(var treeNode in treeNodes){
				zTree_Menu.expandNode(treeNode, true, false, true, true);
			}
		}
	});
	return false;
}

function getEmployees(pageNum, pageCapcity){
	$.ajax({
		type:"get",
		url:baseUrl + "/getEmployees",
		async:true,
		data:"pageNum=" + pageNum + "&pageCapacity=" + pageCapcity + "&corpId=wxa75fcc28f7f6401a",
		success:function(result){
			console.log(result);
			var content = "";
			var pageCode = "";
			var employees = result.pageDatas;
			for(var i = 0; i < employees.length; i++){
				content += "<tr name='pageData'>" + 
						            	"<td><input type='checkbox' class='form-checkbox' value='" + eliminateNull(employees[i].id) + "' name='ids' defaultvalue=''> </td>" +
					                    "<td class='name_icon'><img src='" + eliminateNull(employees[i].avatar) + "'></td>" +
					                    "<td class='name'>" +
					                    	 "<span class='name' name='personName'>" + eliminateNull(employees[i].name) + "</span>" +
					                    "</td>" +
					                    "<td name='wxUserId'>" + eliminateNull(employees[i].userid) + "</td>" +
					                    "<td name='mobile'>" + eliminateNull(employees[i].mobile) + "</td>" +
					                    "<td name='position'>" + eliminateNull(employees[i].position) + "</td>" +
					                    "<td class='name' name='userStatusDesc'>" + getStatus(employees[i].status) + "</td>" +
					                    "<td class='lasttd' '>" +
					                    "<button class='btn btn-default' onmouseover='operationIn(this)' onmouseout='operationOut(this)>" +
				                    		"操作" +
				                    		"<span class='glyphicon glyphicon-menu-hamburger'></span>" +
				                    	"</button>" +
					                    	"<div class='lasttdpop' style='display: none;' onmouseover='menuIn(this)' onmouseout='menuOut(this)'>"+
					                            "<a href='#'>查看</a>"+
					                            "<a href='#'>编辑</a>"+
					                            "<a href='#'>删除</a>"+
				                            "</div>"+
					                    "</td>" +
						            "</tr>";
			}
			$('.main-container table tbody').html(content);
			var pageCount = result.pageCount;
			var pageNo = result.pageNo;
			var pageContent = "<ul class='pager' id='pager-ul'>";
			pageContent += "<li class='previous'><a class='a-pager' href='javascript:void(0);' onclick='getEmployees(" + minOne(pageNo) + ", " + pageCapcity + ")'><span aria-hidden='true'>&larr;</span> Older</a></li>";
			for(var i = 1; i <= pageCount; i++){
				if(i == pageNo){
					pageContent += "<li><a class='a-pager' href='javascript:void(0)'>" + i + "</a></li>";
				}
				else{
					pageContent += "<li><a class='a-pager' href='javascript:void(0)' onclick='getEmployees(" + i + ", " + pageCapcity + ")'>" + i + "</a></li>";
				}
			}
			pageContent += " <li class='next'><a  class='a-pager' href='javascript:void(0);' onclick='getEmployees(" + maxNum(pageNo, pageCount) + ", " + pageCapcity + ")'>Newer <span aria-hidden='true'>&rarr;</span></a></li></ul>";
			$('.main-container .row .nav-page').html(pageContent);
			
			 $(".lasttd .dropdown-toggle").dropdown('toggle');
		}
	});
	return false;
}

function getStatus(status){
	if(status == "1"){
		return "已关注";
	}
	else if(status == "2"){
		return "已冻结";
	}
	else if(status == "4"){
		return "未关注";
	}
	return "未知";
}

$(function(){
	var treeObj = $("#orgTree");
	var checkedTree = $('#checkedOrgTree');
	var selectTree = $('#depTree');
	initCheckOrgTree(checkedTree);
	initOrgTree(treeObj);
	initSelectDepTree(selectTree);
	
	getEmployees(1, 10);
	
	
	$("#orgTree").mousedown(function(e){ 
        if(3 == e.which){ 
          
       } 
     });
	
	$(".pull-right div.dropdown").mouseover(function(){
		$("img.img-circle+ul.dropdown-menu").show(0);
	});
	$(".pull-right div.dropdown").mouseout(function(){
		$("ul.dropdown-menu").delay(300).hide(0);
	});
	
	$('input').iCheck({
	    checkboxClass: 'icheckbox_square-blue',
	    radioClass: 'iradio_square-blue',
	    increaseArea: '20%' // optional
	  });
});