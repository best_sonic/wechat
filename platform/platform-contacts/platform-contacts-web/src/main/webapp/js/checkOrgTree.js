var settings = {
	data: {
		simpleData: {
			enable: true,
			idKey: "id",
			pIdKey: "pId",
			rootPId: 0
		},
		key: {
			children: "nodes",
			name: "name",
			title: "",
		}
	},
	check: {
		enable: true,
		autoCheckTrigger: true,
		chkboxType: {
			"Y": "",
			"N": ""
		},
		chkStyle: "radio",
		radioType: "all",
		nocheckInherit: true
	},
	edit: {
		enable: false,
	},
	view: {
		dblClickExpand: true,
		expandSpeed: "fast",
		fontCss: {
			font: "14px 'Microsoft YaHei', '微软雅黑', Arial, Lucida Grande, Tahoma, sans-serif"
		},
		nameIsHTML: true,
		selectedMulti: true,
		showIcon: true,
		showLine: true,
		showTitle: false,
		txtSelectedEnable: false,
		addDiyDom: addDiyDom
	},
	callback: {
		onClick: viewOrgNode,
	}
};
var nodes = [{
	"id": 1,
	"pId": 0,
	"name": "敏智微服",
	"icon": "img/company.png",
	"depId": "cc2b68c8-af21-11e5-afd8-00163f000707",
	"corpId": "wxa75fcc28f7f6401a"
},{
	"id": 7,
	"pId": 6,
	"name": "1234",
	"icon": "img/department.png",
	"depId": "cc2b68c8-af21-11e5-afd8-00163f000707",
	"corpId": "wxa75fcc28f7f6401a"
}, {
	"id": 6,
	"pId": 5,
	"name": "123",
	"icon": "img/department.png",
	"depId": "cc273a12-af21-11e5-afd8-00163f000707",
	"corpId": "wxa75fcc28f7f6401a"
}, {
	"id": 2,
	"pId": 1,
	"name": "开发部",
	"icon": "img/department.png",
	"depId": "cc169dec-af21-11e5-afd8-00163f000707",
	"corpId": "wxa75fcc28f7f6401a"
}, {
	"id": 3,
	"pId": 1,
	"name": "市场部",
	"icon": "img/department.png",
	"depId": "cc1ac44d-af21-11e5-afd8-00163f000707",
	"corpId": "wxa75fcc28f7f6401a"
}, {
	"id": 4,
	"pId": 1,
	"name": "设备科",
	"icon": "img/department.png",
	"depId": "cc1eeb62-af21-11e5-afd8-00163f000707",
	"corpId": "wxa75fcc28f7f6401a"
}, {
	"id": 5,
	"pId": 1,
	"name": "云计算组",
	"icon": "img/department.png",
	"depId": "cc2363a7-af21-11e5-afd8-00163f000707",
	"corpId": "wxa75fcc28f7f6401a"
}];

/**
 * 勾选前的验证
 * @param treeId
 * @param treeNode
 */
function beforNodeCheck(treeId, treeNode) {
	if (treeNode.nodeId == undefined || treeNode.nodeId == "") {
		return false;
	} else {
		if (!treeNode.checked) {
			var zTree = $.fn.zTree.getZTreeObj(treeId);
			var nodes = zTree.getCheckedNodes(true);
			$.each(nodes, function(i, node) {
				node.checked = false;
				zTree.updateNode(node);
			});
			//zTree.cancelSelectedNode();
			//var nodes = zTree.getNodes();
			/* if (oldNodes) {
	            	oldNodes.checked = false;
	            	zTree.updateNode(oldNodes);
	            	//zTree.cancelSelectedNode(oldNodes);
	            }
	        	oldNodes = treeNode; */
		}
		checkNodes = treeNode;
		return true;
	}
}

function addDiyDom(treeId, treeNode) {
	var spaceWidth = 5;
	var switchObj = $("#" + treeNode.tId + "_switch"),
		icoObj = $("#" + treeNode.tId + "_ico");
	switchObj.remove();
	icoObj.before(switchObj);
	if (treeNode.level > 1) {
		var spaceStr = "<span style='display: inline-block;width:" + (spaceWidth * treeNode.level) + "px'></span>";
		switchObj.before(spaceStr);
	}
}
/**
 * 查看组织机构具体信息
 * @param event
 * @param treeId
 * @param treeNode
 */
function viewOrgNode(event, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj(treeId);
	zTree.expandNode(treeNode);
}

var zTree, rMenu = $("#rMenu");


function onBodyMouseDown(event) {
	if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length > 0)) {
		rMenu.css({
			"visibility": "hidden"
		});
	}
}
var addCount = 1;


function checkTreeNode(checked) {
	var zTree = $.fn.zTree.getZTreeObj(treeId);
	var nodes = zTree.getSelectedNodes();
	if (nodes && nodes.length > 0) {
		zTree.checkNode(nodes[0], checked, true);
	}
	hideRMenu();
}

function initCheckOrgTree(treeObj) {
	//异步加载数据
	zTreeObj = $.fn.zTree.init(treeObj, settings, nodes);
	zTree_Menu = $.fn.zTree.getZTreeObj("checkedOrgTree");
}