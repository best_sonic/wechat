package com.sliansoft.wechat.contacts.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.sliansoft.wechat.contacts.DepartmentUtil;
import com.sliansoft.wechat.contacts.EmployeeUtil;
import com.sliansoft.wechat.platform.contact.model.Department;
import com.sliansoft.wechat.platform.contact.model.Employee;

import net.sf.json.JSONObject;

public class DepTest {
	
	
	
	@Test
	public void getDepList(){
		String accessToken="r0N2p5-vGSBOBGThZAlr4e37GSUVcN1uNXrZ3aMPgJKGDoLFbhOt7Ryhg5UbEpqorgYB7lIeeoOnD2yf4xsOQw";
        Map<String, List<String>> map=new HashMap<>();
//		List<Department> departments=DepartmentUtil.getDepartmentList( accessToken,0);
        List<Employee> employees = EmployeeUtil.getEmployeesByDepDetials(accessToken,2,1,0,map);
		System.out.println(employees.toString());
		System.out.println(map.toString());
	}

	@Test
	public void getEmployeeByDep(){
		
		String accessToken="r0N2p5-vGSBOBGThZAlr4e37GSUVcN1uNXrZ3aMPgJKGDoLFbhOt7Ryhg5UbEpqorgYB7lIeeoOnD2yf4xsOQw";
        Map<String, List<String>> map=new HashMap<>();
        Map<Department, List<Employee>> contacts=new HashMap<>();
		List<Department> departments=DepartmentUtil.getDepartmentList( accessToken,1);
		List<Employee> employees;
		for (int i = 1; i < departments.size()+1; i++) {
			employees=EmployeeUtil.getEmployeesByDepDetials(accessToken,i,1,0,map);
			contacts.put(departments.get(i-1), employees);
		} 

	}
	
	@Test
	public void getGender(){
		JSONObject responseJSON=JSONObject.fromObject("{\"userid\":\"zhongyushuo\",\"name\":\"仲宇烁\",\"department\":[2],\"mobile\":\"13586910629\",\"gender\":\"1\",\"email\":\"364623397@qq.com\",\"weixinid\":\"zhongyushuo1993\",\"avatar\":\"http://shp.qpic.cn/bizmp/BpdL5jfsSZGFibkL2ECeyL0WgBibMvia1djk5QibmJ5DJPMyats0E1Jq1Q/\",\"status\":1}");
		System.out.println(responseJSON.getString("gender"));
	}
	
}	


