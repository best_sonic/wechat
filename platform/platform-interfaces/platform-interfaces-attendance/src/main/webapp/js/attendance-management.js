/**
 * 
 */
var jsonRules = null;
$(function() {
	$.ajax({
		url : "/platform-interfaces-attendance/getRules",
		type : "POST",
		async : false,
		dataType : "json",
		success : function(data) {
			jsonRules = data;
			console.log(data);
			var itemList = "";
			for (var key in data) {
				var item = "<tr class='row'>"+
								"<td class='col-md-2'>" + data[key].ruleName + "</td>"+
								"<td class='col-md-2'>" + data[key].creator + "</td>"+
								"<td class='col-md-3'>" + data[key].createTime + "</td>"+
								"<td class='col-md-4'>" + data[key].centerStr + "</td>"+
								"<td class='col-md-1'>"+
								"<span class='glyphicon glyphicon-menu-hamburger' onmouseover='operationIn(this)' onmouseout='operationOut(this)'></span>"+
									"<div class='lasttdpop' style='display: none;' onmouseover='menuIn(this)' onmouseout='menuOut(this)'>"+
										"<a href='#' onclick='ruleDetail(" + key +");'>详细</a>"+
										"<a href='#' onclick='editRule(" + key + ");'>编辑</a>"+
										"<a href='#' onclick='deleteRule(" + key + ");'>删除</a>"+
										"<a href='#' onclick='viewResult(" + key + ");'>查看结果</a>"+
									"</div></td></tr>";
				
				itemList = itemList + item;
			}
			$("#already tbody").append(itemList);
		},
		
		error : function(data) {
			alert("请求失败 " + data);
		}
	})
})

function menuIn(e) {
	$(e).css({
		'display' : '-webkit-flex'
	});
}

function menuOut(e) {
	$(e).hide();
}

function operationIn(e) {
	var hei = e.offsetTop;
	var top1 = e.offsetParent.offsetTop;
	$(e).siblings(".lasttdpop").css({
		'display' : '-webkit-flex',
		'top' : hei + top1 + 9 + 'px'
	});
}

function operationOut(e) {
	$(e).siblings(".lasttdpop").hide();
}

function ruleDetail(num) {
	var currentRule = jsonRules[num];
	var ruleDetailModal = document.getElementById("ruleDetailModal");
	$("#ruleState").html(currentRule.ruleState);
	$("#ruleName").html(currentRule.ruleName);
	$("#creator").html(currentRule.creator);
	$("#createTime").html(currentRule.createTime);
	$("#checkInTime").html(currentRule.checkInTime);
	$("#checkInPeriod").html(currentRule.checkInPeriod);
	$("#checkOutTime").html(currentRule.checkOutTime);
	$("#checkOutPeriod").html(currentRule.checkOutPeriod);
	$("#delayTime").html(currentRule.delayTime);
	$("#leadTime").html(currentRule.leadTime);
	$("#checkInRemindTime").html(currentRule.checkInRemindTime);
	$("#checkOutRemindTime").html(currentRule.checkOutRemindTime);
	$("#circuled").html(currentRule.circuled);
	$("#radius").html(currentRule.radius);
	$("#circlue").html(currentRule.circlue);
	$("#centerStr").html(currentRule.centerStr);
	$("#users").html(currentRule.users);
}

function editRule(num) {

}
