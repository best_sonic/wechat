package com.sliansoft.wechat.attendance.entity;

/**
 * <p>
 * Description:js-sdk配置新的实体类
 * </p>
 * 
 * @author zheng
 * @time 2016年1月7日 上午10:05:28
 */

public class ConfigInfo {

	private String corpId;
	private String timestamp;
	private String nonceStr;
	private String signature;

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "ConfigInfo [corpId=" + corpId + ", timestamp=" + timestamp + ", nonceStr=" + nonceStr + ", signature="
				+ signature + "]";
	}

}
