package com.sliansoft.wechat.attendance.entity;

import java.io.Serializable;

/**
 * <p>
 * Description:表示考勤结果的实体类，首次点击签到或者签退会产生一条考勤记录，
 * 第二次点击同一条考勤规则的签到或者签退会直接更新原有的记录
 * </p>
 * 
 * @author zheng
 * @time 2015年12月30日 上午9:56:19
 */

/**
 * @author acer
 * @date 2015年12月30日
 */
public class AttendanceResult implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//下面的属性与数据表对应
	private String resultId;
	private String checkInTime;// 签到和签退时间用时间戳表示
	private String checkOutTime;
	private String resultStr;//结果描述，正常、迟到、早退、未签到、未签退、未考勤、位置偏移
	private String checkInPosition;
	private String checkOutPosition;
	
	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(String checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public String getResultStr() {
		return resultStr;
	}

	public void setResultStr(String resultStr) {
		this.resultStr = resultStr;
	}

	public String getCheckInPosition() {
		return checkInPosition;
	}

	public void setCheckInPosition(String checkInPosition) {
		this.checkInPosition = checkInPosition;
	}

	public String getCheckOutPosition() {
		return checkOutPosition;
	}

	public void setCheckOutPosition(String checkOutPosition) {
		this.checkOutPosition = checkOutPosition;
	}

}
