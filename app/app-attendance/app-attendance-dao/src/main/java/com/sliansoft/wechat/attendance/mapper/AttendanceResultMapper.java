package com.sliansoft.wechat.attendance.mapper;

import java.util.List;

import com.sliansoft.wechat.attendance.entity.CustomAttendanceResult;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2016年1月13日 上午10:25:02
 */

public interface AttendanceResultMapper {
	List<CustomAttendanceResult> getAttendanceResult(String userId);

}
