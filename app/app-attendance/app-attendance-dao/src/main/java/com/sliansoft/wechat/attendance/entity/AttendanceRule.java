package com.sliansoft.wechat.attendance.entity;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2015年12月10日 下午3:08:26
 */

public class AttendanceRule {
	private String ruleId;// 规则的唯一标识，uuid生成

	private String ruleName;// 规则名称

	private String corpId;// 规则所属的企业

	private String creator;// 规则创建者

	private String createTime;// 规则创建时间

	private boolean ruleState;// 规则启用状态，默认为false

	private String centerPoint;// 签到的中心位置，经纬度对应的字符串

	private String centerStr;// 签到的中心位置，地名表示

	private int radius;// 签到有效范围半径

	private String checkInTime;// 签到时间，介于签到开始时间可结束时间之间

	private int delayTime;// 允许迟到的时间，值为非负

	private String checkInStartTime;// 签到开始时间

	private String checkInEndTime;// 签到结束时间

	private boolean checkInRemindState;// 是否在签到之前提醒

	private int checkInRemindTime;// 提前多长时间提醒签到，只有当checkInRemindState为true时才有用

	private String checkOutTime;// 签退时间

	private int leadTime;// 允许提前时间

	private String checkOutStartTime;// 签退开始时间

	private String checkOutEndTime;// 签退结束时间

	private boolean checkOutRemindState;// 是否在签退之前提醒

	private int checkOutRemindTime;// 是否在签退之前提醒

	private String circule;// 循环方式，星期日到星期一分别用0-6表示

	private boolean circuled;// 用于标记考勤规则是一次性的还是的循环的

	private String exclude;// 签到排除时间

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public boolean isRuleState() {
		return ruleState;
	}

	public void setRuleState(boolean ruleState) {
		this.ruleState = ruleState;
	}

	public String getCenterPoint() {
		return centerPoint;
	}

	public void setCenterPoint(String centerPoint) {
		this.centerPoint = centerPoint;
	}

	public String getCenterStr() {
		return centerStr;
	}

	public void setCenterStr(String centerStr) {
		this.centerStr = centerStr;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public int getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(int delayTime) {
		this.delayTime = delayTime;
	}

	public String getCheckInStartTime() {
		return checkInStartTime;
	}

	public void setCheckInStartTime(String checkInStartTime) {
		this.checkInStartTime = checkInStartTime;
	}

	public String getCheckInEndTime() {
		return checkInEndTime;
	}

	public void setCheckInEndTime(String checkInEndTime) {
		this.checkInEndTime = checkInEndTime;
	}

	public boolean isCheckInRemindState() {
		return checkInRemindState;
	}

	public void setCheckInRemindState(boolean checkInRemindState) {
		this.checkInRemindState = checkInRemindState;
	}

	public int getCheckInRemindTime() {
		return checkInRemindTime;
	}

	public void setCheckInRemindTime(int checkInRemindTime) {
		this.checkInRemindTime = checkInRemindTime;
	}

	public String getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(String checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public int getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(int leadTime) {
		this.leadTime = leadTime;
	}

	public String getCheckOutStartTime() {
		return checkOutStartTime;
	}

	public void setCheckOutStartTime(String checkOutStartTime) {
		this.checkOutStartTime = checkOutStartTime;
	}

	public String getCheckOutEndTime() {
		return checkOutEndTime;
	}

	public void setCheckOutEndTime(String checkOutEndTime) {
		this.checkOutEndTime = checkOutEndTime;
	}

	public boolean isCheckOutRemindState() {
		return checkOutRemindState;
	}

	public void setCheckOutRemindState(boolean checkOutRemindState) {
		this.checkOutRemindState = checkOutRemindState;
	}

	public int getCheckOutRemindTime() {
		return checkOutRemindTime;
	}

	public void setCheckOutRemindTime(int checkOutRemindTime) {
		this.checkOutRemindTime = checkOutRemindTime;
	}

	public String getCircule() {
		return circule;
	}

	public boolean isCirculed() {
		return circuled;
	}

	public void setCirculed(boolean circuled) {
		this.circuled = circuled;
	}

	public void setCircule(String circule) {
		this.circule = circule;
	}

	public String getExclude() {
		return exclude;
	}

	public void setExclude(String exclude) {
		this.exclude = exclude;
	}
}
