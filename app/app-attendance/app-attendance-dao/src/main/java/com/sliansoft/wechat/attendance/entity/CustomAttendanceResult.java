package com.sliansoft.wechat.attendance.entity;

/**
 * <p>Description:</p>
 * @author zheng
 * @time 2016年1月13日 上午9:50:28
 */

public class CustomAttendanceResult extends AttendanceResult {
	
	private String ruleName;
	private String creator;

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
}
