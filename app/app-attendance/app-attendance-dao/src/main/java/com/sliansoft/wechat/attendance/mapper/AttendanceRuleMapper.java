package com.sliansoft.wechat.attendance.mapper;

import java.util.List;

import com.sliansoft.wechat.attendance.entity.AttendanceResult;
import com.sliansoft.wechat.attendance.entity.AttendanceRule;
import com.sliansoft.wechat.attendance.entity.CustomAttendanceRule;
import com.sliansoft.wechat.attendance.entity.ResultTrans;

/**
 * <p>Description:</p>
 * @author zheng
 * @time 2015年12月6日 下午11:03:26
 */

public interface AttendanceRuleMapper {

	String getAccessTokenByCorpId(String corpId);

	List<CustomAttendanceRule> getAttendanceRuleByCorpIdAndUserId(String corpId, String userId);

	AttendanceRule getAttendanceRuleByRuleId(String ruleId);

	AttendanceResult getAttendanceResultInfo(String ruleId, String userId,  String now);

	void insertCheckInResult(ResultTrans trans);
	
	void insertCheckOutResult(ResultTrans trans);

	void updateCheckOutResult(ResultTrans trans);

	void insertUserRuleResult(String resultId, String ruleId, String userId, String now);

	
}

