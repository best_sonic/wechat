package com.sliansoft.wechat.attendance.entity;

import java.util.List;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2016年1月21日 下午4:21:46
 */

public class CustomAttendanceRule extends AttendanceRule {

	private List<AttendanceUser> listUsers;

	public List<AttendanceUser> getListUsers() {
		return listUsers;
	}

	public void setListUsers(List<AttendanceUser> listUsers) {
		this.listUsers = listUsers;
	}

}
