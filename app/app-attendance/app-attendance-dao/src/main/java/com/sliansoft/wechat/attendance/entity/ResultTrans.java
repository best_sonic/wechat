package com.sliansoft.wechat.attendance.entity;

/**
 * <p>
 * Description:这个类用于封装从前端传递过来的json数据，签到和签退的数据都用这个类保存
 * </p>
 * 
 * @author zheng
 * @time 2015年12月30日 上午10:08:01
 */

public class ResultTrans {

	public String resultId;
	public String ruleId;
	public String userId;
	public String userRuleId;// 规则和用户关联表的id
	public String currentTime;// 当前时间
	public String checkTime;// 规则规定的考勤时间
	public String marginTime;// 允许误差的时间
	public String centerStr;// 规则中规定的签到位置
	public String radius;// 规则中允许误差的范围
	public String latitude; // 考勤位置的经纬度
	public String longitude;
	public String currentPositionStr;// 考勤地理位置
	public String resultStr;// 考勤结果描述

	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserRuleId() {
		return userRuleId;
	}

	public void setUserRuleId(String userRuleId) {
		this.userRuleId = userRuleId;
	}

	public String getCurrentTime() {
		return currentTime;
	}

	public String getCurrentPositionStr() {
		return currentPositionStr;
	}

	public void setCurrentPositionStr(String currentPositionStr) {
		this.currentPositionStr = currentPositionStr;
	}

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	public String getMarginTime() {
		return marginTime;
	}

	public void setMarginTime(String marginTime) {
		this.marginTime = marginTime;
	}

	public String getCenterStr() {
		return centerStr;
	}

	public void setCenterStr(String centerStr) {
		this.centerStr = centerStr;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getResultStr() {
		return resultStr;
	}

	public void setResultStr(String resultStr) {
		this.resultStr = resultStr;
	}

}
