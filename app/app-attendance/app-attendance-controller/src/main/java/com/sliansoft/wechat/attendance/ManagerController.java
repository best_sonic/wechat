package com.sliansoft.wechat.attendance;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sliansoft.wechat.attendance.manageservice.ManageService;

import net.sf.json.JSONArray;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2015年11月23日 下午9:11:38
 */
@Controller
public class ManagerController {

	@Autowired
	ManageService manageService;

	@RequestMapping("getRules")
	public @ResponseBody JSONArray getRules(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
//		String userId = (String) session.getAttribute("userId");
//		String corpId = (String) session.getAttribute("corpId");
		String userId = "zdwssq";
		String corpId = "wxa75fcc28f7f6401a";
		JSONArray jsonRules = manageService.getRules(userId, corpId);
		return jsonRules;
	}

}
