package com.sliansoft.wechat.attendance.manageservice.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sliansoft.wechat.attendance.entity.AttendanceRule;
import com.sliansoft.wechat.attendance.entity.CustomAttendanceRule;
import com.sliansoft.wechat.attendance.manageservice.ManageService;
import com.sliansoft.wechat.attendance.mapper.AttendanceRuleMapper;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2015年12月10日 下午10:35:10
 */
@Service("manageService")
public class ManageServiceImpl implements ManageService {

	@Autowired
	private AttendanceRuleMapper attendanceRuleMapper;

	@Override
	public JSONArray getRules(String userId, String corpId) {
		List<CustomAttendanceRule> rules = attendanceRuleMapper.getAttendanceRuleByCorpIdAndUserId(corpId, userId);
		Iterator<CustomAttendanceRule> iterator = rules.listIterator();
		JSONArray jsonRules = new JSONArray();

		while (iterator.hasNext()) {
			AttendanceRule rule = iterator.next();
			rule.setCheckInStartTime(rule.getCheckInStartTime().substring(11, 16));
			rule.setCheckInEndTime(rule.getCheckInEndTime().substring(10, 11));
			rule.setCheckOutStartTime(rule.getCheckOutStartTime().substring(10, 11));
			rule.setCheckOutEndTime(rule.getCheckOutEndTime().substring(10, 11));

			jsonRules.add(JSONObject.fromObject(rule));
		}

		return jsonRules;
	}

}
