package com.sliansoft.wechat.attendance.kit;

/**
 * <p>
 * Description:在这个类中包含程序中需要使用的常量
 * </p>
 * 
 * @author zheng
 * @time 2015年11月6日 下午2:09:45
 */

public class ContextValue {

	public static final String[] RESULT_STR = { "正常", "迟到", "早退", "签到地点错误", "签退地点错误", "迟到且早退", ""};

	// 这两个参数都是在创建套件的时候随机生成的，用于对应用收到的信息进行解码，这几个参数只在验证回调地址的时候有用
	public static final String TOKEN = "qtSeLrmzUWDST";
	public static final String ENCOGINGAESKEY = "3AebBuyULOCc7U1hgEsGNpI87fRqge8ohQcjgys2Opc";
	public static final String CORPID = "wxa75fcc28f7f6401a";

	//根据企业corpId和组管凭证秘钥获取access_token
	public static final String GETACCESSTOKEN = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=corpId&corpsecret=corpSecret";
	
	//根据CODE参数获取用户信息
	public static final String GETUSERINFO = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE";

	//获取js-sdk的签名
	public static final String GETSIGNATURE = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=ACCESS_TOKEN";
}
