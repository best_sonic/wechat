package com.sliansoft.wechat.attendance.kit;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

/**
 * <p>
 * Description:向百度地图API请求数据
 * </p>
 * 
 * @author zheng
 * @time 2016年1月9日 下午2:08:15
 */

public class BaiduMap {
	// 地球半径
	private static double EARTH_RADIUS = 6378.137;

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	public static int getDistance(String lat1Str, String lng1Str, String lat2Str, String lng2Str) {
		Double lat1 = Double.parseDouble(lat1Str);
		Double lng1 = Double.parseDouble(lng1Str);
		Double lat2 = Double.parseDouble(lat2Str);
		Double lng2 = Double.parseDouble(lng2Str);

		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double difference = radLat1 - radLat2;
		double mdifference = rad(lng1) - rad(lng2);
		double distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(difference / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(mdifference / 2), 2)));
		distance = distance * EARTH_RADIUS;
		distance = Math.round(distance * 10000) / 10;
		String distanceStr = distance + "";
		distanceStr = distanceStr.substring(0, distanceStr.indexOf("."));

		return Integer.valueOf(distanceStr);
	}
	
//	public static void main(String[] args) {
//        //济南国际会展中心经纬度：117.11811  36.68484
//        //趵突泉：117.00999000000002  36.66123
//        System.out.println(getDistance("39.9074648219","116.3917350769","39.9080244523","116.4115619659"));
//    }

	/**
	 * 获取两点之间的直线距离
	 * 
	 * @param centerStr
	 *            第一个点用地点描述
	 * @param latitude
	 *            第二个点用经纬度描述
	 * @param longitude
	 * @return
	 */
	public static int getDistance(String centerStr, String latitude, String longitude) {
		// 将centerStr转换成经纬度（不带回调函数）
		String url = "http://api.map.baidu.com/geocoder/v2/?address=ADDRESS&output=json&ak=9bj8Ci5UU2grnGiBxwAv1ymG";
		CloseableHttpClient client = null;
		CloseableHttpResponse resp = null;
		String lng = null;
		String lat = null;
		int distance = 0;

		client = HttpClientBuilder.create().build();
		url = url.replace("ADDRESS", centerStr);
		HttpPost post = new HttpPost(url);

		try {
			resp = client.execute(post);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("执行post请求时出错");
		}

		int statusCode = resp.getStatusLine().getStatusCode();

		if (statusCode >= 200 && statusCode < 300) {
			HttpEntity entity = resp.getEntity();

			String content = null;
			try {
				content = EntityUtils.toString(entity);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				// 这里最好再进行一下判断
				JSONObject obj = JSONObject.fromObject(content);
				JSONObject location = obj.getJSONObject("result").getJSONObject("location");
				lng = location.getString("lng");
				lat = location.getString("lat");
				distance = getDistance(lat, lng, latitude, longitude);
			} catch (JSONException jsonException) {
				jsonException.printStackTrace();
				System.out.println("地址转换时出错");
			}
		}
		return distance;
	}

}
