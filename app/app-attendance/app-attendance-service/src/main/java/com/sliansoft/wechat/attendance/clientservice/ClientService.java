package com.sliansoft.wechat.attendance.clientservice;

import com.sliansoft.wechat.attendance.entity.ResultTrans;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * <p>Description:</p>
 * @author zheng
 * @time 2015年12月10日 下午3:03:57
 */

public interface ClientService {
	
	JSONArray getTodayAttendance(String corpId, String userId);

	String getAccessTokenByCorpId(String corpId);

	JSONObject getRuleDetail(String jsonRuleId);

	String checkIn(ResultTrans trans);

	String checkOut(ResultTrans trans);

	JSONArray getAttendanceResult(String userId);

}

