package com.sliansoft.wechat.attendance.manageservice;

import net.sf.json.JSONArray;

/**
 * <p>Description:</p>
 * @author zheng
 * @time 2015年12月10日 下午10:34:31
 */

public interface ManageService {

	JSONArray getRules(String userId, String corpId);

}

