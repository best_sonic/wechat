/**
 * 文档加载完毕之后从后台获取数据
 */
$(function() {
	$.ajax({
		url : "/app-mobile-attendance-web/menu_my_getData",
		type : "POST",
		async : false,
		dataType : "json",
		success : function(data) {
			showResultList(data);
		},
		error : function(data) {
			alert("请求失败 " + data);
		}
	});
})

/**
 * 传递过来的是两层的惊悚数组，遍历这个对象的时候，key表示序号，value表示集合中的元素
 * 
 * @param data
 */
function showResultList(data) {
	var temp = "";
	for(var length = 0; length < data.length; length++){
		for(var key1 in data[length]) {
			var itemList = "<div class='panel panel-primary'>"
				+ "<button data-toggle='collapse' data-parent='#accordion'"
				+ "data-target='#collapse" + key1 + "'"
				+ "class='panel panel-heading panel-title btn btn-primary' id='" + key1 + "'>" + key1 + "</button>"
				+ "<div id='collapse" + key1 + "' class='panel-collapse collapse'>"
				+ "<div class='container-fluid'>"
				+ "<div class='panel-body list-group'>";
			
			var timeList = "";
			for(var key2 in data[length][key1]) {
				var resultList = "<div class='list-group-item'>"
					+ "<button class='btn btn-primary' type='button' data-toggle='collapse'"
					+ "data-target='#" + key2 + "' aria-expanded='false'"
					+ "aria-controls='" + key2 + "'>" + data[length][key1][key2].ruleName + "</button>"
					+ "<div class='collapse' id='" + key2 + "'>"
					+ "<div class='well container-fluid'>"
					+ "<div class='row'>"
					+ "<div class='col-xs-4 text-right'>创建者：</div>"
					+ "<div class='col-xs-8 text-left'>" + data[length][key1][key2].creator + "</div>"
					+ "</div>"
					+ "<div class='row'>"
					+ "<div class='col-xs-4 text-right'>签到时间：</div>"
					+ "<div class='col-xs-8 text-left'>" + data[length][key1][key2].checkInTime + "</div>"
					+ "</div>"
					+ "<div class='row'>"
					+ "<div class='col-xs-4 text-right'>签到位置：</div>"
					+ "<div class='col-xs-8 text-left'>" + data[length][key1][key2].checkInPosition + "</div>"
					+ "</div>"
					+ "<div class='row'>"
					+ "<div class='col-xs-4 text-right'>签退时间：</div>"
					+ "<div class='col-xs-8 text-left'>" + data[length][key1][key2].checkOutTime + "</div>"
					+ "</div>"
					+ "<div class='row'>"
					+ "<div class='col-xs-4 text-right'>签退位置：</div>"
					+ "<div class='col-xs-8 text-left'>" + data[length][key1][key2].checkOutPosition + "</div>"
					+ "</div>"
					+ "<div class='row'>"
					+ "<div class='col-xs-4 text-right'>考勤结果：</div>"
					+ "<div class='col-xs-8 text-left'>" + data[length][key1][key2].resultStr + "</div>"
					+ "</div></div></div></div>";
				
				timeList = timeList + resultList;
			}
			itemList = itemList + timeList + "</div></div></div></div>";
			temp = temp + itemList;
		}
	}
	$("#accordion").append(temp);
}

/**
 * 点击按钮之后定位到当前日期所在的地方，点击a标签之后会先触发onclick事件， 然后进行跳转，在js中修改<a>标签的href属性，然后实现点击
 */
function relocate() {
	var selectedDate = document.getElementById("appDate").value;
	location.hash = "#collapse" + selectedDate;
	$("#" + selectedDate).click();
}
