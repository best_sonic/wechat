/**
 * 文档加载完毕之后从后台获取数据
 */
$(document).ready(function() {
	// 用于保存配置信息
	var configInfo = null;
	$.ajax({
		url : "/app-mobile-attendance-web/menu_today_getData",
		type : "GET",
		async : false,
		dataType : "json",
		success : function(data) { // 拿到的数据是json对象组成的数组
			configInfo = data.pop();
			showRuleList(data);
		},
		error : function(data) {
		}
	});

	// 进行验证所需要的数据应该从后台动态的获得，可以在获取考勤规则的时候将这些数据获取过来
	wx.config({
		debug : false,
		appId : configInfo.corpId,
		timestamp : configInfo.timestamp,
		nonceStr : configInfo.nonceStr,
		signature : configInfo.signature,
		jsApiList : [ 'getLocation' ]
	});

});

// 微信的js-sdk的配置
// ready接口用于处理成功验证
wx.ready(function() {
	//alert("验证成功" + res);
})

// error接口处理失败验证
wx.error(function(res) {
	alert("验证失败" + res.errMsg);
});

/**
 * 传递过来的是JSONArray对象，遍历这个对象的时候，key表示序号，value表示集合中的元素
 * 
 * @param data
 */
function showRuleList(data) {
	$
			.each(
					data,
					function(key, value) {
						var shortCheckInTime = value.checkInTime.substring(11, 16);
						var shortCheckInStartTime = value.checkInStartTime.substring(11, 16);
						var shortCheckInEndTime = value.checkInEndTime.substring(11, 16);
						var shortCheckOutTime = value.checkOutTime.substring(11, 16);
						var shortCheckOutStartTime = value.checkOutStartTime.substring(11, 16);
						var shortCheckOutEndTime = value.checkOutEndTime.substring(11, 16);
						var listItem = "<div class='panel panel-primary'>"
								+ "<div class='panel panel-heading'>"
								+ "<h2 class='panel-title'>"
								+ "<a data-toggle='collapse' data-parent='#accordion' href='#collapse"
								+ key
								+ "'>"
								+ value.ruleName
								+ "</a>"
								+ "</h2>"
								+ "</div>"
								+ "<div id='collapse"
								+ key
								+ "' class='panel-collapse collapse'>"
								+ "<div class='panel-body container-fluid'>"
								+ "<div class='row'>"
								+ "<div class='col-xs-3 text-right'>创建者：</div>"
								+ "<div class=''col-xs-9 text-left'>"
								+ value.creator
								+ "</div>"
								+ "</div>"
								+ "<div class='row'>"
								+ "<div class='col-xs-3 text-right'>签到时间：</div>"
								+ "<div class='col-xs-3 text-left'>"
								+ shortCheckInTime
								+ "</div>"
								+ "<div class='col-xs-2 text-right'>限制：</div>"
								+ "<div class='col-xs-4 text-left'>"
								+ shortCheckInStartTime
								+ "~"
								+ shortCheckInEndTime
								+ "</div>"
								+ "</div>"
								+ "<div class='row'>"
								+ "<div class='col-xs-3 text-right'>签退时间：</div>"
								+ "<div class='col-xs-2 text-left'>"
								+ shortCheckOutTime
								+ "</div>"
								+ "<div class='col-xs-3 text-right'>限制：</div>"
								+ "<div class='col-xs-4 text-left'>"
								+ shortCheckOutStartTime
								+ "~"
								+ shortCheckOutEndTime
								+ "</div>"
								+ "</div>"
								+ "<div class='row'>"
								+ "<div class='col-xs-3 text-right'>考勤位置：</div>"
								+ "<div class='col-xs-9 text-left'>"
								+ value.centerStr
								+ "</div>"
								+ "</div>"
								+ "<div class='row'>"
								+ "<div class='col-xs-3 text-right'>工作日：</div>"
								+ "<div class='col-xs-9 text-left'>"
								+ value.circule
								+ "</div>"
								+ "</div>"
								+ "</div>"
								+ "<div class='panel-footer row' id='"
								+ value.ruleId
								+ "'>"
								+ "<input type='hidden' class='ruleId' value='"
								+ value.ruleId
								+ "'/>"
								+ "<input type='hidden' class='checkInTime' value='"
								+ value.checkInTime
								+ "'/>"
								+ "<input type='hidden' class='delayTime' value='"
								+ value.delayTime
								+ "'/>"
								+ "<input type='hidden' class='checkInStartTime' value='"
								+ value.checkInStartTime
								+ "'/>"
								+ "<input type='hidden' class='checkInEndTime' value='"
								+ value.checkInEndTime
								+ "'/>"
								+ "<input type='hidden' class='checkOutTime' value='"
								+ value.checkOutTime
								+ "'/>"
								+ "<input type='hidden' class='leadTime' value='"
								+ value.leadTime
								+ "'/>"
								+ "<input type='hidden' class='checkOutStartTime' value='"
								+ value.checkOutStartTime
								+ "'/>"
								+ "<input type='hidden' class='checkOutEndTime' value='"
								+ value.checkOutEndTime
								+ "'/>"
								+ "<input type='hidden' class='centerStr' value='"
								+ value.centerStr
								+ "'/>"
								+ "<input type='hidden' class='radius' value='"
								+ value.radius
								+ "'/>"
								+ "<div class='col-xs-2'>&nbsp</div>"
								+ "<button type='submit' class='btn btn-primary col-xs-3' onclick='checkIn(this);'>签到</button>"
								+ "<div class='col-xs-2'>&nbsp</div>"
								+ "<button type='submit' class='btn btn-primary col-xs-3' onclick='checkOut(this);'>签退</button>"
								+ "<div class='col-xs-2'>&nbsp</div>"
								+ "</div>" + "</div>" + "</div>";
						$("#accordion").append(listItem);
					});
}

// 考勤需要用到的各种全局变量
var ruleId = null;
var check = null;
var currentTime = null;
var checkStartTime = null;
var checkEndTime = null;
var checkTime = null; // 规定的考勤时间
var marginTime = null;// 允许提前或者推迟的时间
var currentPositionStr = null;// 用户当前的位置
var centerStr = null;// 考勤规则规定的位置
var radius = null;// 考勤规则允许的范围
var latitude = null;
var longitude = null;

// 签到，value中存的是当前的考勤规则对象
function checkIn(obj) {
	currentTime = new Date();
	// 考勤的规则的属性，精确到分钟：2015-12-01 07:30
	checkStartTime = new Date($(obj).siblings(".checkInStartTime").val());
	checkEndTime = new Date($(obj).siblings(".checkInEndTime").val());

	// 如果不在考勤时间之内
	if (currentTime < checkStartTime || currentTime > checkEndTime) {
		$("#sysTimeInfo").modal({
			backdrop : "static"
		});
	} else {
		ruleId = $(obj).siblings(".ruleId").val();
		checkTime = $(obj).siblings(".checkInTime").val();
		marginTime = $(obj).siblings(".delayTime").val();
		centerStr = $(obj).siblings(".centerStr").val();
		radius = $(obj).siblings(".radius").val();
		check = "checkIn";
		checked();
	}

}

// 签退
function checkOut(obj) {
	currentTime = new Date();
	checkStartTime = new Date($(obj).siblings(".checkOutStartTime").val());
	checkEndTime = new Date($(obj).siblings(".checkOutEndTime").val());

	// 这里判断是否在考勤时间段之内
	if (currentTime < checkStartTime || currentTime > checkEndTime) {
		$("#sysTimeInfo").modal({
			backdrop : "static"
		});
	} else {
		ruleId = $(obj).siblings(".ruleId").val();
		checkTime = $(obj).siblings(".checkOutTime").val();
		marginTime = $(obj).siblings(".leadTime").val();
		centerStr = $(obj).siblings(".centerStr").val();
		radius = $(obj).siblings(".radius").val();
		check = "checkOut";
		checked();
	}
}

/**
 * 处理考勤操作
 * 
 * @param check
 *            用于判断是签到还是签退
 * @param currentTime
 *            当前的时间
 * @param ruleId
 *            当前的考勤规则id
 * @param centerStr
 *            当前的考勤规则的考勤中心位置
 */
function checked() {
	// 获取经纬度
	wx.getLocation({
		type : "wgs84",
		success : function(res) {
			latitude = res.latitude;
			longitude = res.longitude;
			var speed = res.speed;
			var accuracy = res.accuracy;
			// 将经纬度转换成地址
			$.ajax({
				url : "http://api.map.baidu.com/geocoder/v2/",
				dataType : 'jsonp',
				processData : false,
				type : "GET",
				async : false,
				data : "ak=gWsdPPlrBhIDvMNf7Q3bGRYj&callback=renderReverse&location="
						+ latitude
						+ ","
						+ longitude
						+ "&output=json&pois=1",
				success : function(data) {
					//alert("success");
				},
			});
		},

		cancel : function(res) {
			// alert("用户拒绝授权获取地理位置");
		}
	});

	renderReverse = function(data) {
		currentPositionStr = data.result.formatted_address
				+ data.result.sematic_description;
		
		// 给出用户当前的位置，提示用户是否进行考勤，如果用户放弃考勤则不向后台传输
		$("#positionValue").html("您当前的位置是：" + currentPositionStr + "，是否进行考勤？");
		$("#sysPositionInfo").modal({
			backdrop : "static"
		});
	}
}

// 用户确认进行考勤
function confirm() {
	currentTime = currentTime.getTime();// 将日期转换成时间戳
	// 开始往数据库中存放
	$.ajax({
		url : "/app-mobile-attendance-web/" + check,
		type : "POST",
		async : false,
		contentType : "application/json",
		data : '{"ruleId":"' + ruleId + '", "currentTime":"' + currentTime
				+ '", "checkTime":"' + checkTime + '", "marginTime":"'
				+ marginTime + '", "currentPositionStr":"' + currentPositionStr
				+ '", "latitude":"' + latitude + '", "longitude":"' + longitude
				+ '", "centerStr":"' + centerStr + '", "radius":"' + radius
				+ '"}',
		success : function(data) {
			if (data != null) {
				// 展示考勤结果，后台返回的结果是字符串
				$("#resultValue").html(data);
				$("#sysResultInfo").modal({
					backdrop : "static"
				});
			}
		},
		error : function(data) {

		}
	});
}

// 用户取消考勤
function cancel() {
	var res = "您已经取消了考勤";
}
