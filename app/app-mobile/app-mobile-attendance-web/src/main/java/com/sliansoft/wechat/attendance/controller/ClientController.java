package com.sliansoft.wechat.attendance.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sliansoft.wechat.attendance.clientservice.ClientService;
import com.sliansoft.wechat.attendance.entity.ConfigInfo;
import com.sliansoft.wechat.attendance.entity.ResultTrans;
import com.sliansoft.wechat.attendance.kit.FecthFromWeChat;
import com.sliansoft.wechat.attendance.weixinkit.AesException;

import net.sf.json.JSONArray;

/**
 * <p>
 * Description:处理微信中传过来的数据的Controller
 * </p>
 * 
 * @author zheng
 * @time 2015年12月6日 下午6:47:46
 */
@Controller
public class ClientController {

	@Autowired
	private ClientService clientService;

	/**
	 * 今日考勤 注解@ResponseBody的作用是将Controller方法的返回值经过HttpMessageConverter转换为指定格式之后，
	 * 写入到HttpServletResponse对象的body数据区
	 * 
	 * @param req
	 * @param resp
	 * @return
	 * @throws AesException
	 */
	@RequestMapping(value = "/menu_today_getData", method = RequestMethod.GET)
	public @ResponseBody JSONArray todayAttendence(HttpServletRequest request, HttpServletResponse response)
			throws AesException {
		HttpSession session = request.getSession();
		// 当前用户所属的企业的id
		String corpId = (String) session.getAttribute("corpId");
		// 当前用户的id
		String userId = (String) session.getAttribute("userId");
		// 当前企业的accesstoken
		String access_token = (String) session.getAttribute("accessToken");
		// 当前页面的地址
		String currentUrl = (String) session.getAttribute("currentUrl");

		JSONArray jsonRules = clientService.getTodayAttendance(corpId, userId);
		// 只有今日考勤的页面中需要使用js-sdk，所以配置信息只需要在这里得到就行了
		ConfigInfo configInfo = FecthFromWeChat.getConfigInfo(access_token, currentUrl, request, response);

		if (configInfo != null) {
			configInfo.setCorpId(corpId);
			jsonRules.add(configInfo);
		}

		return jsonRules;
	}

	/**
	 * 处理签到请求，处理签到签退请求的时候，应该获取用户当前的位置信息
	 * 
	 * @param trans
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "checkIn")
	public @ResponseBody String chekIn(@RequestBody ResultTrans trans, HttpServletRequest request,
			HttpServletResponse response) {
		// 当前用户的id
		String userId = (String) request.getSession().getAttribute("userId");
		trans.setUserId(userId);
		String checkInResutlStr = null;

		if (userId != null && (trans.getCurrentTime()) != null && (trans.getRuleId()) != null)
			checkInResutlStr = clientService.checkIn(trans);
		else {
			checkInResutlStr = "data error";
		}

		return checkInResutlStr;
	}

	/**
	 * 处理签退请求
	 * 
	 * @param ruleId
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "checkOut")
	public @ResponseBody String chekcOut(@RequestBody ResultTrans trans, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("checkOut");
		// 当前用户的id
		String userId = (String) request.getSession().getAttribute("userId");
		trans.setUserId(userId);
		String checkOutResultStr = null;

		if (userId != null && (trans.getCurrentTime()) != null && (trans.getRuleId()) != null) {
			checkOutResultStr = clientService.checkOut(trans);
		} else {
			checkOutResultStr = "data error";
		}
		return checkOutResultStr;
	}

	/**
	 * 我的考勤，前端从这个方法中得到数据，request和response两个参数不是必须的
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "menu_my_getData", method = RequestMethod.POST)
	public @ResponseBody JSONArray myAttendance(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		// 当前用户的id
		String userId = (String) session.getAttribute("userId");

		JSONArray jsonResult = clientService.getAttendanceResult(userId);
		return jsonResult;
	}

	@RequestMapping(value = "/recvMsg", method = RequestMethod.POST)
	public void recvMsg(HttpServletRequest request, HttpServletResponse response) throws IOException {
	}
}
