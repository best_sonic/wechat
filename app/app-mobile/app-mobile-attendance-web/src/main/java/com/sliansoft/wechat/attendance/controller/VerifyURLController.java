package com.sliansoft.wechat.attendance.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sliansoft.wechat.attendance.kit.ContextValue;
import com.sliansoft.wechat.attendance.weixinkit.WXBizMsgCrypt;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2015年12月10日 下午7:30:08
 */
@Controller
public class VerifyURLController {

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public void recvMsg(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			WXBizMsgCrypt wxcpt = new WXBizMsgCrypt(ContextValue.TOKEN, ContextValue.ENCOGINGAESKEY,
					ContextValue.CORPID);

			// 解析出url上的参数值如下：
			String sVerifyMsgSig = request.getParameter("msg_signature");
			String sVerifyTimeStamp = request.getParameter("timestamp");
			String sVerifyNonce = request.getParameter("nonce");
			String sVerifyEchoStr = request.getParameter("echostr");

			String sEchoStr; // 需要返回的明文
			sEchoStr = wxcpt.VerifyURL(sVerifyMsgSig, sVerifyTimeStamp, sVerifyNonce, sVerifyEchoStr);
			// 验证URL成功，将sEchoStr返回
			PrintWriter out = response.getWriter();
			out.print(sEchoStr);
		} catch (Exception e) {
			PrintWriter out = response.getWriter();
			out.print(e);
			// 验证URL失败，错误原因请查看异常
			System.out.println(e.toString());
			e.printStackTrace();
		}

		// HttpSession session = request.getSession();
		// System.out.println(session.getId());
		// System.out.println(session);

	}
}
