package com.sliansoft.wechat.attendance.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import net.sf.json.JSONObject;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2016年1月13日 下午8:55:19
 */
@Controller
public class TestController {

	@RequestMapping("getMsg")
	public JSONObject getMsg(HttpServletRequest request, HttpServletResponse response) {
		String jsonStr = "{'name':'zhangsan', 'age':20}";
		return JSONObject.fromObject(jsonStr);
	}
}
