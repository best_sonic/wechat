package com.sliansoft.wechat.attendance.weixinkit;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2016年1月6日 下午4:01:07
 */

public class SHAEncoder {

	public static String sha(String str) throws AesException {
		// SHA1签名生成
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new AesException(AesException.ComputeSignatureError);
		}
		md.update(str.getBytes());
		byte[] digest = md.digest();

		StringBuffer hexstr = new StringBuffer();
		String shaHex = "";
		for (int i = 0; i < digest.length; i++) {
			shaHex = Integer.toHexString(digest[i] & 0xFF);
			if (shaHex.length() < 2) {
				hexstr.append(0);
			}
			hexstr.append(shaHex);
		}
		return hexstr.toString();
	}

}
