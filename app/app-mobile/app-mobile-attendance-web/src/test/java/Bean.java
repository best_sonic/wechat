
/**
 * <p>
 * Description:
 * </p>
 * 
 * @author zheng
 * @time 2016年1月12日 下午11:05:59
 */

public class Bean {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Bean(String name) {
		super();
		this.name = name;
	}

}
